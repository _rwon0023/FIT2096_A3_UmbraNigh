// { Ryan Wong <32491034> 2022 }


#include "LargeContainer.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"

// Sets default values
ALargeContainer::ALargeContainer()
{
	PrimaryActorTick.bCanEverTick = false;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Root Component")));

	// Set up mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ALargeContainer::BeginPlay()
{
	Super::BeginPlay();

	bIsFull = false;
}

void ALargeContainer::Interact_Implementation(AUmbraNighCharacter* Interactor)
{
	if (!bIsFull) {
		if (!Interactor->bIsDraggingBody()) {
			ContainCharacter(Interactor);
			Interactor->InteractionOverrides.Add(GetInteractionType_Implementation(), this);
		}
		else {
			StoredBody = Interactor->DraggedBody;
			Interactor->Drop();

			StoredBody->Mesh->SetSimulatePhysics(false);
			StoredBody->SetActorEnableCollision(false);
			StoredBody->SetActorHiddenInGame(true);

			StoredBody->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
		}
		bIsFull = true;
	}
	else {
		if (StoredCharacter) {
			ReleaseCharacter();
		}
		else if (StoredBody) {
			StoredBody->SetActorEnableCollision(true);
			StoredBody->Mesh->SetSimulatePhysics(true);
			StoredBody->SetActorHiddenInGame(false);

			StoredBody->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
			StoredBody = nullptr;
		}

		bIsFull = false;
	}
}

EInteractionType ALargeContainer::GetInteractionType_Implementation()
{
	return EInteractionType::INTERACT;
}

void ALargeContainer::ContainCharacter(AUmbraNighCharacter* ToContain)
{
	StoredCharacter = ToContain;
	ToContain->Disable();
	ToContain->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	StoredRelativeLocation = ToContain->GetRootComponent()->GetRelativeLocation();
	ToContain->SetActorRelativeLocation(FVector::ZeroVector);
}

void ALargeContainer::ReleaseCharacter()
{
	StoredCharacter->Enable();
	StoredCharacter->SetActorRelativeLocation(StoredRelativeLocation);
	StoredCharacter->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	StoredCharacter = nullptr;
}

