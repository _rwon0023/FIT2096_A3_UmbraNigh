// { Ryan Wong <32491034> 2022 }


#include "BlindableLight.h"

// Sets default values
ABlindableLight::ABlindableLight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);
	AssignColliderAsShootable(Mesh);
}

// Called when the game starts or when spawned
void ABlindableLight::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABlindableLight::OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy)
{
	FirearmShotBy->OnHeadshot(this);
}

bool ABlindableLight::bIsBlind_Implementation()
{
	return bBlind;
}

void ABlindableLight::DoBlindForDuration_Implementation(int Duration)
{
	BlindSelf_Implementation();
	GetWorld()->GetTimerManager().SetTimer(BlindDurationHandle, this, &ABlindableLight::UnBlindSelf_Implementation, Duration, false);
}

void ABlindableLight::BlindSelf_Implementation()
{
	bBlind = true;
	for (ULightComponent* Light : Lights) {
		if (Light) {
			Light->Deactivate();
		}
	}
}

void ABlindableLight::UnBlindSelf_Implementation()
{
	bBlind = false;
	for (ULightComponent* Light : Lights) {
		if (Light) {
			Light->Activate();
		}
	}
}

void ABlindableLight::DoDefaultBlind_Implementation()
{
	DoBlindForDuration_Implementation(60.0F);
}

