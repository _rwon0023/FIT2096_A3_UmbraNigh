// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/LimpBody.h"

#include "UmbraNigh/Interfaces/Interactable.h"
#include "GameFramework/Actor.h"
#include "LargeContainer.generated.h"

UCLASS()
class UMBRANIGH_API ALargeContainer : public AActor, public IInteractable
{
	GENERATED_BODY()
	
private:
	AUmbraNighCharacter* StoredCharacter;
	ALimpBody* StoredBody; 

	FVector StoredRelativeLocation;

public:	
	// Sets default values for this actor's properties
	ALargeContainer();

	UPROPERTY(VisibleAnywhere)
	bool bIsFull;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	virtual void Interact_Implementation(AUmbraNighCharacter* Interactor) override;
	virtual EInteractionType GetInteractionType_Implementation() override;

	void ContainCharacter(AUmbraNighCharacter* ToContain);
	void ReleaseCharacter();
};
