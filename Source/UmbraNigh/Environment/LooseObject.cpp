// { Ryan Wong <32491034> 2022 }


#include "LooseObject.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"

// Sets default values
ALooseObject::ALooseObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set up root scene componnet
	// SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Root Component")));

	// Set up Perception Stimuli Source
	PerceptionStimuli = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("Perception Stimuli"));

	// Set up mesh with physics
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	InitialiseAssignedMesh();

}

// Called when the game starts or when spawned
void ALooseObject::BeginPlay()
{
	Super::BeginPlay();
	
	InitialiseAssignedMesh();
}

void ALooseObject::InitialiseAssignedMesh()
{
	if (Mesh) {

		Mesh->SetSimulatePhysics(true);
		AssignColliderAsShootable(Mesh);

		if (RootComponent.Get()) {
			Mesh->SetWorldLocationAndRotation(RootComponent.Get()->GetComponentLocation(), RootComponent.Get()->GetComponentRotation());
		}

		SetRootComponent(Mesh);

		PerceptionStimuli->RegisterForSense(UAISense_Sight::StaticClass());
		PerceptionStimuli->RegisterWithPerceptionSystem();
	}
}

// Called every frame
void ALooseObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALooseObject::OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy)
{
	UE_LOG(LogTemp, Display, TEXT("{%s} is shot"), *GetActorNameOrLabel());
	if (Mesh && Mesh->IsSimulatingPhysics()) {
		float ImpulseMagnitude = 50000;
		
		ShotDirection.Normalize();
		Mesh->AddImpulseAtLocation(ShotDirection * ImpulseMagnitude, Hit.ImpactPoint);

		UE_LOG(LogTemp, Display, TEXT("{%s}'s mesh is shot with impulse <%f> with direction <%s>"), *GetActorNameOrLabel(), ImpulseMagnitude, *ShotDirection.ToString());
	}
}

void ALooseObject::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	AUmbraNighCharacter* DetectorCharacter = Cast<AUmbraNighCharacter>(Detector);
	if (DetectorCharacter) {
		// Set up Trace Params
		FCollisionQueryParams TraceParams(FName(TEXT("Interactor Recognised Trace")), true, this);
		TraceParams.bReturnPhysicalMaterial = false;
		TArray<FOverlapResult> HitOuts;
		HitOuts.Empty();
		/* Debugging * /
		const FName TraceTag("InteractorRecognisedTrace");
		GetWorld()->DebugDrawTraceTag = TraceTag;
		TraceParams.TraceTag = TraceTag;
		//*/

		// Perform Trace/Overlap
		FVector Extents;
		FVector Origin;
		GetActorBounds(true, Origin, Extents, true);
		float Range = Extents.GetMax() + 50;
		GetWorld()->OverlapMultiByChannel(HitOuts, Origin, FQuat::Identity, ECollisionChannel::ECC_Pawn, FCollisionShape::MakeSphere(Range), TraceParams);

		for (FOverlapResult HitOut : HitOuts) {
			AUmbraNighCharacter* HitCharacter = Cast<AUmbraNighCharacter>(HitOut.GetActor());
			if (HitCharacter) {
				if (HitCharacter->Allegiance == DetectorCharacter->Allegiance) {
					return; // Break early if right next to same allegiance (eg non suspicious if an ally seems to be causing the movement)
				}
			}
		}
	}
	
	if (Mesh->GetComponentVelocity().Length() > 1.0F) {
		SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
		SuspicionOut.DetectionAmount = 15;
		SuspicionOut.bSuspicionOnly = true;
	}
}

void ALooseObject::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 25;
	SuspicionOut.bSuspicionOnly = true;
}

