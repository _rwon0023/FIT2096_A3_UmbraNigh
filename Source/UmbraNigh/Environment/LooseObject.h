// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISense_Sight.h"

#include "UmbraNigh/Interfaces/Shootable.h"
#include "UmbraNigh/Interfaces/Detectable.h"
#include "GameFramework/Actor.h"
#include "LooseObject.generated.h"

UCLASS()
class UMBRANIGH_API ALooseObject : public AActor, public IShootable, public IDetectable
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ALooseObject();

	UPROPERTY(EditAnywhere)
		UMeshComponent* Mesh;
	UPROPERTY(EditAnywhere)
		UAIPerceptionStimuliSourceComponent* PerceptionStimuli;

protected:
	bool bDisablePhysicsImpulse;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void InitialiseAssignedMesh();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy) override;

	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
};
