// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/UmbraNighCharacter.h"

#include "UmbraNigh/Interfaces/Interactable.h"
#include "GameFramework/Actor.h"
#include "MissionObjective.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnObjectiveResolved, AMissionObjective*, ObjectiveResolved);

UCLASS()
class UMBRANIGH_API AMissionObjective : public AActor, public IInteractable
{
	GENERATED_BODY()
	
private:
	bool bObjectiveCompleted = false;
public:	
	// Sets default values for this actor's properties
	AMissionObjective();

	FOnObjectiveResolved ObjectiveResolvedDelegate;
	FTimerHandle PerformObjectiveDurationHandle;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
		bool bDisableOnCompletion = true;

	UPROPERTY(EditAnywhere)
		float Duration = 5.0F;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Interact_Implementation(AUmbraNighCharacter* Interactor);
	EInteractionType GetInteractionType_Implementation();

	UFUNCTION()
	void CompleteObjective(AUmbraNighCharacter* Interactor);
};
