// { Ryan Wong <32491034> 2022 }


#include "MissionObjective.h"

// Sets default values
AMissionObjective::AMissionObjective()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Root Component")));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AMissionObjective::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMissionObjective::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMissionObjective::Interact_Implementation(AUmbraNighCharacter* Interactor)
{
	if (!bObjectiveCompleted) {
		if (!Interactor->bIsHandsOccupied()) {
			Interactor->Restraints.Add(this);
			FTimerDelegate CompletObjectiveDelegate = FTimerDelegate::CreateUObject(this, &AMissionObjective::CompleteObjective, Interactor);
			GetWorld()->GetTimerManager().SetTimer(PerformObjectiveDurationHandle, CompletObjectiveDelegate, Duration, false);
		}
	}
}

EInteractionType AMissionObjective::GetInteractionType_Implementation()
{
	return EInteractionType::INTERACT;
}

void AMissionObjective::CompleteObjective(AUmbraNighCharacter* Interactor)
{
	if (IsValid(Interactor)) {
		bObjectiveCompleted = true;

		ObjectiveResolvedDelegate.Broadcast(this);

		if (bDisableOnCompletion) {
			SetActorEnableCollision(false);
			SetActorHiddenInGame(true);
		}

		Interactor->Restraints.Remove(this);
	}
}

