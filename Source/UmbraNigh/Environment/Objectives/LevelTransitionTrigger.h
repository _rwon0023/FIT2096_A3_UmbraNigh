// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/StreamableManager.h"
#include "DrawDebugHelpers.h"
#include "UmbraNigh/Player/PlayerTheWitch.h"
#include "UmbraNigh/Environment/Objectives/MissionObjective.h"

#include "Engine/TriggerVolume.h"
#include "LevelTransitionTrigger.generated.h"

UCLASS()
class UMBRANIGH_API ALevelTransitionTrigger : public ATriggerVolume
{
	GENERATED_BODY()

private:
	FStreamableManager StreamableManager;
	
public:
	ALevelTransitionTrigger();

	UPROPERTY(EditAnywhere)
		TMap<AMissionObjective*,bool> RequiredObjectives;

	UPROPERTY(EditAnywhere)
		TSoftObjectPtr<UWorld> LevelTransitionTarget;
	UPROPERTY(VisibleAnywhere)
		FName LevelTransitionTargetName;
	
	virtual void BeginPlay() override;
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	UFUNCTION()
	void ResolveObjective(AMissionObjective* ObjectiveResolved);
	UFUNCTION()
	void OnTrigger(AActor* OverlappedActor, AActor* OtherActor);
	void ConditionalTransitionLevel();
	
};
