// { Ryan Wong <32491034> 2022 }


#include "LevelTransitionTrigger.h"

ALevelTransitionTrigger::ALevelTransitionTrigger()
{
	// Enable collision and bind overlap trigger function
	SetActorEnableCollision(true);
	OnActorBeginOverlap.Clear();
	OnActorBeginOverlap.AddUniqueDynamic(this, &ALevelTransitionTrigger::OnTrigger);
}

void ALevelTransitionTrigger::BeginPlay()
{
	Super::BeginPlay();
	/* Debugging */
	DrawDebugBox(GetWorld(), GetActorLocation(), GetActorScale() * 100, FColor::Cyan, true, -1, 0, 5);
	//*/
	for (TPair<AMissionObjective*, bool> Objective : RequiredObjectives) {
		Objective.Key->ObjectiveResolvedDelegate.AddUniqueDynamic(this, &ALevelTransitionTrigger::ResolveObjective);
	}
}

void ALevelTransitionTrigger::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	if (PropertyChangedEvent.GetPropertyName() == FName("LevelTransitionTarget")) { // If LevelTransitionTarget is changed
		if (LevelTransitionTarget.IsPending()) // Load LevelTransitionTarget UWorld using soft pointer
		{
			const FSoftObjectPath& AssetRef = LevelTransitionTarget.ToString();
			LevelTransitionTarget = Cast<UWorld>(StreamableManager.LoadSynchronous(AssetRef));
		}
		LevelTransitionTargetName = FName(LevelTransitionTarget.GetAssetName()); // Store name of UWorld (Store UWorld pointer itself doesn't allow map to save due to external ref, and soft pointer gets cleared if in different map)
	
	}
}

void ALevelTransitionTrigger::ResolveObjective(AMissionObjective* ObjectiveResolved)
{
	if (RequiredObjectives.Contains(ObjectiveResolved)) {
		RequiredObjectives[ObjectiveResolved] = true;
	}
}

void ALevelTransitionTrigger::OnTrigger(AActor* OverlappedActor, AActor* OtherActor)
{
	UE_LOG(LogTemp, Display, TEXT("Actor <%s> is being overlapped by <%s>"), *OverlappedActor->GetActorNameOrLabel(), *OtherActor->GetActorNameOrLabel());
	if (Cast<APlayerTheWitch>(OtherActor)) {
		ConditionalTransitionLevel();
	}
}

void ALevelTransitionTrigger::ConditionalTransitionLevel()
{
	UE_LOG(LogTemp, Display, TEXT("Conditional Level Transition Called on [%s]"), *LevelTransitionTargetName.ToString());

	for (TPair<AMissionObjective*, bool> Objective : RequiredObjectives) {
		if (!Objective.Value) {
			return; // Break if incomplete objective
		}
	}

	// Check if level transition target name has been set
	if (!LevelTransitionTargetName.IsNone()) { 
		// Open level using stored FName
		UE_LOG(LogTemp, Display, TEXT("Valid Target Exists for Conditional Level Transition, attempting to open..."));
		UGameplayStatics::OpenLevel(this, LevelTransitionTargetName);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No Level Transition Target Set"))
	}
}
