// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Weapons/Firearms/Firearm.h"
#include "Components/LightComponent.h"

#include "GameFramework/Actor.h"
#include "UmbraNigh/Interfaces/Blindable.h"
#include "UmbraNigh/Interfaces/Shootable.h"
#include "BlindableLight.generated.h"

UCLASS()
class UMBRANIGH_API ABlindableLight : public AActor, public IShootable, public IBlindable
{
	GENERATED_BODY()
	
private:
	bool bBlind;
public:	
	// Sets default values for this actor's properties
	ABlindableLight();

	FTimerHandle BlindDurationHandle;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere)
		TArray<ULightComponent*> Lights;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	virtual void OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy) override;
	virtual bool bIsBlind_Implementation() override;
	virtual void DoBlindForDuration_Implementation(int Duration) override;
	virtual void BlindSelf_Implementation() override;
	virtual void UnBlindSelf_Implementation() override;
	virtual void DoDefaultBlind_Implementation() override;
};
