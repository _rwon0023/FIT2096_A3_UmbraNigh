// { Ryan Wong <32491034> 2022 }


#include "GrimoirePossessionUI.h"

void UGrimoirePossessionUI::NativeConstruct()
{
	Super::NativeConstruct();
}

void UGrimoirePossessionUI::RequestCloseUI()
{
	RequestCloseDelegate.Broadcast();
}

void UGrimoirePossessionUI::InitialiseButtons(UGrimoire* PossessionGrimoire)
{
	if (PossessionGrimoire) {
		if (PossessionButtonClass && ButtonContainer) {
			UE_LOG(LogTemp, Display, TEXT("Initialising Possession UI Buttons"))
				TArray<TScriptInterface<IPlayable>> Possessables = PossessionGrimoire->GetAllPossessables();
			for (size_t i = 0; i < Possessables.Num(); i++)
			{
				UE_LOG(LogTemp, Display, TEXT("Initialising Possession UI Button no.%d"), i);
				UPossessionButtonUI* PossessionButton = CreateWidget<UPossessionButtonUI>(this, PossessionButtonClass.Get());
				PossessionButton->AssignPossessable(this, PossessionGrimoire, i);
				ButtonContainer->AddChild(PossessionButton);
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Failed to initialise Possession UI Buttons: Button class or container is unassigned"))
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to initialise Possession UI Buttons: Possession Grimoire is null"))
	}
	
}
