// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Components/Button.h"
#include "UmbraNigh/Magic/Grimoire.h"

#include "GenericUIDelegates.h"
#include "Blueprint/UserWidget.h"
#include "GrimoireSpellsUI.generated.h"


UCLASS()
class UMBRANIGH_API UGrimoireSpellsUI : public UUserWidget
{
	GENERATED_BODY()
	
private:
	TArray<USpellcastProxy*> SpellcastProxies;

public:
	FRequestClose RequestCloseDelegate;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TMap<ESpellSchool, UButton*> SpellButtons;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		UGrimoire* Spellbook;

	virtual void NativeConstruct() override;
	void AssignSpellbook(UGrimoire* GrimoireAssigned);
	void OnClickSpellButton();
};



UCLASS()
class UMBRANIGH_API USpellcastProxy : public UObject {
	GENERATED_BODY()
public:
	UGrimoireSpellsUI* OwnerPtr;
	UGrimoire* Spellbook;
	ESpellSchool School;
	UFUNCTION()
		void OnClickSpellcast();
};