// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "GenericUIDelegates.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRequestClose);

USTRUCT()
struct FGenericUIDelegates // Workaround to generate .generated.h file for shared delegate/s
{
	GENERATED_BODY()
};