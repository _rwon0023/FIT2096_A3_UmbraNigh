// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include <string>

#include "Components/ProgressBar.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "UmbraNigh/Player/PlayerTheWitch.h"
#include "UmbraNigh/Player/UmbraNighPlayerController.h"

#include "Blueprint/UserWidget.h"
#include "InGameUI.generated.h"

UCLASS(Abstract)
class UMBRANIGH_API UInGameUI : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
		class UProgressBar* HPBar;
	UPROPERTY(meta = (BindWidget))
		class UProgressBar* LightBar;
	UPROPERTY(meta = (BindWidget))
		class UImage* LightIndicator;
	UPROPERTY(meta = (BindWidget))
		class UImage* Reticle;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* AmmoIndicator;

	AUmbraNighPlayerController* PlayerController;
	APlayerTheWitch* MainPlayer;
	AUmbraNighCharacter* CurrentCharacter;
	void NativeConstruct() override;
	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;


};
