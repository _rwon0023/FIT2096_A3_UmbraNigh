 // { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Magic/Grimoire.h"
#include "UmbraNigh/Interfaces/Playable.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"

#include "Blueprint/UserWidget.h"
#include "PossessionButtonUI.generated.h"

// Forward Declarations
class UGrimoirePossessionUI;

UCLASS()
class UMBRANIGH_API UPossessionButtonUI : public UUserWidget
{
	GENERATED_BODY()
	
private:
	//Pointer to owner
	UGrimoirePossessionUI* OwnerUI;
	// Pointers to targets
	UGrimoire* TargetGrimoire;
	TScriptInterface<IPlayable> Possessable;
	int PossessableGrimoireIndex;
public:
	// UI components

	UPROPERTY(meta = (BindWidget))
		class UButton* PossessButton;

	UPROPERTY(meta = (BindWidget))
		class UImage* ProfileImg;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* NameText;

public:
	void AssignPossessable(UGrimoirePossessionUI* ParentUI, UGrimoire* GrimoireAssigned, int _PossessableGrimoireIndex);
	UFUNCTION()
	void OnClickPossess();
};