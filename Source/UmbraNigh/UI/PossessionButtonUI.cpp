// { Ryan Wong <32491034> 2022 }


#include "PossessionButtonUI.h"
#include "GrimoirePossessionUI.h"

void UPossessionButtonUI::AssignPossessable(UGrimoirePossessionUI* ParentUI, UGrimoire* GrimoireAssigned, int _PossessableGrimoireIndex)
{
	if (PossessButton) {
		UE_LOG(LogTemp, Display, TEXT("Possession Button {%s} assigning"), *GetName())
		OwnerUI = ParentUI;
		TargetGrimoire = GrimoireAssigned;
		PossessableGrimoireIndex = _PossessableGrimoireIndex;
		Possessable = GrimoireAssigned->GetPossessableAt(PossessableGrimoireIndex);
		if (Possessable && IsValid(Possessable.GetObject())) {
			APawn* PossessablePawn = Cast<APawn>(Possessable.GetObject());
			if (Possessable && PossessablePawn) {
				NameText->SetText(FText::FromString(PossessablePawn->GetActorNameOrLabel()));
					PossessButton->OnClicked.AddDynamic(this, &UPossessionButtonUI::OnClickPossess);
			}
			else {
				Possessable = nullptr;
				UE_LOG(LogTemp, Warning, TEXT("Possession Button {%s} failed to assign: Invalid PossessablePawn"), *GetName())
			}
		}
		else {
			Possessable = nullptr;
			UE_LOG(LogTemp, Warning, TEXT("Possession Button {%s} failed to assign: Invalid Possessable"), *GetName())
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Possession Button {%s} failed to assign: Button widget is not bound"), *GetName())
	}
}

void UPossessionButtonUI::OnClickPossess()
{
	UE_LOG(LogTemp, Display, TEXT("Possession Button {%s} was clicked"), *GetName());
	if (TargetGrimoire) {
		if (Possessable) {
			TargetGrimoire->PossessAtIndex(PossessableGrimoireIndex);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Possession Button {%s} failed to trigger possess: Grimoire was not assigned"), *GetName())
	}
	
	if (OwnerUI) {
		OwnerUI->RequestCloseUI();
	}
}
