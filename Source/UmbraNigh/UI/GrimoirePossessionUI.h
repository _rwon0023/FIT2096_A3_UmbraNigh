// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Components/PanelWidget.h"
#include "PossessionButtonUI.h"

#include "GenericUIDelegates.h"
#include "Blueprint/UserWidget.h"
#include "GrimoirePossessionUI.generated.h"


UCLASS()
class UMBRANIGH_API UGrimoirePossessionUI : public UUserWidget
{
	GENERATED_BODY()
	
public:

	FRequestClose RequestCloseDelegate;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UPossessionButtonUI> PossessionButtonClass;

	UPROPERTY(meta = (BindWdget))
		class UPanelWidget* ButtonContainer;

	virtual void NativeConstruct() override;
	void RequestCloseUI();
	void InitialiseButtons(UGrimoire* PossessionGrimoire);
};
