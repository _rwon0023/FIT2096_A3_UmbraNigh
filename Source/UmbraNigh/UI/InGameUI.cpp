// { Ryan Wong <32491034> 2022 }


#include "InGameUI.h"

void UInGameUI::NativeConstruct()
{
	Super::NativeConstruct();
	
	PlayerController = Cast<AUmbraNighPlayerController>(GetWorld()->GetFirstPlayerController());
	
	 MainPlayer = Cast<APlayerTheWitch>(PlayerController->MainPlayerPawn.GetObjectRef());
	 if (!MainPlayer) {
			UE_LOG(LogTemp, Warning, TEXT("Player could not be found by InGame UI"));
	 }
}

void UInGameUI::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (PlayerController) {
		CurrentCharacter = Cast<AUmbraNighCharacter>(PlayerController->CurrentPawn.GetObjectRef());
	}
	
	if (MainPlayer) {
		AUmbraNighCharacter* CurrentPlayer = (CurrentCharacter) ? CurrentCharacter : MainPlayer;

		HPBar->SetPercent(CurrentPlayer->HP->GetPercentFull());
		
		ALightDetector* LightDetector = CurrentPlayer->GetLightDetector();
		if (LightDetector) {
			LightBar->SetPercent(LightDetector->CalculateNormalisedLightValue());
		}

		AFirearm* Firearm = CurrentPlayer->Firearm;
		if (Firearm) {
			FText FormattedAmmoIndicatorText = FText::Format(FText::FromString("[{0} / {1}]"), FText::AsNumber(Firearm->Ammo->CurrentValue), FText::AsNumber(Firearm->Ammo->MaxValue));
			AmmoIndicator->SetText(FormattedAmmoIndicatorText);

			if(Reticle) Reticle->SetVisibility((CurrentPlayer->bIsAiming || CurrentPlayer->bIsCrouched)? ESlateVisibility::Visible : ESlateVisibility::Hidden);
		}

		// UE_LOG(LogTemp, Display, TEXT("Set UI HP Bar to <%f>"), Player->HP->GetPercentFull());
		// UE_LOG(LogTemp, Display, TEXT("Set UI Light Bar to <%f>"), Player->GetLightDetector()->CalculateNormalisedLightValue());

		MainPlayer = Cast<APlayerTheWitch>(PlayerController->MainPlayerPawn.GetObjectRef()); // Update MainPlayer to check if unassigned
	}
}
