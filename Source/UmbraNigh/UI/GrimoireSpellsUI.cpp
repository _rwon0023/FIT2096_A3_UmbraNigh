// { Ryan Wong <32491034> 2022 }


#include "GrimoireSpellsUI.h"

void UGrimoireSpellsUI::NativeConstruct()
{
	Super::NativeConstruct();
	bIsFocusable = true;
}

void UGrimoireSpellsUI::AssignSpellbook(UGrimoire* GrimoireAssigned)
{
	if (IsValid(GrimoireAssigned)) {

		Spellbook = GrimoireAssigned;

		UE_LOG(LogTemp, Display, TEXT("Grimoire <%s> Assiged to UI"), *Spellbook->GetName());

		for (TPair<ESpellSchool, UButton*> AvaiableSchool : SpellButtons) {
			ESpellSchool School = AvaiableSchool.Key;
			UButton* SpellButton = AvaiableSchool.Value;
			if (SpellButton) {
				if (!Spellbook->bIsSpellCurrentlyViable(School)) {
					UE_LOG(LogTemp, Display, TEXT("Disabling <%s>"), *SpellButton->GetName());
					SpellButton->SetIsEnabled(false);
				}
				else {
					// Create and use a Proxy struct to bind different spellcasts to each button
					USpellcastProxy* SpellcastProxy = NewObject<USpellcastProxy>(this);
					SpellcastProxy->OwnerPtr = this;
					SpellcastProxy->Spellbook = Spellbook;
					SpellcastProxy->School = School;
					SpellcastProxies.Add(SpellcastProxy); // Store proxies so not garbage collected

					SpellButton->OnClicked.AddDynamic(SpellcastProxy, &USpellcastProxy::OnClickSpellcast);
				}
				
			}
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Grimoire failed assignment to UI"));
	}
}

void UGrimoireSpellsUI::OnClickSpellButton()
{
	RequestCloseDelegate.Broadcast();
}

void USpellcastProxy::OnClickSpellcast()
{
	// Cast stored spell on stored spellbook
	this->Spellbook->CastSpellIfViable(this->School);
	// Close menu
	this->OwnerPtr->OnClickSpellButton();
}