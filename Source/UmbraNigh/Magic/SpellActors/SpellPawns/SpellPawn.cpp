// { Ryan Wong <32491034> 2022 }


#include "SpellPawn.h"

// Sets default values
ASpellPawn::ASpellPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	// ===== Camera

	CameraRig = CreateDefaultSubobject<UPawnCameraRig>(TEXT("Camera Rig"));
	CameraRig->SetupAttachment(RootComponent);

	// ===== Mesh

	// Set up mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	// Disable collision
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called when the game starts or when spawned
void ASpellPawn::BeginPlay()
{
	Super::BeginPlay();
}

void ASpellPawn::UnPossessed()
{
	Super::UnPossessed();
	Destroy();
}

FHitResult ASpellPawn::DoCameraTrace()
{
	FVector TraceStart = CameraRig->FollowCamera->GetComponentLocation();
	FVector Direction = CameraRig->FollowCamera->GetForwardVector();
	const FVector TraceEnd = TraceStart + Direction * 5000;

	// Set up Trace Params
	FCollisionQueryParams TraceParams(FName(TEXT("SpellPawn Trace")), true, this);
	TraceParams.bReturnPhysicalMaterial = false;
	FHitResult HitOut = FHitResult(ForceInit);
	/* Debugging * /
	const FName TraceTag("SpellPawnTrace");
	GetWorld()->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;
	//*/

	// Perform Trace
	GetWorld()->LineTraceSingleByChannel(HitOut, TraceStart, TraceEnd, ECollisionChannel::ECC_Camera, TraceParams);

	if (!HitOut.GetActor()) {
		HitOut.ImpactPoint = TraceEnd;
	}
	
	return HitOut;
}

void ASpellPawn::DoLook_Implementation(FVector LookInput)
{
	if (Controller && !LookInput.IsZero()) {
		AddControllerPitchInput(LookInput.Y);
		AddControllerYawInput(LookInput.Z);

		SetActorRotation(Controller->GetControlRotation());
	}
}

void ASpellPawn::DoSneak_Implementation()
{
	bSneakKeyDown = true;
}

void ASpellPawn::DoSneakEnd_Implementation()
{
	bSneakKeyDown = false;
}

void ASpellPawn::LinkSpell(USpell* Spell, AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	LinkedSpell = Spell;
	LinkedCharacter = Caster;
	LinkedGrimoire = Spellbook;

	if (LinkedCharacter) {
		this->SetActorLocation(LinkedCharacter->GetActorLocation() + FVector::UpVector * (LinkedCharacter->GetMesh()->Bounds.SphereRadius + 20));
	}
	if (LinkedGrimoire) {
		LinkedGrimoire->PossessNewPossessable(this);
	}
}
