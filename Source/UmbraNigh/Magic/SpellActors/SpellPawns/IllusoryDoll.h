// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/LightDetector.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraComponent.h"
#include "Materials/Material.h"

#include "SpellPawn.h"
#include "IllusoryDoll.generated.h"

UCLASS()
class UMBRANIGH_API AIllusoryDoll : public ASpellPawn
{
	GENERATED_BODY()
	
private:
	ALightDetector* LightDetector;
	UNiagaraComponent* TargetLocationParticles;

public:
	UPROPERTY(EditAnywhere)
		UNiagaraSystem* PS_TargetLocation;

	UPROPERTY(EditAnywhere)
		UMaterial* CloneMaterial;

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void UnPossessed() override;

	virtual void LinkSpell(USpell* Spell, AUmbraNighCharacter* Caster, UGrimoire* Spellbook);

	virtual void DoPrimaryAction_Implementation() override;
	virtual void DoSecondaryAction_Implementation() override;
};
