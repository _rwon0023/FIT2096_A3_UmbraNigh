// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Magic/Grimoire.h"
#include "UmbraNigh/Environment/LargeContainer.h"

#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "Ravenrat.generated.h"


UCLASS()
class UMBRANIGH_API ARavenrat : public AUmbraNighCharacter
{
	GENERATED_BODY()
	
private:
	AUmbraNighCharacter* PolymorphedCaster;
	UGrimoire* BoundGrimoire;
	FVector FlyDirection;
	ALargeContainer* PolymorphedContainer;

public:
	ARavenrat();

protected:
	virtual void BeginPlay() override;

public:

	bool bIsFlying;

	virtual void Tick(float DeltaTime) override;
	void PolymorphCaster(AUmbraNighCharacter* Spellcaster, UGrimoire* Spellbook);
	virtual void Die() override;

	virtual void DoJump_Implementation() override;
	virtual void DoJumpEnd_Implementation() override;
	virtual void DoSneak_Implementation() override;
	virtual void DoSneakEnd_Implementation() override;
	virtual void DoPrimaryAction_Implementation() override;
	virtual void DoSecondaryAction_Implementation() override;

	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
};
