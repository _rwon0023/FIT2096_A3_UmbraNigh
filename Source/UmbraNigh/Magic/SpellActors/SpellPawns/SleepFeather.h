// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "NiagaraSystem.h"

#include "SpellPawn.h"
#include "SleepFeather.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API ASleepFeather : public ASpellPawn
{
	GENERATED_BODY()
	
private:
	inline static const FName RemainingUsesDisplayParameterName = FName("Percent");
public:
	UPROPERTY(EditAnywhere)
		UNiagaraSystem* PS_MarkEffect;
	
	UPROPERTY(EditAnywhere)
		UMaterialInstanceDynamic* RemainingUsesDisplayMaterial;

	virtual void LinkSpell(USpell* Spell, AUmbraNighCharacter* Caster, UGrimoire* Spellbook);

	virtual void DoPrimaryAction_Implementation() override;
	virtual void DoSecondaryAction_Implementation() override;

	void UpdateRemainingUsesDisplay();
};
