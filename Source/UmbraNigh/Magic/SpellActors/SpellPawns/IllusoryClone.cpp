// { Ryan Wong <32491034> 2022 }


#include "IllusoryClone.h"

AIllusoryClone::AIllusoryClone()
{
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	CameraRig->ProbeChannel = ECollisionChannel::ECC_Visibility;

	Allegiance = EAllegiance::PLAYER;
}

void AIllusoryClone::DoPrimaryAction_Implementation()
{
	Die();
}

void AIllusoryClone::DealDamage_Implementation(int ShotDamage)
{
	if (!DamagedDisperseTimerHandle.IsValid()) {
		GetWorld()->GetTimerManager().SetTimer(DamagedDisperseTimerHandle, this, &AIllusoryClone::Die, DamageDisperseDuration, true);
	}
}

void AIllusoryClone::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 10;
	SuspicionOut.bSuspicionOnly = false;
	SuspicionOut.Priority = 10;
}

void AIllusoryClone::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::EXPLICIT;
	SuspicionOut.DetectionAmount = 40;
	SuspicionOut.bSuspicionOnly = false;
}
