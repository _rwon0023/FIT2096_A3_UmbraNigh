// { Ryan Wong <32491034> 2022 }


#include "SleepFeather.h"
#include "UmbraNigh/Magic/Spells/SleepSpell.h"

void ASleepFeather::LinkSpell(USpell* Spell, AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	USleepSpell* SleepSpell = Cast<USleepSpell>(Spell);
	if (SleepSpell) {
		Super::LinkSpell(Spell, Caster, Spellbook);

		// Setup Dynamic Material
		if (Mesh) {
			UMaterialInterface* RemainingUsesDisplayMeshMaterialInterface = Mesh->GetMaterial(0);
			if (RemainingUsesDisplayMeshMaterialInterface) {
				UMaterial* RemainingUsesDisplayMeshMaterial = RemainingUsesDisplayMeshMaterialInterface->GetMaterial();
				if (RemainingUsesDisplayMeshMaterial) {
					TArray<FMaterialParameterInfo> MatInfos;
					TArray<FGuid> MatIDs;
					RemainingUsesDisplayMeshMaterial->GetAllScalarParameterInfo(MatInfos, MatIDs);

					bool HasNecessaryParameter = false;
					for (FMaterialParameterInfo MatInfo : MatInfos) {
						if (MatInfo.Name == RemainingUsesDisplayParameterName) {
							HasNecessaryParameter = true;
							break;
						}
					}

					if (HasNecessaryParameter) {
						RemainingUsesDisplayMaterial = UMaterialInstanceDynamic::Create(RemainingUsesDisplayMeshMaterialInterface, this);
						Mesh->SetMaterial(0, RemainingUsesDisplayMaterial);
						UpdateRemainingUsesDisplay();
						UE_LOG(LogTemp, Warning, TEXT("{%s} Dynamic Material Generation Success"), *GetName());
					}
					else {
						UE_LOG(LogTemp, Warning, TEXT("{%s} Dynamic Material Generation Failed: Material Parameter not found"), *GetName());
					}
				}
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("{%s} Dynamic Material Generation Failed : Material not found"), *GetName());
			}
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Spell Link failed on {%s}"), *GetName());
	}
}

void ASleepFeather::DoPrimaryAction_Implementation()
{
	if (LinkedSpell) {
		Cast<USleepSpell>(LinkedSpell)->PerformSleep();
	}
}

void ASleepFeather::DoSecondaryAction_Implementation()
{
	if (bSneakKeyDown) {
		Destroy();
	}
	else {
		if (LinkedSpell) {
			USleepSpell* SleepSpell = Cast<USleepSpell>(LinkedSpell);
			if (SleepSpell->bCanMark()) {
				UE_LOG(LogTemp, Display, TEXT("{%s} Performing Trace"), *GetName());
				FHitResult HitOut = DoCameraTrace();
				AUmbraNighCharacter* HitCharacter = Cast<AUmbraNighCharacter>(HitOut.GetActor());
				if (HitCharacter && HitCharacter->bSneakAttackable) {
					SleepSpell->MarkCharacter(HitCharacter, PS_MarkEffect);
					UpdateRemainingUsesDisplay();
				}
			}
		}
	}
}

void ASleepFeather::UpdateRemainingUsesDisplay()
{
	USleepSpell* SleepSpell = Cast<USleepSpell>(LinkedSpell);
	if (SleepSpell && IsValid(RemainingUsesDisplayMaterial)) {
		float PercentUses = (float)SleepSpell->MarksRemaining / SleepSpell->StartingMarks;
		RemainingUsesDisplayMaterial->SetScalarParameterValue(RemainingUsesDisplayParameterName, PercentUses);
	}
}
