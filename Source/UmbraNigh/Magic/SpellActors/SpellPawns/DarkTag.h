// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "SpellPawn.h"
#include "DarkTag.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API ADarkTag : public ASpellPawn
{
	GENERATED_BODY()
	
public:
	inline static const int STENCIL_VALUE = 3;

	UPROPERTY(EditAnywhere)
		UMaterial* TagStencilMaterial;

	virtual void LinkSpell(USpell* Spell, AUmbraNighCharacter* Caster, UGrimoire* Spellbook);

	virtual void DoPrimaryAction_Implementation() override;
	virtual void DoSecondaryAction_Implementation() override;

	void TagTarget(AActor* ToTag);
	void Untag();
};
