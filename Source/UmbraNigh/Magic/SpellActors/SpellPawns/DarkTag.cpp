// { Ryan Wong <32491034> 2022 }


#include "DarkTag.h"
#include "UmbraNigh/Magic/Spells/DarkTagSpell.h"

void ADarkTag::LinkSpell(USpell* Spell, AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	if (Cast<UDarkTagSpell>(Spell)) {
		Super::LinkSpell(Spell, Caster, Spellbook);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Spell Link failed on {%s}"), *GetName());
	}
}

void ADarkTag::DoPrimaryAction_Implementation()
{
	if (LinkedCharacter && LinkedCharacter->GetLightDetector()->bIsShadowed()) {
		FHitResult HitOut = DoCameraTrace();
		AActor* HitActor = HitOut.GetActor();
		if (HitActor) {
			UE_LOG(LogTemp, Display, TEXT("{%s} Tagged <%s>"), *GetName(), *HitActor->GetActorNameOrLabel());
			TagTarget(HitActor);
		}
	}
	else {
		Destroy();
	}
}

void ADarkTag::DoSecondaryAction_Implementation()
{
	if (bSneakKeyDown) {
		Destroy();
	}
	else {
		if (LinkedSpell && Cast<UDarkTagSpell>(LinkedSpell)->TaggedTarget) {
			Untag();
		}
	}
}

void ADarkTag::TagTarget(AActor* ToTag)
{
	UDarkTagSpell* Spell = Cast<UDarkTagSpell>(LinkedSpell);
	if (Spell && !ToTag->ActorHasTag(FName(TEXT("CannotMark")))) {
		Untag();
		Spell->TaggedTarget = ToTag;
		if (ToTag) {
			TArray<UActorComponent*> TaggedPrimitiveComponents;
			ToTag->GetComponents(UMeshComponent::StaticClass(), TaggedPrimitiveComponents, true);

			for (UActorComponent* Component : TaggedPrimitiveComponents) {
				UMeshComponent* TaggedMesh = Cast<UMeshComponent>(Component);
				if (TaggedMesh && !TaggedMesh->bRenderCustomDepth) {
					TaggedMesh->SetRenderCustomDepth(true);
					TaggedMesh->CustomDepthStencilValue = STENCIL_VALUE;
					Spell->TaggedMeshes.Add(TaggedMesh);
				}
			}
		}
	}	
}

void ADarkTag::Untag()
{
	UDarkTagSpell* Spell = Cast<UDarkTagSpell>(LinkedSpell);
	if (Spell) {
		for (UMeshComponent* TaggedMesh : Spell->TaggedMeshes) {
			if (TaggedMesh) {
				TaggedMesh->SetRenderCustomDepth(false);
				TaggedMesh->CustomDepthStencilValue = 0;
			}
		}
		Spell->TaggedMeshes.Empty();
		Spell->TaggedTarget = nullptr;
	}
}
