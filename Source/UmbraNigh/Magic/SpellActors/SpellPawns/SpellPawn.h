// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/PawnCameraRig.h"
#include "UmbraNigh/Magic/Spells/Spell.h"
#include "UmbraNigh/Magic/Grimoire.h"

#include "GameFramework/Pawn.h"
#include "UmbraNigh/Interfaces/Playable.h"
#include "SpellPawn.generated.h"

UCLASS()
class UMBRANIGH_API ASpellPawn : public APawn, public IPlayable
{
	GENERATED_BODY()

public:
	ASpellPawn();

	UPROPERTY(EditAnywhere)
		UPawnCameraRig* CameraRig;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;

protected:
	USpell* LinkedSpell;
	AUmbraNighCharacter* LinkedCharacter;
	UGrimoire* LinkedGrimoire;

	bool bSneakKeyDown;

	virtual void BeginPlay() override;
	virtual void UnPossessed() override;

public:
	FHitResult DoCameraTrace();
	virtual void DoLook_Implementation(FVector LookInput) override;
	virtual void DoSneak_Implementation() override;
	virtual void DoSneakEnd_Implementation() override;

	virtual void LinkSpell(USpell* Spell, AUmbraNighCharacter* Caster, UGrimoire* Spellbook);
};
