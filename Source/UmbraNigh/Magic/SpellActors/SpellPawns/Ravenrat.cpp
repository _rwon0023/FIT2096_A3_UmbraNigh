// { Ryan Wong <32491034> 2022 }


#include "Ravenrat.h"

ARavenrat::ARavenrat()
{
	// Setup Smaller Hitbox Size
	GetCapsuleComponent()->InitCapsuleSize(40.0F, 40.0F);

	HP->InitialiseAsFull(1);
	Allegiance = EAllegiance::PLAYER;
}

void ARavenrat::BeginPlay()
{
	Super::BeginPlay();

	PolymorphedContainer = NewObject<ALargeContainer>(this);
	PolymorphedContainer->AttachToComponent(RootComponent,FAttachmentTransformRules::SnapToTargetIncludingScale);
}

void ARavenrat::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!bIsImmobile() && bIsFlying && !FlyDirection.IsZero()) {
		AddMovementInput(FlyDirection);
	}
}

void ARavenrat::PolymorphCaster(AUmbraNighCharacter* Spellcaster, UGrimoire* Spellbook)
{
	if (Spellcaster && Spellbook) {
		// Replace caster
		PolymorphedCaster = Spellcaster;
		BoundGrimoire = Spellbook;
		BoundGrimoire->RemovePossessable(PolymorphedCaster);
		BoundGrimoire->AddPossessableFront(this);
		BoundGrimoire->PossessAtIndex(0);
		// Position appropirately in the place of the caster and contain caster
		SetActorLocationAndRotation(PolymorphedCaster->GetActorLocation(), PolymorphedCaster->GetActorRotation());
		PolymorphedContainer->ContainCharacter(PolymorphedCaster);
		// Transfer any constraints
		Restraints = PolymorphedCaster->Restraints;
		WeighDowns = PolymorphedCaster->WeighDowns;
		// Disable Spellcasting
		BoundGrimoire->bSpellcastingEnabled = false;
		// Set HP
		HP->InitialiseAsFull(PolymorphedCaster->HP->CurrentValue / 2);
	}
}

void ARavenrat::Die()
{
	if (PolymorphedCaster) {
		bool PresentlyPossessed = BoundGrimoire->GetCurrentPossessed() == this;

		// Reposition caster here
		PolymorphedCaster->SetActorLocationAndRotation(GetActorLocation()+FVector::UpVector*PolymorphedCaster->GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), GetActorRotation());

		// Re-enable Spellcasting
		BoundGrimoire->bSpellcastingEnabled = true;

		// Re-enable caster and replace this
		PolymorphedContainer->ReleaseCharacter();
		BoundGrimoire->RemovePossessable(this);
		BoundGrimoire->AddPossessableFront(PolymorphedCaster);
		// If possessing Ravenrat when dies, switch possession to caster
		if (PresentlyPossessed) {
			BoundGrimoire->PossessAtIndex(0);
		}
	}
	Super::Die();
}


void ARavenrat::DoJump_Implementation()
{
	bIsFlying = true;
	ClientCheatFly();
	FlyDirection.Z = 1;
}

void ARavenrat::DoJumpEnd_Implementation()
{
	FlyDirection.Z = 0;
}

void ARavenrat::DoSneak_Implementation()
{
	FlyDirection.Z = -1;
}

void ARavenrat::DoSneakEnd_Implementation()
{
	FlyDirection.Z = 0;
}

void ARavenrat::DoPrimaryAction_Implementation()
{
	bIsFlying = false;
	ClientCheatWalk();
}

void ARavenrat::DoSecondaryAction_Implementation()
{
	if (FlyDirection.Z == -1) {
		Die();
	}
}

void ARavenrat::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 10;
	SuspicionOut.bSuspicionOnly = false;
}

void ARavenrat::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::EXPLICIT;
	SuspicionOut.DetectionAmount = 40;
	SuspicionOut.bSuspicionOnly = false;
}
