// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Magic/Grimoire.h"

#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "ShadowHand.generated.h"


UCLASS()
class UMBRANIGH_API AShadowHand : public AUmbraNighCharacter
{
	GENERATED_BODY()

private:
	AUmbraNighCharacter* BoundCaster;
	bool bDissapatePrimed = false;

public:
	AShadowHand();

	UPROPERTY(EditAnywhere)
		float BoundDistance = 1000.0F;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	void BindCaster(AUmbraNighCharacter* Spellcaster, UGrimoire* Spellbook);

	virtual void Die() override;
	
	virtual void DoSneak_Implementation() override;
	virtual void DoSneakEnd_Implementation() override;
	virtual void DoPrimaryAction_Implementation() override;
	virtual void DoSecondaryAction_Implementation() override;

	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
};
