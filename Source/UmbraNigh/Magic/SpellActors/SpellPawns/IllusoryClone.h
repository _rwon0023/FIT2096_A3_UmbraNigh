// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "IllusoryClone.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API AIllusoryClone : public AUmbraNighCharacter
{
	GENERATED_BODY()
	
public:
	AIllusoryClone();

	FTimerHandle DamagedDisperseTimerHandle;
	float DamageDisperseDuration = 3.0F;

	virtual void DoPrimaryAction_Implementation() override;
	virtual void DealDamage_Implementation(int ShotDamage) override;

	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
};
