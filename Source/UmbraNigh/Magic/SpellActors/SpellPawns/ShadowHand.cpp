// { Ryan Wong <32491034> 2022 }


#include "ShadowHand.h"

AShadowHand::AShadowHand()
{
	PrimaryActorTick.bCanEverTick = true;

	// Setup Smaller Hitbox Size
	GetCapsuleComponent()->InitCapsuleSize(40.0F, 40.0F);

	HP->InitialiseAsFull(1);

	Allegiance = EAllegiance::PLAYER;
}

void AShadowHand::BeginPlay()
{
	Super::BeginPlay();
}

void AShadowHand::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (BoundCaster) {
		FVector BoundLocation = BoundCaster->GetActorLocation();
		FVector DifferenceFromBound = GetActorLocation() - BoundLocation;
		if (DifferenceFromBound.Length() > BoundDistance) {
			DifferenceFromBound.Normalize(); // Get Direction
			SetActorLocation(BoundLocation + (DifferenceFromBound * BoundDistance));
		}
	}
}

void AShadowHand::BindCaster(AUmbraNighCharacter* Spellcaster, UGrimoire* Spellbook)
{
	BoundCaster = Spellcaster;
	if (BoundCaster) {
		// Add as restraint
		BoundCaster->Restraints.Add(this);
		// Set location to be in front of caster of spell
		float Offset = BoundCaster->GetCapsuleComponent()->GetScaledCapsuleRadius() + GetCapsuleComponent()->GetScaledCapsuleRadius();
		FVector CasterForward = BoundCaster->GetActorForwardVector();
		SetActorLocation(BoundCaster->GetActorLocation() + (CasterForward * Offset));
		// Rotate to start facing caster to avoid camera issues
		SetActorRotation((-CasterForward).ToOrientationRotator());

		// Possess this hand
		Spellbook->PossessNewPossessable(this);

		UE_LOG(LogTemp, Display, TEXT("Shadow Hand Bound to spellcaster <%s>"), *BoundCaster->GetActorNameOrLabel());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Shadow Hand failed to Bind to spellcaster"));
	}
}

void AShadowHand::Die()
{
	BoundCaster->Restraints.Remove(this);
	Super::Die();
}

void AShadowHand::DoSneak_Implementation()
{
	Super::DoSneak_Implementation();
	bDissapatePrimed = true;
}

void AShadowHand::DoSneakEnd_Implementation()
{
	Super::DoSneakEnd_Implementation();
	bDissapatePrimed = false;
}

void AShadowHand::DoPrimaryAction_Implementation()
{
	if (bDissapatePrimed) {
		BoundCaster->SetActorLocation(GetActorLocation() + BoundCaster->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());
		Die();
	}
}

void AShadowHand::DoSecondaryAction_Implementation()
{
	if (bDissapatePrimed) {
		Die();
	}
}

void AShadowHand::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 10;
	SuspicionOut.bSuspicionOnly = false;
}

void AShadowHand::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::EXPLICIT;
	SuspicionOut.DetectionAmount = 40;
	SuspicionOut.bSuspicionOnly = false;
}