// { Ryan Wong <32491034> 2022 }


#include "IllusoryDoll.h"
#include "UmbraNigh/Magic/Spells/IllusorySelfSpell.h"


void AIllusoryDoll::BeginPlay()
{
	Super::BeginPlay();
	LightDetector = Cast<ALightDetector>(GetWorld()->SpawnActor(ALightDetector::StaticClass()));
	if (LightDetector) {
		LightDetector->SetDetectorUniformScale(0.5F);
	}
	if (PS_TargetLocation) {
		TargetLocationParticles = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), PS_TargetLocation,FVector::ZeroVector);
	}
}

void AIllusoryDoll::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	FHitResult HitOut = DoCameraTrace();
	LightDetector->SetActorLocation(HitOut.ImpactPoint + FVector::UpVector*25);
	if (TargetLocationParticles) {
		TargetLocationParticles->SetWorldLocation(HitOut.ImpactPoint);
	}
	TargetLocationParticles->SetNiagaraVariableBool("Coalesce", LightDetector->bIsShadowed());
	GEngine->AddOnScreenDebugMessage((int32)(intptr_t)(this), 0.1f, FColor::White, FString::Printf(TEXT("Illusory Doll Light Value: <%f>"), LightDetector->CalculateNormalisedLightValue()));
}

void AIllusoryDoll::UnPossessed()
{
	if (TargetLocationParticles) { 
		TargetLocationParticles->Deactivate();
	}
	LightDetector->Destroy();

	Super::UnPossessed();
}

void AIllusoryDoll::LinkSpell(USpell* Spell, AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	if (Cast<UIllusorySelfSpell>(Spell)) {
		Super::LinkSpell(Spell, Caster, Spellbook);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Spell Link failed on {%s}"), *GetName());
	}
	
}

void AIllusoryDoll::DoPrimaryAction_Implementation()
{
	if (LightDetector && LightDetector->bIsShadowed()) {
		if (LinkedSpell && LinkedCharacter) {
			AIllusoryClone* NewClone = Cast<AIllusoryClone>(GetWorld()->SpawnActor(AIllusoryClone::StaticClass()));
			if (NewClone) {
				NewClone->CopyOther(LinkedCharacter, false);
				NewClone->SetActorLocation(LightDetector->GetActorLocation() + FVector::UpVector * NewClone->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());
				if (CloneMaterial) {
					for (size_t i = 0; i < NewClone->GetMesh()->GetNumMaterials(); i++)
					{
						NewClone->GetMesh()->SetMaterial(i, CloneMaterial);
					}
				}

				Cast<UIllusorySelfSpell>(LinkedSpell)->CurrentClone = NewClone;
				if (LinkedGrimoire) {
					LinkedGrimoire->AddPossessable(NewClone);
				}
			}
		}
		Destroy();
	}
}

void AIllusoryDoll::DoSecondaryAction_Implementation()
{
	if (bSneakKeyDown) {
		Destroy();
	}
}
