// { Ryan Wong <32491034> 2022 }


#include "BallisticVeil.h"

// Sets default values
ABallisticVeil::ABallisticVeil()
{
	PrimaryActorTick.bCanEverTick = true;

	// Create hitbox and set as root component
	Hitbox = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Collider"));
	RootComponent = Hitbox;
	// Assign hitbox to Shootable trace channel
	AssignColliderAsShootable(Hitbox);

	if (!PS_HitMist) {
		PS_HitMist = UBlueprintDefaultHelper::GetBlueprintObject<UNiagaraSystem>(EBlueprintDefaultHelperRef::PS_HIT_MIST);
	}

}

// Called when the game starts or when spawned
void ABallisticVeil::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABallisticVeil::AssignVeilToHitbox(UCapsuleComponent* ParentHitbox)
{
	// Set Hitbox up to size of parent hitbox + small offset
	Hitbox->InitCapsuleSize(ParentHitbox->GetScaledCapsuleRadius() + VEIL_OFFSET, ParentHitbox->GetScaledCapsuleHalfHeight() + VEIL_OFFSET);
	
	// Attach to parent hitbox
	AttachToComponent(ParentHitbox, FAttachmentTransformRules::SnapToTargetNotIncludingScale, NAME_None);

	Hitbox->RecreatePhysicsState();
}

void ABallisticVeil::Dissipate()
{
	UE_LOG(LogTemp, Display, TEXT("Ballistic Veil Dissipated"));
	Destroy();
}

void ABallisticVeil::OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy)
{
	if (PS_HitMist) {
		UNiagaraFunctionLibrary::SpawnSystemAttached(PS_HitMist, Hitbox, NAME_None, Hit.ImpactPoint, FRotator::ZeroRotator, EAttachLocation::KeepWorldPosition, true);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Ballistic Veil missing Particle System"));
	}

	if (!bDissipating) {
		UE_LOG(LogTemp, Display, TEXT("Ballistic Veil Dissipating"));
		bDissipating = true;
		GetWorld()->GetTimerManager().SetTimer(DissapateTimerHandle, this, &ABallisticVeil::Dissipate, DissapateDuration, true);
	}
}