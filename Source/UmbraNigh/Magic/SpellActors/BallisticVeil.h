// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "NiagaraFunctionLibrary.h"
#include "Components/CapsuleComponent.h"
#include "UmbraNigh/Common/BlueprintDefaultHelper.h"

#include "GameFramework/Actor.h"
#include "UmbraNigh/Interfaces/Shootable.h"
#include "BallisticVeil.generated.h"

UCLASS()
class UMBRANIGH_API ABallisticVeil : public AActor, public IShootable
{

	GENERATED_BODY()
	
private:
	inline static const float VEIL_OFFSET = 1.0F;
	FTimerHandle DissapateTimerHandle;
	bool bDissipating;
public:	
	// Sets default values for this actor's properties
	ABallisticVeil();
	
	UPROPERTY(EditAnywhere)
		UNiagaraSystem* PS_HitMist;

	UPROPERTY(EditAnywhere)
		float DissapateDuration = 2.5F;

	UPROPERTY(EditAnywhere)
		UCapsuleComponent* Hitbox;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	void AssignVeilToHitbox(UCapsuleComponent* ParentHitbox);
	UFUNCTION()
		void Dissipate();
	virtual void OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy) override;



};
