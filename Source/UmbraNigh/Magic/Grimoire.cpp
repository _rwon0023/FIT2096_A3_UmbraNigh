// { Ryan Wong <32491034> 2022 }


#include "Grimoire.h"

// Sets default values for this component's properties
UGrimoire::UGrimoire()
{
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UGrimoire::AssignSpells(TArray<USpell*> SpellArray)
{
	for (USpell* Spell : SpellArray) {
		if (Spell) {
			Spells.Add(Spell->GetSpellSchool(), Spell);
		}
	}
}

bool UGrimoire::bIsSpellCurrentlyViable(ESpellSchool SpellIdentifier)
{
	if (bSpellcastingEnabled && IsValid(Spellcaster) && Spells.Contains(SpellIdentifier)) {
		return Spells[SpellIdentifier]->bIsCastViable(Spellcaster);
	}
	return false;
}

void UGrimoire::CastSpellIfViable(ESpellSchool SpellIdentifier)
{
	if (bIsSpellCurrentlyViable(SpellIdentifier)) {
		Spells[SpellIdentifier]->ExecuteCast(Spellcaster, this);
	}
}

TScriptInterface<IPlayable> UGrimoire::GetCurrentPossessed()
{
	if (Possessables.Num() > 0 && IsValid(Spellcaster)) {
		if(CurrentPossessedIndex < Possessables.Num()){
			TScriptInterface<IPlayable> CurrentPossessed = Possessables[CurrentPossessedIndex];
			if (CurrentPossessed && IsValid(CurrentPossessed.GetObject())) {
				UE_LOG(LogTemp, Display, TEXT("Current Possessed (%s) is valid"), *CurrentPossessed.GetObjectRef()->GetName());
				return Possessables[CurrentPossessedIndex];
			}
		}
		ValidateFilterPossessables();
		UE_LOG(LogTemp, Display, TEXT("Current Possessed was not avaliable, Reassigning..."));
		CurrentPossessedIndex = 0;
		if (Possessables.Num() == 0) {
			Possessables.Add(Spellcaster);
		}
		return Possessables[0];
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("{%s} does not have a valid current possessable"), *GetName());
		return nullptr;
	}
}

TArray<TScriptInterface<IPlayable>> UGrimoire::GetAllPossessables()
{	
	ValidateFilterPossessables();
	return Possessables;
}

TScriptInterface<IPlayable> UGrimoire::GetPossessableAt(int PossessableIndex)
{
	ValidateFilterPossessables();
	return (PossessableIndex >= 0 && PossessableIndex < Possessables.Num()) ? GetAllPossessables()[PossessableIndex] : nullptr;
}

void UGrimoire::PossessNewPossessable(TScriptInterface<IPlayable> NewPossessable)
{
	ValidateFilterPossessables();
	int NewPossessionIndex = Possessables.Num();
	AddPossessable(NewPossessable);
	PossessAtIndex(NewPossessionIndex);
}

void UGrimoire::AddPossessable(TScriptInterface<IPlayable> NewPossessable)
{
	ValidateFilterPossessables();
	Possessables.Add(NewPossessable);
}

void UGrimoire::AddPossessableFront(TScriptInterface<IPlayable> NewPossessable)
{
	if (NewPossessable && IsValid(NewPossessable.GetObject())) {
		ValidateFilterPossessables();
		Possessables.Insert(NewPossessable, 0);
	}
}

void UGrimoire::PossessAtIndex(int PossessIndex)
{
	if (PossessIndex >= 0 && PossessIndex < Possessables.Num()) {
		CurrentPossessedIndex = PossessIndex;
		PossessionChangedDelegate.Broadcast();
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("{%s} tried to Possess Possessable at invalid index <%d>"), *GetName(), PossessIndex);
	}
}


void UGrimoire::RemovePossessable(TScriptInterface<IPlayable> LostPossessable)
{
	int LostIndex;
	if (Possessables.Find(LostPossessable, LostIndex)) {
		Possessables.RemoveAt(LostIndex);
		if (LostIndex == CurrentPossessedIndex) {
			PossessAtIndex(0);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("{%s} failed to unpossess"), *GetName());
	}

}

void UGrimoire::ValidateFilterPossessables()
{
	if (Possessables.Num() > 0) {
		for (int i = Possessables.Num()-1; i >= 0; i--)
		{
			if (!Possessables[i] || !IsValid(Possessables[i].GetObject())) {
				Possessables.RemoveAt(i);
			}
		}
	}
}

void UGrimoire::CastInitialSpells()
{
	for (TPair<ESpellSchool, USpell*> Spell : Spells) {
		if (Spell.Value && Spell.Value->bShouldExecuteOnStart) {
			Spell.Value->ExecuteCast(Spellcaster, this);
		}
	}
}
