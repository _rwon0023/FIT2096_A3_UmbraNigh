// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Spells/Spell.h"
#include "UmbraNigh/Interfaces/Playable.h"

#include "Components/ActorComponent.h"
#include "Grimoire.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPossessionChange);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UMBRANIGH_API UGrimoire : public UActorComponent
{
	GENERATED_BODY()

private:
	int CurrentPossessedIndex;

public:
	UGrimoire();

	bool bSpellcastingEnabled = true;

	FOnPossessionChange PossessionChangedDelegate;

	UPROPERTY(VisibleAnywhere)
		AUmbraNighCharacter* Spellcaster = nullptr;

	UPROPERTY(VisibleAnywhere)
		TArray<TScriptInterface<IPlayable>> Possessables;

	UPROPERTY(VisibleAnywhere)
		TMap<ESpellSchool, USpell*> Spells;

public:	
	void AssignSpells(TArray<USpell*> SpellArray);
	bool bIsSpellCurrentlyViable(ESpellSchool SpellIdentifier);
	UFUNCTION()
	void CastSpellIfViable(ESpellSchool SpellIdentifier);

	TScriptInterface<IPlayable> GetCurrentPossessed();
	TArray<TScriptInterface<IPlayable>> GetAllPossessables();
	TScriptInterface<IPlayable> GetPossessableAt(int PossessableIndex);
	void PossessNewPossessable(TScriptInterface<IPlayable> NewPossessable);
	void AddPossessable(TScriptInterface<IPlayable> NewPossessable);
	void AddPossessableFront(TScriptInterface<IPlayable> NewPossessable);
	void PossessAtIndex(int PossessIndex);
	void RemovePossessable(TScriptInterface<IPlayable> LostPossessable);
	void ValidateFilterPossessables();

	void CastInitialSpells();
		
};
