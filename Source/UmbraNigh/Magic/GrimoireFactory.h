// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Magic/Spells/NightBulletSpell.h"
#include "UmbraNigh/Magic/Spells/BallisticVeilSpell.h"
#include "UmbraNigh/Magic/Spells/ShadowHandSpell.h"
#include "UmbraNigh/Magic/Spells/PolymorphRavenratSpell.h"
#include "UmbraNigh/Magic/Spells/IllusorySelfSpell.h"
#include "UmbraNigh/Magic/Spells/SleepSpell.h"
#include "UmbraNigh/Magic/Spells/AnimateDeadSpell.h"
#include "UmbraNigh/Magic/Spells/DarkTagSpell.h"

#include "UmbraNigh/Magic/Grimoire.h"

#include "UObject/NoExportTypes.h"
#include "GrimoireFactory.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API UGrimoireFactory : public UObject
{
	GENERATED_BODY()
	
public:
	static const void GenerateDefaultGrimoire(UGrimoire*& GrimoirePtrRef);
};
