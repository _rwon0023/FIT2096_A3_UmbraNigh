	// { Ryan Wong <32491034> 2022 }


#include "BallisticVeilSpell.h"

UBallisticVeilSpell::UBallisticVeilSpell()
{
	bShouldExecuteOnStart = true;

	if (!PS_Spellcasting) {
		PS_Spellcasting = UBlueprintDefaultHelper::GetBlueprintObject<UNiagaraSystem>(EBlueprintDefaultHelperRef::PS_DARK_COALESCE);
	}
}

ESpellSchool UBallisticVeilSpell::GetSpellSchool()
{
	return ESpellSchool::ABJURATION;
}

bool UBallisticVeilSpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
	return (!bWeaveStarted)
		&& (Caster->GetLightDetector()->bIsShadowed())
		&& (!IsValid(CurrentVeil));
}

void UBallisticVeilSpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	if (!bWeaveStarted && !IsValid(CurrentVeil) && IsValid(Caster)) {
		bWeaveStarted = true;
		VeilTarget = Caster;

		VeilTarget->Restraints.Add(this);

		if (PS_Spellcasting) {
			WeaveParticles = UNiagaraFunctionLibrary::SpawnSystemAttached(PS_Spellcasting, Caster->GetMesh(), NAME_None, FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, true);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Ballistic Veil Spell missing Spellweave Particle System"));
		}

		Caster->GetWorld()->GetTimerManager().SetTimer(VeilWeaveTimerHandle, this, &UBallisticVeilSpell::CreateVeilOnTarget, WeaveDuration, false);
	}
}

void UBallisticVeilSpell::CreateVeilOnTarget()
{
	UE_LOG(LogTemp, Display, TEXT("Ballistic Veil Spell Creating Veil"));
	
	if (bWeaveStarted) {
		bWeaveStarted = false;
		
		if (IsValid(WeaveParticles)) {
			WeaveParticles->Deactivate();
		}

		VeilTarget->Restraints.Remove(this);
	}

	if (!IsValid(CurrentVeil)) {
		AActor* NewVeil = VeilTarget->GetWorld()->SpawnActor(ABallisticVeil::StaticClass());
		CurrentVeil = Cast<ABallisticVeil>(NewVeil);
		if (CurrentVeil) {
			UCapsuleComponent* Hitbox = VeilTarget->GetCapsuleComponent();
			CurrentVeil->AssignVeilToHitbox(Hitbox);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Problem spawning ballistic veil"));
		}
	}
}
