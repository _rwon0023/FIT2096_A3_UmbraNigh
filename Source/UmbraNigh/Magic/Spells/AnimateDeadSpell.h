// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/DeadBody.h"
#include "UmbraNigh/Magic/Grimoire.h"

#include "Spell.h"
#include "AnimateDeadSpell.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API UAnimateDeadSpell : public USpell
{
	GENERATED_BODY()

public:
	float RaiseRange = 300.0F;

	virtual ESpellSchool GetSpellSchool() override;
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster) override;
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook) override;

	ADeadBody* SearchForCorpse(AUmbraNighCharacter* Origin);
};
