// { Ryan Wong <32491034> 2022 }


#include "Spell.h"
#include "UmbraNigh/Magic/Grimoire.h"

void USpell::ConstructorAssignSummonParticles()
{
    if (!PS_SummonCloud) {
        PS_SummonCloud = UBlueprintDefaultHelper::GetBlueprintObject<UNiagaraSystem>(EBlueprintDefaultHelperRef::PS_SUMMON_CLOUD);
    }
    if (!IsValid(PS_SummonCloud)) {
        UE_LOG(LogTemp, Warning, TEXT("Failed to Assign Summon Particles"));
    }
}

ESpellSchool USpell::GetSpellSchool()
{
    return ESpellSchool::NONE;
}

bool USpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
    return false;
}

void USpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
}
