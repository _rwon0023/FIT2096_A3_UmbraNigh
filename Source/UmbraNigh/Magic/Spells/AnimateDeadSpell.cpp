// { Ryan Wong <32491034> 2022 }


#include "AnimateDeadSpell.h"

ESpellSchool UAnimateDeadSpell::GetSpellSchool()
{
	return ESpellSchool::NECROMANCY;
}

bool UAnimateDeadSpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
	return (Caster->GetLightDetector()->bIsShadowed())
		&& (SearchForCorpse(Caster));
}

void UAnimateDeadSpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	ADeadBody* DetectedCorpse = SearchForCorpse(Caster);
	if (DetectedCorpse) {
		Spellbook->AddPossessable(DetectedCorpse->GetBodyCharacter());
		DetectedCorpse->UnlimpBodyCharacter();
	}
}

ADeadBody* UAnimateDeadSpell::SearchForCorpse(AUmbraNighCharacter* Origin)
{
	FVector OriginLocation = Origin->GetActorLocation();

	// Set up Trace Params
	FCollisionQueryParams TraceParams(FName(TEXT("Animate Dead Trace")), true, Origin);
	TraceParams.bReturnPhysicalMaterial = false;
	TArray<FOverlapResult> HitOuts;
	HitOuts.Empty();
	/* Debugging */
	const FName TraceTag("AnimateDeadTrace");
	Origin->GetWorld()->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;
	//*/

	// Perform Trace/Overlap
	Origin->GetWorld()->OverlapMultiByChannel(HitOuts, OriginLocation, FQuat::Identity, ECollisionChannel::ECC_Visibility, FCollisionShape::MakeSphere(RaiseRange), TraceParams);

	for (FOverlapResult HitOut : HitOuts) {
		ADeadBody* DetectedCorpse = Cast<ADeadBody>(HitOut.GetActor());
		if (DetectedCorpse) {
			return DetectedCorpse;
		}
	}

	return nullptr;
}
