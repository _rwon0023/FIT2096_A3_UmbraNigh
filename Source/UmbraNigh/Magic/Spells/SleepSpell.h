// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Magic/SpellActors/SpellPawns/SleepFeather.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

#include "Spell.h"
#include "SleepSpell.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API USleepSpell : public USpell
{
	GENERATED_BODY()
	
private:
	ASleepFeather* ActiveSpellpawn;
public:
	USleepSpell();

	TSubclassOf<ASleepFeather> SleepFeatherClass;

	TMap<AUmbraNighCharacter*, UNiagaraComponent*> MarkedCharacters;
	int MarksRemaining;
	int StartingMarks = 10;

	virtual ESpellSchool GetSpellSchool() override;
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster) override;
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook) override;

	bool bCanMark();
	void PerformSleep();
	void MarkCharacter(AUmbraNighCharacter* Marked, UNiagaraSystem* ParticleEffect);
};
