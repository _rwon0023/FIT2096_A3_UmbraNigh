// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Magic/SpellActors/SpellPawns/Ravenrat.h"

#include "Spell.h"
#include "PolymorphRavenratSpell.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API UPolymorphRavenratSpell : public USpell
{
	GENERATED_BODY()

public:
	UPolymorphRavenratSpell();

	UPROPERTY(VisibleAnywhere)
		ARavenrat* CurrentRavenrat;

	TSubclassOf<ARavenrat> RavenratClass;

	virtual ESpellSchool GetSpellSchool() override;
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster) override;
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook) override;
};
