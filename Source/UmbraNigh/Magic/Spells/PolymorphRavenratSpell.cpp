// { Ryan Wong <32491034> 2022 }


#include "PolymorphRavenratSpell.h"

UPolymorphRavenratSpell::UPolymorphRavenratSpell()
{
	ConstructorAssignSummonParticles();
	if (!RavenratClass) {
		RavenratClass = UBlueprintDefaultHelper::GetDefaultClass<ARavenrat>(EBlueprintDefaultHelperRef::CLASS_RAVENRAT_PAWN);
	}
}

ESpellSchool UPolymorphRavenratSpell::GetSpellSchool()
{
	return ESpellSchool::TRANSMUTATION;
}

bool UPolymorphRavenratSpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
	return (Caster->bIsEnabled())
		&& (!Caster->bIsImmobile())
		&& (!Caster->bIsHandsOccupied())
		&& (Caster->GetLightDetector()->bIsShadowed())
		&& (!IsValid(CurrentRavenrat));
}

void UPolymorphRavenratSpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	if (!IsValid(CurrentRavenrat)) {
		AActor* NewRavenrat = Caster->GetWorld()->SpawnActor(RavenratClass.Get());
		CurrentRavenrat = Cast<ARavenrat>(NewRavenrat);
		if (CurrentRavenrat) {
			CurrentRavenrat->PolymorphCaster(Caster, Spellbook);
			
			if(PS_SummonCloud) UNiagaraFunctionLibrary::SpawnSystemAtLocation(Caster->GetWorld(), PS_SummonCloud, CurrentRavenrat->GetActorLocation());
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Problem spawning Ravenrat"));
		}
	}
}
