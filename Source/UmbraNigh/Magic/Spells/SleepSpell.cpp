// { Ryan Wong <32491034> 2022 }


#include "SleepSpell.h"

USleepSpell::USleepSpell()
{
	if (!SleepFeatherClass) {
		SleepFeatherClass = UBlueprintDefaultHelper::GetDefaultClass<ASleepFeather>(EBlueprintDefaultHelperRef::CLASS_SLEEPFEATHER_PAWN);
	}
	MarksRemaining = StartingMarks;
}

ESpellSchool USleepSpell::GetSpellSchool()
{
	return ESpellSchool::ENCHANTMENT;
}

bool USleepSpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
	return (!IsValid(ActiveSpellpawn))
		&& (bCanMark() || !MarkedCharacters.IsEmpty());
}

void USleepSpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	AActor* NewSpellpawn = Caster->GetWorld()->SpawnActor(SleepFeatherClass.Get());
	ActiveSpellpawn = Cast<ASleepFeather>(NewSpellpawn);
	if (ActiveSpellpawn) {
		ActiveSpellpawn->LinkSpell(this, Caster, Spellbook);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Problem spawning Sleep Feather"));
	}
}

bool USleepSpell::bCanMark()
{
	return (MarksRemaining > 0);
}

void USleepSpell::PerformSleep()
{
	for (TPair<AUmbraNighCharacter*, UNiagaraComponent*> Marked : MarkedCharacters) {
		Marked.Key->Sleep();
		Marked.Value->Deactivate();
	}
	MarkedCharacters.Empty();
	UE_LOG(LogTemp, Display, TEXT("{%s} Performed Sleep cast"), *GetName());
}

void USleepSpell::MarkCharacter(AUmbraNighCharacter* Marked, UNiagaraSystem* ParticleEffect)
{
	if (bCanMark() && Marked && !MarkedCharacters.Contains(Marked)) {
		MarksRemaining--;

		UNiagaraComponent* Particles = UNiagaraFunctionLibrary::SpawnSystemAttached(ParticleEffect, Marked->GetMesh(), NAME_None, FVector::UpVector*(Marked->GetCapsuleComponent()->GetScaledCapsuleHalfHeight() * 2 + 10), FRotator::ZeroRotator, EAttachLocation::Type::KeepRelativeOffset, true);

		MarkedCharacters.Add(Marked, Particles);
		UE_LOG(LogTemp, Display, TEXT("{%s} Marked Character <%s>"), *GetName(), *Marked->GetActorNameOrLabel());
	}
}
