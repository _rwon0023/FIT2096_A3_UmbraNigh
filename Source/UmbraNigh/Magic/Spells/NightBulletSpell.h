// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "Spell.h"
#include "NightBulletSpell.generated.h"

UCLASS()
class UMBRANIGH_API UNightBulletSpell : public USpell
{
	GENERATED_BODY()
	
public:
	UNightBulletSpell();
	virtual ESpellSchool GetSpellSchool() override;
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster) override;
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook) override;
};
