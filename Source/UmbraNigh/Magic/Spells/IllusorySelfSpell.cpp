// { Ryan Wong <32491034> 2022 }


#include "IllusorySelfSpell.h"

UIllusorySelfSpell::UIllusorySelfSpell()
{
	if (!IllusoryDollClass) {
		IllusoryDollClass = UBlueprintDefaultHelper::GetDefaultClass<AIllusoryDoll>(EBlueprintDefaultHelperRef::CLASS_ILLUSORYDOLL_PAWN);
	}
}

ESpellSchool UIllusorySelfSpell::GetSpellSchool()
{
	return ESpellSchool::ILLUSION;
}

bool UIllusorySelfSpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
	return (!IsValid(ActiveDoll))
		&& (!IsValid(CurrentClone));
}

void UIllusorySelfSpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	BoundGrimoire = Spellbook;
	if (!IsValid(ActiveDoll)) {
		AActor* NewDoll = Caster->GetWorld()->SpawnActor(IllusoryDollClass.Get());
		ActiveDoll = Cast<AIllusoryDoll>(NewDoll);
		if (ActiveDoll) {
			ActiveDoll->LinkSpell(this,Caster,Spellbook);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Problem spawning Illusory Doll"));
		}
	}
}