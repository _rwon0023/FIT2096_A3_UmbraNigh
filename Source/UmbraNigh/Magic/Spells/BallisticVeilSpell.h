// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "UmbraNigh/Common/BlueprintDefaultHelper.h"
#include "UmbraNigh/Magic/SpellActors/BallisticVeil.h"

#include "Spell.h"
#include "BallisticVeilSpell.generated.h"


UCLASS()
class UMBRANIGH_API UBallisticVeilSpell : public USpell
{
	GENERATED_BODY()

private:
	ABallisticVeil* CurrentVeil;
	bool bWeaveStarted = false;
	UNiagaraComponent* WeaveParticles;
	AUmbraNighCharacter* VeilTarget;
	
public:
	UBallisticVeilSpell();

	FTimerHandle VeilWeaveTimerHandle;

	float WeaveDuration = 5.0F;

	UPROPERTY(EditAnywhere)
		UNiagaraSystem* PS_Spellcasting;

	virtual ESpellSchool GetSpellSchool() override;
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster) override;
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook) override;

	void CreateVeilOnTarget();
};
