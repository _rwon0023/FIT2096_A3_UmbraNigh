// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Common/BlueprintDefaultHelper.h"
#include "UmbraNigh/Magic/SpellActors/SpellPawns/ShadowHand.h"

#include "Spell.h"
#include "ShadowHandSpell.generated.h"


UCLASS()
class UMBRANIGH_API UShadowHandSpell : public USpell
{
	GENERATED_BODY()
	
private:
	AShadowHand* CurrentShadowHand;
public:
	UShadowHandSpell();

	TSubclassOf<AShadowHand> ShadowHandClass;

	virtual ESpellSchool GetSpellSchool() override;
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster) override;
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook) override;
};

