// { Ryan Wong <32491034> 2022 }


#include "ShadowHandSpell.h"

UShadowHandSpell::UShadowHandSpell()
{
	ConstructorAssignSummonParticles();
	if (!ShadowHandClass) {
		ShadowHandClass = UBlueprintDefaultHelper::GetDefaultClass<AShadowHand>(EBlueprintDefaultHelperRef::CLASS_SHADOW_HAND_PAWN);
	}
}

ESpellSchool UShadowHandSpell::GetSpellSchool()
{
    return ESpellSchool::CONJURATION;
}

bool UShadowHandSpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
	return (Caster->GetLightDetector()->bIsShadowed())
		&& (!IsValid(CurrentShadowHand));
}

void UShadowHandSpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	if (!IsValid(CurrentShadowHand)) {
		AActor* NewHand = Caster->GetWorld()->SpawnActor(ShadowHandClass.Get());
		CurrentShadowHand = Cast<AShadowHand>(NewHand);
		if (CurrentShadowHand) {
			CurrentShadowHand->BindCaster(Caster, Spellbook);
			
			if (PS_SummonCloud) UNiagaraFunctionLibrary::SpawnSystemAtLocation(Caster->GetWorld(), PS_SummonCloud, CurrentShadowHand->GetActorLocation());
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Problem spawning shadow hand"));
		}
	}
}