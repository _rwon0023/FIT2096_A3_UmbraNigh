// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Common/BlueprintDefaultHelper.h"
#include "UmbraNigh/Magic/Grimoire.h"
#include "UmbraNigh/Magic/SpellActors/SpellPawns/IllusoryDoll.h"
#include "UmbraNigh/Magic/SpellActors/SpellPawns/IllusoryClone.h"

#include "Spell.h"
#include "IllusorySelfSpell.generated.h"


UCLASS()
class UMBRANIGH_API UIllusorySelfSpell : public USpell
{
	GENERATED_BODY()

private:
	AIllusoryDoll* ActiveDoll;
public:
	UIllusorySelfSpell();

	TSubclassOf<AIllusoryDoll> IllusoryDollClass;
	UGrimoire* BoundGrimoire;
	AIllusoryClone* CurrentClone;

	virtual ESpellSchool GetSpellSchool() override;
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster) override;
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook) override;
};
