// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Magic/SpellActors/SpellPawns/DarkTag.h"

#include "Spell.h"
#include "DarkTagSpell.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API UDarkTagSpell : public USpell
{
	GENERATED_BODY()

private:
	UMaterialInstanceDynamic* TagStencilMaterialDyanamic;

	ADarkTag* ActiveSpellpawn;
	AUmbraNighCharacter* LastKnownCatser;

public:
	UDarkTagSpell();

	AActor* TaggedTarget;
	TArray<UMeshComponent*> TaggedMeshes;

	TSubclassOf<ADarkTag> DarkTagClass;

	virtual ESpellSchool GetSpellSchool();
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster);
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook);
};
