// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "NiagaraFunctionLibrary.h"
#include "UmbraNigh/Common/BlueprintDefaultHelper.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"

#include "UObject/NoExportTypes.h"
#include "Spell.generated.h"

UENUM()
enum class ESpellSchool : uint8 {
	NONE,
	EVOCATION,
	ABJURATION,
	CONJURATION,
	TRANSMUTATION,
	ILLUSION,
	ENCHANTMENT,
	NECROMANCY,
	DIVINATION
};

// Forward Declarations
class UGrimoire;

UCLASS(Abstract)
class UMBRANIGH_API USpell : public UObject
{
	GENERATED_BODY()
	
protected:
	UNiagaraSystem* PS_SummonCloud;
	void ConstructorAssignSummonParticles();

public:
	bool bShouldExecuteOnStart;
	virtual ESpellSchool GetSpellSchool();
	virtual bool bIsCastViable(AUmbraNighCharacter* Caster);
	virtual void ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook);

	
};
