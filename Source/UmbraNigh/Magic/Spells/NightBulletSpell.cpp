// { Ryan Wong <32491034> 2022 }


#include "NightBulletSpell.h"

UNightBulletSpell::UNightBulletSpell()
{
	bShouldExecuteOnStart = true;
}

ESpellSchool UNightBulletSpell::GetSpellSchool()
{
	return ESpellSchool::EVOCATION;
}

bool UNightBulletSpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
	// Return true if caster is in shadows and has firearm with the appropriate ammo type which is not at capacity
	return (Caster->GetLightDetector()->bIsShadowed()) 
		&& (Caster->Firearm) 
		&& (Caster->Firearm->AmmoType == EAmmoType::NIGHT_BULLET) 
		&& (!Caster->Firearm->bIsAmmoFull());
}

void UNightBulletSpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	// Reverify minimum requirements
	AFirearm* CasterFirearm = Caster->Firearm;
	if ((CasterFirearm) && (CasterFirearm->AmmoType == EAmmoType::NIGHT_BULLET)) {
		CasterFirearm->Reload(); // Reload gun
	}
}
