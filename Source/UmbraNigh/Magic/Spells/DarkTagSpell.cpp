// { Ryan Wong <32491034> 2022 }


#include "DarkTagSpell.h"

UDarkTagSpell::UDarkTagSpell()
{
	if (!DarkTagClass) {
		DarkTagClass = UBlueprintDefaultHelper::GetDefaultClass<ADarkTag>(EBlueprintDefaultHelperRef::CLASS_DARKTAG_PAWN);
	}
}

ESpellSchool UDarkTagSpell::GetSpellSchool()
{
	return ESpellSchool::DIVINATION;
}

bool UDarkTagSpell::bIsCastViable(AUmbraNighCharacter* Caster)
{
	return (!IsValid(ActiveSpellpawn));
}

void UDarkTagSpell::ExecuteCast(AUmbraNighCharacter* Caster, UGrimoire* Spellbook)
{
	AActor* NewSpellpawn = Caster->GetWorld()->SpawnActor(DarkTagClass.Get());
	ActiveSpellpawn = Cast<ADarkTag>(NewSpellpawn);
	if (ActiveSpellpawn) {
		ActiveSpellpawn->LinkSpell(this, Caster, Spellbook);

		if (ActiveSpellpawn->TagStencilMaterial) {
			if (LastKnownCatser != Caster) {
				LastKnownCatser = Caster;
				if (!TagStencilMaterialDyanamic) {
					TagStencilMaterialDyanamic = NewObject<UMaterialInstanceDynamic>();
				}
				TagStencilMaterialDyanamic->Parent = ActiveSpellpawn->TagStencilMaterial;
				TagStencilMaterialDyanamic->SetScalarParameterValue(FName(TEXT("Stencil ID")), ActiveSpellpawn->STENCIL_VALUE); // Set Stencil ID to constant STENCIL_VALUE used by this class
				Caster->GetCamera_Implementation()->AddOrUpdateBlendable(TagStencilMaterialDyanamic);
			}
			if (TagStencilMaterialDyanamic) {
				ActiveSpellpawn->CameraRig->FollowCamera->AddOrUpdateBlendable(TagStencilMaterialDyanamic);
			}
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Problem spawning DarkTag Spellpawn"));
	}
}
