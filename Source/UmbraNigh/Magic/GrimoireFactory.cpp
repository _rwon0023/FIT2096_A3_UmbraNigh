// { Ryan Wong <32491034> 2022 }


#include "GrimoireFactory.h"

const void UGrimoireFactory::GenerateDefaultGrimoire(UGrimoire*& GrimoirePtrRef)
{
	if (!GrimoirePtrRef) {
		GrimoirePtrRef = NewObject<UGrimoire>();

		const TArray<USpell*> SpellList = {
			NewObject<UNightBulletSpell>(),
			NewObject<UBallisticVeilSpell>(),
			NewObject<UShadowHandSpell>(),
			NewObject<UPolymorphRavenratSpell>(),
			NewObject<UIllusorySelfSpell>(),
			NewObject<USleepSpell>(),
			NewObject<UAnimateDeadSpell>(),
			NewObject<UDarkTagSpell>()
		};

		GrimoirePtrRef->AssignSpells(SpellList);
	}

	if (IsValid(GrimoirePtrRef)) {
		UE_LOG(LogTemp, Display, TEXT("Grimoire <%s> successfully initialised"), *GrimoirePtrRef->GetName());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to initialise Grimoire"));
	}
}

