// { Ryan Wong <32491034> 2022 }


#include "Undead.h"

void AUndead::TakeOverCharacter(AUmbraNighCharacter* TargetCharacter)
{
	if (TargetCharacter) {
		CopyOther(TargetCharacter, true);
		SetActorTransform(TargetCharacter->GetActorTransform());
		EnsureChildActorsInitialised();
		DisguiseAllegiance = TargetCharacter->Allegiance;
		Allegiance = DisguiseAllegiance;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("{%s} failed to take over character: TargetCharacter is invalid"));
;	}
}

bool AUndead::CanInteract_Implementation(TScriptInterface<IInteractable>& Interactable)
{
	return !IInteractable::Execute_bIsMainPlayerExclusive(Interactable.GetObjectRef());
}

void AUndead::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	if (FVector::Distance(Detector->GetActorLocation(), GetActorLocation()) > RecogniseRange) {
		Allegiance = DisguiseAllegiance;
		return;
	}

	Allegiance = EAllegiance::PLAYER;

	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 10;
	SuspicionOut.bSuspicionOnly = true;

	Super::GetInitialDetectability_Implementation(SuspicionOut, Detector);
}

void AUndead::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{

	SuspicionOut.SuspicionType = ESuspicionType::EXPLICIT;
	SuspicionOut.DetectionAmount = 50;
	SuspicionOut.bSuspicionOnly = true;

	Super::GetDetectability_Implementation(SuspicionOut, Detector);
}