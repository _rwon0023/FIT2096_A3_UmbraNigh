// { Ryan Wong <32491034> 2022 }


#include "LimpBody.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "UmbraNigh/Environment/LargeContainer.h"


ALimpBody::ALimpBody()
{
	InteractionTrigger = CreateDefaultSubobject<USphereComponent>(TEXT("Interaction Trigger"));
	InteractionTrigger->SetupAttachment(Mesh);
	InteractionTrigger->SetCollisionProfileName(TEXT("Trigger"));
}

void ALimpBody::BeginPlay()
{
	Super::BeginPlay();
}

AUmbraNighCharacter* ALimpBody::GetBodyCharacter()
{
	return BodyCharacter;
}

void ALimpBody::SetBodyCharacter(AUmbraNighCharacter* Character)
{
	if (IsValid(Character)) {
		BodyCharacter = Character;
		
		SetActorTransform(Character->GetActorTransform());

		Character->GetMesh()->SetRenderCustomDepth(false);

		USkeletalMeshComponent* SkeletalMesh = DuplicateObject<USkeletalMeshComponent>(Character->GetMesh(), this);
		if (SkeletalMesh) {
			Mesh->DestroyComponent();

			SkeletalMesh->SetAnimInstanceClass(nullptr);
			SkeletalMesh->SetCollisionProfileName(TEXT("Ragdoll"));
			SkeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
			SkeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

			Mesh = SkeletalMesh;
			InitialiseAssignedMesh();
			Mesh->RegisterComponent();
		}

		// Refresh Body container
		if (BodyContainer) {
			BodyContainer->Destroy();
		}
		BodyContainer = NewObject<ALargeContainer>(this);
		BodyContainer->AttachToComponent(Mesh, FAttachmentTransformRules::SnapToTargetIncludingScale);

		// Configure Interaction Trigger Sphere
		float CharacterHalfHeight = Character->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
		InteractionTrigger->SetRelativeLocation(Character->GetActorUpVector() * CharacterHalfHeight);
		InteractionTrigger->InitSphereRadius(CharacterHalfHeight);
		InteractionTrigger->AttachToComponent(Mesh, FAttachmentTransformRules::KeepRelativeTransform);

		// Disable and contain character
		BodyContainer->ContainCharacter(BodyCharacter);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("{%s} failed to set body character, given character was invalid"), *GetActorNameOrLabel())
	}
}

void ALimpBody::UnlimpBodyCharacter()
{
	if (BodyCharacter && BodyContainer) {
		BodyContainer->ReleaseCharacter();
		BodyCharacter->SetActorLocationAndRotation(Mesh->GetComponentLocation(),FQuat::Identity);
	}
	Destroy();
}

void ALimpBody::Interact_Implementation(AUmbraNighCharacter* Interactor)
{
	UE_LOG(LogTemp, Display, TEXT("<%s> interacts with Limp Body {%s}"), *Interactor->GetActorNameOrLabel(), *GetActorNameOrLabel());
	
	if (Interactor->bIsDraggingBody()) {
		Interactor->Drop();
	}
	else {
		Interactor->InteractionOverrides.Add(GetInteractionType_Implementation(), this);
		Interactor->DragBody(this);
	}
}

EInteractionType ALimpBody::GetInteractionType_Implementation()
{
	return EInteractionType::HANDLE;
}
