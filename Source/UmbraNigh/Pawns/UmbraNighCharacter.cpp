// Fill out your copyright notice in the Description page of Project Settings.


#include "UmbraNighCharacter.h"

// Sets default values
AUmbraNighCharacter::AUmbraNighCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	// ===== Body

	UCapsuleComponent* Hitbox = GetCapsuleComponent();

	// Setup Hitbox Size
	Hitbox->InitCapsuleSize(40.0F, 90.0F);
	// Make able to be stepped on
	Hitbox->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_Yes;
	// Assign hitbox as Shootable
	AssignColliderAsShootable(Hitbox);
	// Set other hitbox collision channel/s
	// Hitbox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	
	// Attach Head hitbox (Position and Scale in Blueprint or subclass)
	HeadshotChecker = CreateDefaultSubobject<UHeadshotChecker>(TEXT("Head Collider"));
	HeadshotChecker->SetupAttachment(Hitbox);
	HeadshotChecker->AssignParentCollider(Hitbox); // Assign hitbox as parent collider

	// Set Mesh Position
	GetMesh()->SetRelativeLocation(FVector::UpVector * (-100));

	// Note: Skeletal Mesh and Anim Blueprint References to be set in Blueprint

	// ===== Camera

	CameraRig = CreateDefaultSubobject<UPawnCameraRig>(TEXT("Camera Rig"));
	CameraRig->SetupAttachment(GetMesh());

	// ===== Character Movement Component

	// Face direction of movement at given rate	
	GetCharacterMovement()->bOrientRotationToMovement = true; 
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f);

	// Configure movement parameters
	GetCharacterMovement()->JumpZVelocity = 400.0F;
	GetCharacterMovement()->AirControl = 0.4F;
	GetCharacterMovement()->MaxWalkSpeed = 500.0F;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.0F;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.0F;
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;
	GetCharacterMovement()->bRunPhysicsWithNoController = true;
	GetCharacterMovement()->bRequestedMoveUseAcceleration = true;
	GetCharacterMovement()->SetCrouchedHalfHeight(45.0F);

	// ===== HP

	// Create and add HP component
	HP = CreateDefaultSubobject<UDepletableValue>(TEXT("HP"));
	AddOwnedComponent(HP);
	// Bind death function to when HP is depleated
	HP->DepletedDelegate.AddDynamic(this, &AUmbraNighCharacter::Die);

	// ===== Light Detector

	// Create and Attach Child Actor Component for Light Detector
	LightDetectorComponent = CreateDefaultSubobject<UChildActorComponent>(TEXT("Light Detector"));
	LightDetectorComponent->SetChildActorClass(ALightDetector::StaticClass());
	LightDetectorComponent->SetupAttachment(GetMesh());
}

// Called when the game starts or when spawned
void AUmbraNighCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	EnsureChildActorsInitialised();

	// Set Default HP if none specified
	if (HP->MaxValue == 0) {
		HP->InitialiseAsFull(20);
	}

	if (!Firearm && DefaultFirearm) {
		SetFirearm(Cast<AFirearm>(GetWorld()->SpawnActor(DefaultFirearm.Get())));
	}

	// Reload Firearm if exists
	if (Firearm) {
		Firearm->Reload();
	}
}

void AUmbraNighCharacter::EnsureChildActorsInitialised()
{
	// Align camera rig onto headshot checker
	if (bAlignCameraToHead && CameraRig && HeadshotChecker) CameraRig->SetWorldLocation(HeadshotChecker->GetComponentLocation());

	// Supposed to be set in constructor but occasionally the child actor class just vanishes from the Blueprint on compile, absolutely no clue why so here's a failsafe
	if (!LightDetectorComponent->GetChildActorClass() || !IsValid(LightDetector)) LightDetectorComponent->SetChildActorClass(ALightDetector::StaticClass());

	// Get reference to Light Detector from Child Actor component
	LightDetector = Cast<ALightDetector>(LightDetectorComponent->GetChildActor());
}

// Called every frame
void AUmbraNighCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsAiming) {
		Aim();
	}
}

void AUmbraNighCharacter::SetFirearm(AFirearm* NewFirearm)
{
	if (Firearm && Firearm != NewFirearm) {
		Firearm->Destroy();
	}
	Firearm = NewFirearm;
	Firearm->SetEquipped(true);
	if (GetMesh()->DoesSocketExist(FirearmSocketName)) {
		Firearm->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, FirearmSocketName);
		Firearm->SetOwner(this);
		UE_LOG(LogTemp, Display, TEXT("Attached Firearm <%s> to {%s} at socket <%s>"), *Firearm->GetActorNameOrLabel(), *GetActorNameOrLabel(), *FirearmSocketName.ToString());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("<%s> Is not a valid Mesh Socket name of {%s}, unable to attach Firearm <%s>"), *FirearmSocketName.ToString(), *GetActorNameOrLabel(), *Firearm->GetActorNameOrLabel());
	}
}

void AUmbraNighCharacter::AimStart()
{
	bIsAiming = true;
	
	GetCharacterMovement()->bOrientRotationToMovement = false;
	CameraRig->Zoom(10.0f);
	CameraRig->OffsetCamera(FVector::RightVector * 50.0f);
}

void AUmbraNighCharacter::Aim()
{
	float ControllerYaw = GetControlRotation().Yaw;
	float ActorYaw = GetActorRotation().Yaw;
	if (FMath::Abs(ControllerYaw - ActorYaw) > AIM_ROTATION_TOLERANCE) {
		DesiredRotation = FRotator(0, GetControlRotation().Yaw, 0).Quaternion();
		SetActorRotation(FQuat::Slerp(GetActorRotation().Quaternion(), DesiredRotation, 0.5));
	}
}

void AUmbraNighCharacter::AimEnd()
{
	bIsAiming = false;
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	CameraRig->ResetRig();
}

void AUmbraNighCharacter::ShootFirearm()
{
	if (!bIsEnabled()) return;
	if (Firearm) {
		UE_LOG(LogTemp, Display, TEXT("{%s} has a loaded firearm <%s>, and attempts to shoot it"), *GetActorNameOrLabel(), *Firearm->GetActorNameOrLabel());

		if (Firearm->bIsAmmoEmpty()) {
			UE_LOG(LogTemp, Display, TEXT("<%s> is out of ammo"), *Firearm->GetActorNameOrLabel());
		}
		else {
			FVector TraceStart = GetCamera_Implementation()->GetComponentLocation();
			FVector Direction = GetCamera_Implementation()->GetForwardVector();
			float DirectionalDistanceFromCameraToFirearm = FVector::DotProduct(Firearm->GetActorLocation() - TraceStart, Direction); // Corrects for offset between gun and camera
			float TraceDistance = Firearm->Range + DirectionalDistanceFromCameraToFirearm + 1000; // Distance is firearm range, corrected for camera offset, plus extra 1000 units just in case (actual range check is done by firearm, this is just for aiming from the camera)
			const FVector TraceEnd = TraceStart + Direction * TraceDistance;

			// Set up Trace Params
			FCollisionQueryParams TraceParams(FName(TEXT("Character Shoot Firearm Trace")), true, this);
			TraceParams.bReturnPhysicalMaterial = false;

			// Ignore Child actors
			TArray<AActor*> AttachedActors;
			GetAttachedActors(AttachedActors);
			for (AActor* AttachedActor : AttachedActors) {
				if (AttachedActor) {
					TraceParams.AddIgnoredActor(AttachedActor);
				}
			}

			FHitResult HitOut = FHitResult(ForceInit);
			/* Debugging * /
			const FName TraceTag("CharacterShootFirearmTrace");
			GetWorld()->DebugDrawTraceTag = TraceTag;
			TraceParams.TraceTag = TraceTag;
			//*/

			// Perform Trace on Shootables 
			GetWorld()->LineTraceSingleByChannel(HitOut, TraceStart, TraceEnd, SHOOTABLE_TRACE_CHANNEL, TraceParams);


			AActor* HitActor = HitOut.GetActor();
			UE_LOG(LogTemp, Display, TEXT("{%s} shoots with <%s>"), *GetActorNameOrLabel(), *Firearm->GetActorNameOrLabel());


			FVector ShootTarget;
			if (HitActor) {
				ShootTarget = HitOut.ImpactPoint;
				UE_LOG(LogTemp, Display, TEXT("{%s} shoots <%s>"), *GetActorNameOrLabel(), *HitActor->GetActorNameOrLabel());
			}
			else {
				ShootTarget = TraceEnd;
				UE_LOG(LogTemp, Display, TEXT("{%s} shoots at nothing"), *GetActorNameOrLabel())
			}
			// Call on Firearm to check for any obstructions from the gun itself, and execute shot
			Firearm->ShootAt(ShootTarget, Direction, this);
		}
	}
}

void AUmbraNighCharacter::DragBody(ALimpBody* LimpBody)
{
	if (!bIsEnabled()) return;
	if (!bIsDraggingBody()) {
		if (IsValid(LimpBody)) {
			UE_LOG(LogTemp, Warning, TEXT("{%s} tries to drag a body <%s>"), *GetActorNameOrLabel(), *LimpBody->GetActorNameOrLabel());
			DraggedBody = LimpBody;
			DraggedBody->Mesh->SetSimulatePhysics(false);
			DraggedBody->Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			if (GetMesh()->DoesSocketExist(DragSocketName)) {
				DraggedBody->Mesh->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, DragSocketName);
				UE_LOG(LogTemp, Warning, TEXT("Attached Body <%s> to {%s} at socket <%s>"), *DraggedBody->GetActorNameOrLabel(), *GetActorNameOrLabel(), *DragSocketName.ToString());
			}
			WeighDowns.Add(LimpBody);
			DoSneak_Implementation();
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("<%s> failed drag body, body was invalid"), *GetActorNameOrLabel())
		}
	}
}

void AUmbraNighCharacter::Drop()
{
	if (bIsDraggingBody()) {
		UE_LOG(LogTemp, Display, TEXT("{%s} drops a body <%s>"), *GetActorNameOrLabel(), *DraggedBody->GetActorNameOrLabel());

		DraggedBody->Mesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		DraggedBody->Mesh->SetSimulatePhysics(true);
		DraggedBody->Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		WeighDowns.Remove(DraggedBody);
		DraggedBody = nullptr;
		DoSneakEnd_Implementation();
	}
}

void AUmbraNighCharacter::Throw()
{
}

bool AUmbraNighCharacter::bIsHandsOccupied()
{
	return bIsDraggingBody();
}

bool AUmbraNighCharacter::bIsDraggingBody()
{
	return IsValid(DraggedBody);
}

void AUmbraNighCharacter::Die()
{
	UE_LOG(LogTemp, Display, TEXT("{%s} dies"), *GetActorNameOrLabel());
	bEnabled = false;
	Destroy();
}

void AUmbraNighCharacter::Sleep()
{
	if (bSneakAttackable) {
		AUnconsciousBody* UnconsiousBody = Cast<AUnconsciousBody>(GetWorld()->SpawnActor(AUnconsciousBody::StaticClass()));
		if (UnconsiousBody) {
			UnconsiousBody->SetBodyCharacter(this);
		}
	}
}

void AUmbraNighCharacter::Enable()
{
	bEnabled = true;
	SetActorHiddenInGame(false);
	SetActorEnableCollision(true);
	if (Firearm) Firearm->SetActorHiddenInGame(false);
	SetActorTickEnabled(true);
}

void AUmbraNighCharacter::Disable()
{
	bEnabled = false;
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
	if (Firearm) Firearm->SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
}

bool AUmbraNighCharacter::bIsEnabled()
{
	return bEnabled;
}

void AUmbraNighCharacter::CopyOther(AUmbraNighCharacter* ToCopy, bool bTakeFirearm)
{
	// ===== Body

	// Copy Hitbox dimensions
	GetCapsuleComponent()->InitCapsuleSize(ToCopy->GetCapsuleComponent()->GetScaledCapsuleRadius(), ToCopy->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());

	// Copy Headshot Checker Transform
	HeadshotChecker->SetRelativeTransform(ToCopy->HeadshotChecker->GetRelativeTransform());

	// Copy Mesh and Mesh Transform
	GetMesh()->SetSkeletalMesh(ToCopy->GetMesh()->SkeletalMesh);
	GetMesh()->SetRelativeTransform(ToCopy->GetMesh()->GetRelativeTransform());

	// Copy Animation Blueprint
	UAnimInstance* AnimInstance = ToCopy->GetMesh()->GetAnimInstance();
	if (AnimInstance) {
		GetMesh()->SetAnimInstanceClass(AnimInstance->GetClass());
	}

	// ===== Character Movement Component

	UCharacterMovementComponent* ToCopyMovementComponent = ToCopy->GetCharacterMovement();

	GetCharacterMovement()->RotationRate = ToCopyMovementComponent->RotationRate;
	GetCharacterMovement()->JumpZVelocity = ToCopyMovementComponent->JumpZVelocity;
	GetCharacterMovement()->AirControl = ToCopyMovementComponent->AirControl;
	GetCharacterMovement()->MaxWalkSpeed = ToCopyMovementComponent->MaxWalkSpeed;
	GetCharacterMovement()->MinAnalogWalkSpeed = ToCopyMovementComponent->MinAnalogWalkSpeed;
	GetCharacterMovement()->BrakingDecelerationWalking = ToCopyMovementComponent->BrakingDecelerationWalking;
	GetCharacterMovement()->bRunPhysicsWithNoController = ToCopyMovementComponent->bRunPhysicsWithNoController;
	GetCharacterMovement()->SetCrouchedHalfHeight(ToCopyMovementComponent->GetCrouchedHalfHeight());

	// ===== Light Detector

	// Copy Light Detector Transform
	LightDetectorComponent->SetRelativeTransform(ToCopy->LightDetectorComponent->GetRelativeTransform());
	LightDetector->UpdateClippingPlanes();

	// ===== Firearm

	if (bTakeFirearm && ToCopy->Firearm) {
		SetFirearm(ToCopy->Firearm);
	}
}

ALightDetector* AUmbraNighCharacter::GetLightDetector()
{
	return LightDetector;
}

bool AUmbraNighCharacter::bIsImmobile()
{
	return Restraints.Num() > 0;
}

void AUmbraNighCharacter::OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy)
{
	UE_LOG(LogTemp, Display, TEXT("{%s} was shot by <%s>"), *GetActorNameOrLabel(), *FirearmShotBy->GetActorNameOrLabel());
	// GEngine->AddOnScreenDebugMessage((int32)(intptr_t)this, 3.0f, FColor::Red, FString::Printf(TEXT("{%s} Debug Light Detector: <%F>"), *GetActorNameOrLabel(), LightDetector->CalculateNormalisedLightValue()));

	// GetCapsuleComponent()->AddImpulseAtLocation(ShotDirection * 10000, Hit.ImpactPoint);
	// GetCapsuleComponent()->AddImpulse(ShotDirection * 50000);

	if (HeadshotChecker->bIsHeadshot(Hit.ImpactPoint, ShotDirection)) {
		FirearmShotBy->OnHeadshot(this);
	}
	else {
		FirearmShotBy->OnHit(this);
	}
}

void AUmbraNighCharacter::DealDamage_Implementation(int ShotDamage)
{
	HP->Deplete(ShotDamage);
}

UCameraComponent* AUmbraNighCharacter::GetCamera_Implementation()
{
	return CameraRig->FollowCamera;
}

void AUmbraNighCharacter::DoMove_Implementation(FVector MovementInput)
{
	if (!bIsEnabled()) return;
	if (!bIsImmobile() && Controller && !MovementInput.IsZero())
	{
		if (!WeighDowns.IsEmpty() && !bIsCrouched) {
			Crouch();
		}
		if (!MovementInput.IsNormalized()) { MovementInput.Normalize(); } // Normalise if not already done
		AddMovementInput(MovementInput); // Handle movement in controller
	}
}

void AUmbraNighCharacter::DoLook_Implementation(FVector LookInput)
{
	if (Controller && !LookInput.IsZero()) {
		// UE_LOG(LogTemp, Display, TEXT("Character Received Look Input <%s>"), *LookInput.ToString());
		AddControllerPitchInput(LookInput.Y);
		AddControllerYawInput(LookInput.Z);
	}
}

void AUmbraNighCharacter::DoJump_Implementation()
{
	if (!bIsEnabled()) return;
	if (!bIsImmobile()) {
		UnCrouch();
		Jump();
	}
}

void AUmbraNighCharacter::DoJumpEnd_Implementation()
{
	StopJumping();
}

void AUmbraNighCharacter::DoSneak_Implementation()
{
	bSneakKeyDown = true;

	UE_LOG(LogTemp, Display, TEXT("Sneak Triggered on {%s}"), *GetActorNameOrLabel());
	
	Crouch();
}

void AUmbraNighCharacter::DoSneakEnd_Implementation()
{
	bSneakKeyDown = false;

	UE_LOG(LogTemp, Display, TEXT("Sneak Triggered to end on {%s}"), *GetActorNameOrLabel());

	if (WeighDowns.IsEmpty()) {
		UnCrouch();
	}
}

void AUmbraNighCharacter::DoPrimaryAction_Implementation()
{
	if (!bIsEnabled()) return;

	UE_LOG(LogTemp, Display, TEXT("Character Received Primary Action Input"));
	if(!bIsHandsOccupied() && bIsAiming) ShootFirearm();
}

void AUmbraNighCharacter::DoPrimaryActionRelease_Implementation()
{
	UE_LOG(LogTemp, Display, TEXT("Character Received Primary Action Release Input"));
}

void AUmbraNighCharacter::DoSecondaryAction_Implementation()
{
	if (!bIsEnabled()) return;

	UE_LOG(LogTemp, Display, TEXT("Character Received Secondary Action Input"));
	AimStart();
}

void AUmbraNighCharacter::DoSecondaryActionRelease_Implementation()
{
	UE_LOG(LogTemp, Display, TEXT("Character Received Secondary Action Release Input"));
	AimEnd();
}

void AUmbraNighCharacter::Landed(const FHitResult& Hit)
{
	// UE_LOG(LogTemp, Display, TEXT("{%s} Landed"), *GetActorNameOrLabel());
	bLanding = true;
	
	if (bSneakKeyDown) {
		DoSneak_Implementation(); // If crouch/sneak is queued up while falling, perform
	}

	bLanding = false;
}

void AUmbraNighCharacter::Crouch(bool bClientSimulation)
{
	if (bLanding || !GetCharacterMovement()->IsFalling()) { // Disallow crouch if falling
		Super::Crouch(bClientSimulation);
	}
}

void AUmbraNighCharacter::OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnStartCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	if (LightDetector) {
		LightDetector->SetActorRelativeLocation(FVector::UpVector * -(ScaledHalfHeightAdjust*2));
	}
}

void AUmbraNighCharacter::OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnEndCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	if (LightDetector) {
		LightDetector->SetActorRelativeLocation(FVector::ZeroVector);
	}
}

bool AUmbraNighCharacter::CanJumpInternal_Implementation() const
{
	if (bIsCrouched) {
		return true; // Override to allow jumping while crouched
	}
	return Super::CanJumpInternal_Implementation();
}

bool AUmbraNighCharacter::bIsBlind_Implementation()
{
	return bBlinded;
}

void AUmbraNighCharacter::DoBlindForDuration_Implementation(int Duration)
{
	BlindSelf_Implementation();
	GetWorld()->GetTimerManager().SetTimer(BlindDurationHandle, this, &AUmbraNighCharacter::UnBlindSelf_Implementation, Duration, false);
}

void AUmbraNighCharacter::BlindSelf_Implementation()
{
	bBlinded = true;
}

void AUmbraNighCharacter::UnBlindSelf_Implementation()
{
	bBlinded = false;
}

void AUmbraNighCharacter::DoDefaultBlind_Implementation()
{
	DoBlindForDuration_Implementation(5.0F);
}

void AUmbraNighCharacter::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	if (LightDetector && LightDetector->bIsShadowed()) {
		SuspicionOut.DetectionAmount *= 0.75F;
	}
}

void AUmbraNighCharacter::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	if (LightDetector && LightDetector->bIsShadowed()) {
		SuspicionOut.DetectionAmount *= 0.75F;
	}
}
