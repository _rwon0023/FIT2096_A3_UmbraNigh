// { Ryan Wong <32491034> 2022 }


#include "DeadBody.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"


void ADeadBody::SetBodyCharacter(AUmbraNighCharacter* Character)
{
	/* Debugging * /
	 
	// Animator desyncs on duplication, so store and restore afterwards
	UClass* AnimInstanceClassSync = Character->GetMesh()->GetAnimInstance()->GetClass();
	Character->GetMesh()->SetAnimInstanceClass(nullptr);

	// Dead Character gets destroyed, so generate new character to store as the body
	FActorSpawnParameters Cloner;
	Cloner.Template = Character;
	AUmbraNighCharacter* UndeadBody = Cast<AUmbraNighCharacter>(Character->GetWorld()->SpawnActor(Character->GetClass(), 0,0, Cloner));

	UndeadBody->GetMesh()->SetAnimInstanceClass(AnimInstanceClassSync);
	
	// */
	
	AUndead* UndeadBody = Cast<AUndead>(GetWorld()->SpawnActor(AUndead::StaticClass()));

	UndeadBody->TakeOverCharacter(Character);

	Super::SetBodyCharacter(UndeadBody);
}

void ADeadBody::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 5;
	SuspicionOut.bSuspicionOnly = true;
}

void ADeadBody::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::EXPLICIT;
	SuspicionOut.DetectionAmount = 45;
	SuspicionOut.bSuspicionOnly = true;
}
