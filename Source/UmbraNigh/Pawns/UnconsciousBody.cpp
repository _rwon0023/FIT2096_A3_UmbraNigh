// { Ryan Wong <32491034> 2022 }


#include "UnconsciousBody.h"


void AUnconsciousBody::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 5;
	SuspicionOut.bSuspicionOnly = true;
}

void AUnconsciousBody::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 20;
	SuspicionOut.bSuspicionOnly = true;
}