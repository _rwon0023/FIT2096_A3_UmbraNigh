// Ryan Wong 32491034 2022


#include "PawnCameraRig.h"

// Sets default values for this component's properties
UPawnCameraRig::UPawnCameraRig()
{
	PrimaryComponentTick.bCanEverTick = true;
	
	// Initialise Camera Arm
	SocketOffset = FVector::ZeroVector;
	ResetRig();

	// Handle controller rotation via Camera
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn) {
		UE_LOG(LogTemp, Display, TEXT("Camera Rig Detected Owner: <%s>"), *GetOwner()->GetActorNameOrLabel());

		OwnerPawn->bUseControllerRotationPitch = false;
		OwnerPawn->bUseControllerRotationYaw = false;
		OwnerPawn->bUseControllerRotationRoll = false;
		this->bUsePawnControlRotation = true;
	}

	// Create and attach Follow Camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Follow Camera"));
	FollowCamera->SetupAttachment(this, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

void UPawnCameraRig::BeginPlay()
{
	Super::BeginPlay();
}

void UPawnCameraRig::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (SocketOffset != DesiredCameraOffset) {
		SocketOffset = FMath::Lerp(SocketOffset, DesiredCameraOffset, DeltaTime*10);
		if (FVector::DistSquared(SocketOffset, DesiredCameraOffset) < 10) {
			UE_LOG(LogTemp, Display, TEXT("{%s} snapped to Desired Camera Offset"), *GetName());
			SocketOffset = DesiredCameraOffset;
		}
	}
}

void UPawnCameraRig::ResetRig()
{
	this->TargetArmLength = DefaultArmLength;
	DesiredCameraOffset = FVector::ZeroVector;
}

void UPawnCameraRig::Zoom(float ZoomDistance)
{
	this->TargetArmLength = DefaultArmLength-ZoomDistance;
}

void UPawnCameraRig::OffsetCamera(FVector Offset)
{
	DesiredCameraOffset = Offset;
}

