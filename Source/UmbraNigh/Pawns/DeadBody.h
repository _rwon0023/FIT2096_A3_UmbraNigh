// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/Undead.h"

#include "LimpBody.h"
#include "DeadBody.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API ADeadBody : public ALimpBody
{
	GENERATED_BODY()

public:
	virtual void SetBodyCharacter(AUmbraNighCharacter* Character) override;
	
	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
};
