// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Kismet/GameplayStatics.h"
#include "UmbraNigh/Common/BlueprintDefaultHelper.h"

#include "Components/SceneCaptureComponent2D.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Materials/MaterialInstanceConstant.h"

#include "GameFramework/Actor.h"
#include "LightDetector.generated.h"

// This is a slightly modified version of USceneCaptureComponent2D which is used in ALightDetector
// USceneCaptureComponent2D does not recognise its owner object as its view owner by default for some reason, so this class overrides the function to do so.
// This is required by ALightDetector to only show the octohedron reference mesh to the internal scene renderers
UCLASS()
class UMBRANIGH_API USceneCaptureComponentWithViewOwner : public USceneCaptureComponent2D {
	GENERATED_BODY()
public:
	virtual const AActor* GetViewOwner() const override { return GetOwner(); }
};


UCLASS()
class UMBRANIGH_API ALightDetector : public AActor
{
	GENERATED_BODY()

private:
	inline static const float CACHE_TIME = 0.1F;
	inline static const float SHADOW_THRESHOLD = 0.5F;			// Normalised Light Value threshold under which counts as being 'shadowed'
	inline static const int RENDER_TEXTURE_RESOLUTION = 64;		// Default render textures are generated with dimesnions RENDER_TEXTURE_RESOLUTION x RENDER_TEXTURE_RESOLUTION (Default was 256)
	inline static const int STENCIL_VALUE = 1;					// Stencil Value/ID used by mesh and StencilPPMaterial to cull non-mesh depth
	inline static const float MESH_RADIUS = 70.0F;				// Radius of the mesh, used to configure Scene capture components
	inline static const float CAPTURE_RADIUS = 580.0F;			// Distance of Scene capture components from center
	
	float CachedValue;
	float NextCacheTime;

	void ConfigureUninitialisedDefaults();
	
public:	
	ALightDetector();

	UPROPERTY(EditAnywhere)
		USceneCaptureComponentWithViewOwner* TopRender;
	UPROPERTY(EditAnywhere)
		USceneCaptureComponentWithViewOwner* BottomRender;

	UPROPERTY(EditAnywhere)
		UTextureRenderTarget2D* TopTexture;
	UPROPERTY(EditAnywhere)
		UTextureRenderTarget2D* BottomTexture;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* OctohedronMesh;

	UPROPERTY(EditAnywhere)
		TScriptInterface<IBlendableInterface> StencilPPMaterial;

protected:
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable)
	void PerformSceneCapture();
	float GetBrightestPixel(UTextureRenderTarget2D* Texture);

public:	
	float CalculateNormalisedLightValue();
	bool bIsShadowed();

	void SetDetectorUniformScale(float Scale);
	void UpdateClippingPlanes();
};
