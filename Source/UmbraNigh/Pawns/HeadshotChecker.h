// Ryan Wong 32491034 2022

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Interfaces/Shootable.h"
#include "UmbraNigh/Weapons/Firearms/Firearm.h"

#include "Components/SphereComponent.h"
#include "HeadshotChecker.generated.h"

UCLASS()
class UMBRANIGH_API UHeadshotChecker : public USphereComponent, public IShootable
{
	GENERATED_BODY()
	
private:
	inline static const ECollisionChannel HEADSHOT_TRACE_CHANNEL = ECollisionChannel::ECC_GameTraceChannel2;

public:
	UHeadshotChecker();

protected:
	virtual void BeginPlay() override;
	UShapeComponent* ParentCollider;

public:
	void AssignParentCollider(UShapeComponent* Collider);
	bool bIsHeadshot(FVector ImpactPoint, FVector ShotDirection);

	virtual void OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy) override;
};
