// Ryan Wong 32491034 2022


#include "HeadshotChecker.h"

UHeadshotChecker::UHeadshotChecker()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UHeadshotChecker::BeginPlay()
{
	Super::BeginPlay();
	
	SetCollisionResponseToChannel(HEADSHOT_TRACE_CHANNEL, ECollisionResponse::ECR_Block);
	AssignColliderAsShootable(this);
}

void UHeadshotChecker::AssignParentCollider(UShapeComponent* Collider)
{
	ParentCollider = Collider;
}

bool UHeadshotChecker::bIsHeadshot(FVector ImpactPoint, FVector ShotDirection)
{
	if (ParentCollider) {
		const FVector TraceEnd = ImpactPoint + ShotDirection * (ParentCollider->Bounds.SphereRadius * 2);

		// Set up Trace Params
		FCollisionQueryParams TraceParams(FName(TEXT("Headshot Trace")), false, 0);
		TraceParams.bReturnPhysicalMaterial = false;
		FHitResult HitOut = FHitResult(ForceInit);
		/* Debugging */
		const FName TraceTag("HeadshotTrace");
		GetWorld()->DebugDrawTraceTag = TraceTag;
		TraceParams.TraceTag = TraceTag;
		//*/

		// Perform Trace from passed in Hit
		GetWorld()->LineTraceSingleByChannel(HitOut, ImpactPoint, TraceEnd, HEADSHOT_TRACE_CHANNEL, TraceParams);

		UPrimitiveComponent* HitComponent = HitOut.GetComponent();
		FString ResultString = (HitComponent) ? HitComponent->GetName() : "No component found";
		
		UE_LOG(LogTemp, Display, TEXT("Headshot Checker traced for check, returned component: <%s>"), *ResultString);

		return HitComponent == this;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Headshot Checker attempted to check for headshot while no Parent collider assigned"));
		return false;
	}
}

void UHeadshotChecker::OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy)
{
	if (GetOwner()->GetClass()->ImplementsInterface(UShootable::StaticClass())) {
		FirearmShotBy->OnHeadshot(GetOwner());
		// IShootable::Execute_OnShot(GetOwner(), Hit, ShotDirection, FirearmShotBy);
	}
}
