// { Ryan Wong <32491034> 2022 }


#include "EnemyCharacter.h"

AEnemyCharacter::AEnemyCharacter()
{
	bSneakAttackable = true;
	Allegiance = EAllegiance::ENEMY;
}

void AEnemyCharacter::Die()
{
	ADeadBody* DeadBody = Cast<ADeadBody>(GetWorld()->SpawnActor(ADeadBody::StaticClass()));
	if (DeadBody) {
		DeadBody->SetBodyCharacter(this);
	}

	Super::Die();
}
