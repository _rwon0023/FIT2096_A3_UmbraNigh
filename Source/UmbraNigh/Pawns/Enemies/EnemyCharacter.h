// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/DeadBody.h"

#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "EnemyCharacter.generated.h"

USTRUCT()
struct FPatrolPoint {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	FVector PatrolLocation;
	UPROPERTY(EditAnywhere)
	float PatrolWaitTime;
};

/**
 * 
 */
UCLASS()
class UMBRANIGH_API AEnemyCharacter : public AUmbraNighCharacter
{
	GENERATED_BODY()
	
public:
	AEnemyCharacter();

	UPROPERTY(EditAnywhere, Category = Patrol)
		int StartPatrolIndex = 0;
	UPROPERTY(EditAnywhere, Category = Patrol)
		TArray<FPatrolPoint> PatrolLocations;

	virtual void Die() override;
};
