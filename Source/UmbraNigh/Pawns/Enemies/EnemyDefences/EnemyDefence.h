// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Pawn.h"
#include "UmbraNigh/Interfaces/Shootable.h"
#include "UmbraNigh/Interfaces/Blindable.h"
#include "EnemyDefence.generated.h"

UCLASS()
class UMBRANIGH_API AEnemyDefence : public APawn, public IShootable, public IBlindable
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemyDefence();

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;
	//PerceptionVolume* Perception

	UPROPERTY(EditAnywhere)
		float MovementSpeed = 250;
	UPROPERTY(EditAnywhere)
		float RotationSpeed = 10;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TurnOn();
	void TurnOff();

};
