// { Ryan Wong <32491034> 2022 }


#include "EnemyDefence.h"

// Sets default values
AEnemyDefence::AEnemyDefence()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	// Set up mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AEnemyDefence::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyDefence::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemyDefence::TurnOn()
{
}

void AEnemyDefence::TurnOff()
{
}
