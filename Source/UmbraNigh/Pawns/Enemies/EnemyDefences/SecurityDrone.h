// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "EnemyDefence.h"
#include "SecurityDrone.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API ASecurityDrone : public AEnemyDefence
{
	GENERATED_BODY()
	
private:
	inline static const float DISTANCE_TOLERANCE = 3.0F;
	inline static const float ROTATION_TOLERANCE = 0.1F;

public:
	ASecurityDrone();

	UPROPERTY(EditAnywhere)
		TArray<FVector> PatrolLocations;

	UFUNCTION()
		bool TraverseTowards(FVector Destination, float DeltaTime);
};
