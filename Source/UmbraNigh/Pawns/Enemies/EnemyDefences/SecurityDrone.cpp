// { Ryan Wong <32491034> 2022 }


#include "SecurityDrone.h"

ASecurityDrone::ASecurityDrone()
{
	PrimaryActorTick.bCanEverTick = false;
}

bool ASecurityDrone::TraverseTowards(FVector Destination, float DeltaTime)
{
	// Get current location and vector between current location and destination
	FVector Location = GetActorLocation();
	FVector LocationDifference = Destination - Location;

	if (LocationDifference.Size() < DISTANCE_TOLERANCE) {
		return true; // If within tolerable distance to destination, return true
	}

	// Get current direction, and direction to destination, normalised
	FVector CurrentDirection = GetActorForwardVector();
	FVector DestinationDirection = LocationDifference;
	DestinationDirection.Normalize();

	FVector DirectionDifference = DestinationDirection - CurrentDirection;
	if (DirectionDifference.Size() > ROTATION_TOLERANCE) { // If noticable difference in current direction from desired, Rotate towards desired
		FVector BetweenDirectionsVector = DirectionDifference; // Get direction from current direction vector to destination direction
		BetweenDirectionsVector.Normalize();

		FVector NewDirection = CurrentDirection + (BetweenDirectionsVector * RotationSpeed * DeltaTime); // Nudge current direction to get new direction
		FRotator NewRotation = NewDirection.Rotation(); // Assign new direction as rotation
		SetActorRotation(NewRotation);
	}
	else {
		FVector NewLocation = Location + DestinationDirection * MovementSpeed * DeltaTime;
		SetActorLocation(NewLocation);
	}
	return false; // Return false if have not reached destination

}
