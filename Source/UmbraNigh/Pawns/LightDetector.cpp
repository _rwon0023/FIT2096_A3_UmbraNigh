// { Ryan Wong <32491034> 2022 }


#include "LightDetector.h"

void ALightDetector::ConfigureUninitialisedDefaults()
{
	// Set Render Targets for Top and Bottom Scene captures to be the appropriate textures
	if (TopTexture) TopRender->TextureTarget = TopTexture;
	if (BottomRender) BottomRender->TextureTarget = BottomTexture;

	// If Stencil Post Process Material is not set, use default
	if (!StencilPPMaterial) {
		UMaterial* DefaultStencilPPMaterial = UBlueprintDefaultHelper::GetBlueprintObject<UMaterial>(EBlueprintDefaultHelperRef::OBJ_MAT_SEE_STENCIL_ONLY);
		if (DefaultStencilPPMaterial) {
			UMaterialInstanceDynamic* DefaultStencilPPMaterialDyanamic = CreateDefaultSubobject<UMaterialInstanceDynamic>(TEXT("Stencil Mask Material"));
			DefaultStencilPPMaterialDyanamic->Parent = DefaultStencilPPMaterial;
			DefaultStencilPPMaterialDyanamic->SetScalarParameterValue(FName(TEXT("Stencil ID")), STENCIL_VALUE); // Set Stencil ID to constant STENCIL_VALUE used by this class
			StencilPPMaterial = DefaultStencilPPMaterialDyanamic;
		}
	}
	// Add Stencil Post Process Material to Scene Capture Post Process Blendables
	TopRender->PostProcessSettings.AddBlendable(StencilPPMaterial.GetObjectRef(), 1);
	BottomRender->PostProcessSettings.AddBlendable(StencilPPMaterial.GetObjectRef(), 1);

	// If Octohedron Mesh not assigned, use default
	if (!OctohedronMesh->GetStaticMesh()) {
		UStaticMesh* PlaceholderMesh = UBlueprintDefaultHelper::GetBlueprintObject<UStaticMesh>(EBlueprintDefaultHelperRef::OBJ_CONTROLDIAMOND);
		if (PlaceholderMesh) {
			OctohedronMesh->SetStaticMesh(PlaceholderMesh); // Found a ControlRig asset which is exactly what I needed anyway so using that
		}
	}

	// If has Parent Actor, hide parent
	AActor* ParentActor = GetParentActor();
	if (ParentActor) {
		if (!TopRender->HiddenActors.Contains(ParentActor)) {
			TopRender->HiddenActors.Add(ParentActor);
		}
		if (!BottomRender->HiddenActors.Contains(ParentActor)) {
			BottomRender->HiddenActors.Add(ParentActor);
		}
	}

	// Disable the scene capturers from capturing (will be called manually)
	TopRender->PrimaryComponentTick.SetTickFunctionEnable(false);
	BottomRender->PrimaryComponentTick.SetTickFunctionEnable(false);
	TopRender->bCaptureEveryFrame = false;
	TopRender->bCaptureOnMovement = false;
	BottomRender->bCaptureEveryFrame = false;
	BottomRender->bCaptureOnMovement = false;

	// Required for postprocessing
	TopRender->bAlwaysPersistRenderingState = true;
	BottomRender->bAlwaysPersistRenderingState = true;

	// Set Scaled Clipping Planes for scene capturers
	UpdateClippingPlanes();
}

void ALightDetector::UpdateClippingPlanes()
{
	double ScaleSize = GetActorScale().Z;
	TopRender->CustomNearClippingPlane = (CAPTURE_RADIUS - MESH_RADIUS) * ScaleSize;
	BottomRender->CustomNearClippingPlane = (CAPTURE_RADIUS - MESH_RADIUS) * ScaleSize;
}



ALightDetector::ALightDetector()
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	// ===== Scene Captures

	// Create and Attach Scene Capture Components for Top and Bottom
	TopRender = CreateDefaultSubobject<USceneCaptureComponentWithViewOwner>(TEXT("Scene Capture Top"));
	BottomRender = CreateDefaultSubobject<USceneCaptureComponentWithViewOwner>(TEXT("Scene Capture Bottom"));
	TopRender->SetupAttachment(RootComponent);
	BottomRender->SetupAttachment(RootComponent);

	// Configure Scene Capture Components
	TopRender->ProjectionType = ECameraProjectionMode::Perspective;
	BottomRender->ProjectionType = ECameraProjectionMode::Perspective;
	TopRender->FOVAngle = 10;
	BottomRender->FOVAngle = 10;
	TopRender->bOverride_CustomNearClippingPlane = true;
	BottomRender->bOverride_CustomNearClippingPlane = true;

	// (Required for Post processing)
	TopRender->CaptureSource = ESceneCaptureSource::SCS_FinalToneCurveHDR;
	BottomRender->CaptureSource = ESceneCaptureSource::SCS_FinalToneCurveHDR;

	// Position Scene Capture components at top and bottom
	TopRender->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, CAPTURE_RADIUS), FRotator(-90.0f, 0.0f, 0.0f));
	BottomRender->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -CAPTURE_RADIUS), FRotator(90.0f, 180.0f, 0.0f));

	// ===== Detector Mesh

	OctohedronMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Octohedron Mesh"));
	OctohedronMesh->SetupAttachment(RootComponent);

	// Disadble mesh shadowcasting
	OctohedronMesh->SetCastShadow(false);

	// Disable collision
	OctohedronMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	// Make only this actor able to see the mesh
	OctohedronMesh->bOnlyOwnerSee = true;

	// Render mesh to custom stencil depth, used to cull any background
	OctohedronMesh->bRenderCustomDepth = true;
	OctohedronMesh->CustomDepthStencilValue = STENCIL_VALUE;

	// ===== Other

	/* Debugging * /
	static ConstructorHelpers::FObjectFinder<UTextureRenderTarget2D> TestRenderTarget(TEXT("TextureRenderTarget2D'/Game/TestRenderTarget.TestRenderTarget'"));
	if (TestRenderTarget.Object) TopTexture = TestRenderTarget.Object;
	//*/

	// Initialise defaults that may need to be reinitialised in BeginPlay
	ConfigureUninitialisedDefaults();
}

// Called when the game starts or when spawned
void ALightDetector::BeginPlay()
{
	Super::BeginPlay();

	// Initialise cache
	CachedValue = 0;
	NextCacheTime = 0;

	// Create Top and Bottom Render textures if not already assigned

	if (!TopTexture) {
		TopTexture = NewObject<UTextureRenderTarget2D>();
		TopTexture->InitAutoFormat(RENDER_TEXTURE_RESOLUTION, RENDER_TEXTURE_RESOLUTION);
		TopTexture->UpdateResourceImmediate();
	}
	
	if (!BottomTexture) {
		BottomTexture = NewObject<UTextureRenderTarget2D>();
		BottomTexture->InitAutoFormat(RENDER_TEXTURE_RESOLUTION, RENDER_TEXTURE_RESOLUTION);
		BottomTexture->UpdateResourceImmediate();
	}

	// Reinitialise any defaults that have not been assigned
	ConfigureUninitialisedDefaults();

}

void ALightDetector::PerformSceneCapture()
{
	// GEngine->AddOnScreenDebugMessage((int32)(intptr_t)(this) + 3, 2.0f, FColor::Yellow, FString::Printf(TEXT("{%s} Performing Scene Capture"), *GetActorNameOrLabel()));
	TopRender->CaptureScene();
	BottomRender->CaptureScene();
}

float ALightDetector::GetBrightestPixel(UTextureRenderTarget2D* Texture)
{

	// Read Pixels into Colour Array
	FRenderTarget* RenderTarget = Texture->GameThread_GetRenderTargetResource(); // GetRenderTargetResource (w/o GameThread_) can crash if called too early
	TArray<FColor> Pixels;
	RenderTarget->ReadPixels(Pixels);
	
	float BrightestPixel{ 0 };
	float CurrentPixelBrightness{ 0 };

	// Iterate through every Pixel in render texture
	for (int i = 0; i < Pixels.Num(); i++) {

		// Get Pixel brightness from RGB values based off formula
		// www.stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color
		CurrentPixelBrightness = ((0.299 * Pixels[i].R) + (0.587 * Pixels[i].G) + (0.114 * Pixels[i].B));
		// Compare against brightest pixel so far, track max brightness
		BrightestPixel = FMath::Max(BrightestPixel, CurrentPixelBrightness);
	}
	// Return max brightness found over all pixels
	return BrightestPixel;
}

float ALightDetector::CalculateNormalisedLightValue()
{
	if (!TopTexture || !BottomTexture) return 0.0F; // Return 0 if unassigned textures or called before initialisation

	// Check if cache is valid, if so return the cached light value for optimisation
	float TimeNow = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	if (TimeNow < NextCacheTime) {
		// GEngine->AddOnScreenDebugMessage((int32)(intptr_t)(this), 2.0f, FColor::Yellow, FString::Printf(TEXT("Using Cached Light Value: %F"), CachedValue));
		// GEngine->AddOnScreenDebugMessage((int32)(intptr_t)(this) + 1, 2.0f, FColor::Yellow, FString::Printf(TEXT("Next Cache: %F / %F"), TimeNow, NextCacheTime));
		return CachedValue;
	}
	else {
		NextCacheTime = TimeNow + CACHE_TIME;
	}

	// Render to textures
	PerformSceneCapture();

	// Get the brigher of the two max brightnesses, over Top and Bottom textures
	float LightValue = FMath::Max(GetBrightestPixel(TopTexture), GetBrightestPixel(BottomTexture));
	
	// Normalise Light Value between 0 and 1
	LightValue = LightValue / 255.0;
	// UE_LOG(LogTemp, Display, TEXT("Calculated Normalised Light Value as <%f>"), LightValue);
	
	CachedValue = LightValue; // Cache the light value

	return LightValue;
}

bool ALightDetector::bIsShadowed()
{
	return CalculateNormalisedLightValue() <= SHADOW_THRESHOLD;
}




void ALightDetector::SetDetectorUniformScale(float Scale)
{
	SetActorScale3D(FVector::OneVector * Scale);
	UpdateClippingPlanes();
}
