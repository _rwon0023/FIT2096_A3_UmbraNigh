// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "Undead.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API AUndead : public AUmbraNighCharacter
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere)
		float RecogniseRange = 500.0F;

	EAllegiance DisguiseAllegiance;

	void TakeOverCharacter(AUmbraNighCharacter* TargetCharacter);

	virtual bool CanInteract_Implementation(TScriptInterface<IInteractable>& Interactable) override;

	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
};
