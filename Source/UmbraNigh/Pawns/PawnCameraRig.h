// Ryan Wong 32491034 2022

#pragma once

#include "CoreMinimal.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

#include "Components/SceneComponent.h"
#include "PawnCameraRig.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent, AllowPrivateAccess = "true") )
class UMBRANIGH_API UPawnCameraRig : public USpringArmComponent
{
	GENERATED_BODY()

private:
	FVector DesiredCameraOffset = FVector::ZeroVector;

public:
	UPawnCameraRig();

	UPROPERTY(EditAnywhere, Category = Camera)
		UCameraComponent* FollowCamera;

	UPROPERTY(EditAnywhere, Category = Camera)
		float DefaultArmLength = 400.0f;

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ResetRig();
	void Zoom(float ZoomDistance);
	void OffsetCamera(FVector Offset);
};
