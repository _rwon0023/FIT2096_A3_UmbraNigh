// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "LimpBody.h"
#include "UnconsciousBody.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API AUnconsciousBody : public ALimpBody
{
	GENERATED_BODY()
	
public:
	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
};
