// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Components/SphereComponent.h"

#include "UmbraNigh/Interfaces/Interactable.h"
#include "UmbraNigh/Environment/LooseObject.h"
#include "LimpBody.generated.h"

// Forward Declaration
class AUmbraNighCharacter;
class ALargeContainer;

/**
 * 
 */
UCLASS()
class UMBRANIGH_API ALimpBody : public ALooseObject, public IInteractable
{
	GENERATED_BODY()
	
private:
	AUmbraNighCharacter* BodyCharacter;
	ALargeContainer* BodyContainer;

public:
	ALimpBody();

	UPROPERTY(EditAnywhere)
		USphereComponent* InteractionTrigger;

	virtual void BeginPlay() override;
	AUmbraNighCharacter* GetBodyCharacter();
	virtual void SetBodyCharacter(AUmbraNighCharacter* Character);
	virtual void UnlimpBodyCharacter();

	virtual void Interact_Implementation(AUmbraNighCharacter* Interactor) override;
	virtual EInteractionType GetInteractionType_Implementation() override;
};
