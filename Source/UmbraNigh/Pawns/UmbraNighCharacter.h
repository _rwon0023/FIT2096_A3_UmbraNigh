// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "HeadshotChecker.h"
#include "PawnCameraRig.h"
#include "UmbraNigh/Common/DepletableValue.h"
#include "UmbraNigh/Weapons/Firearms/Firearm.h"
#include "LimpBody.h"
#include "UnconsciousBody.h"
#include "UmbraNigh/Pawns/LightDetector.h"

#include "GameFramework/Character.h"
#include "UmbraNigh/Interfaces/Playable.h"
#include "UmbraNigh/Interfaces/Shootable.h"
#include "UmbraNigh/Interfaces/Blindable.h"
#include "UmbraNigh/Interfaces/Detectable.h"
#include "UmbraNighCharacter.generated.h"

UENUM()
enum class EAllegiance : uint8 {
	NONE		UMETA(DisplayName = "NO ALLEGIANCE"),
	PLAYER		UMETA(DisplayName = "PLAYER"),
	ENEMY		UMETA(DisplayName = "ENEMY"),
};

UCLASS()
class UMBRANIGH_API AUmbraNighCharacter : public ACharacter, public IPlayable, public IShootable, public IBlindable, public IDetectable
{
	GENERATED_BODY()

private:
	inline static const float AIM_ROTATION_TOLERANCE = 0.01F;

public:
	AUmbraNighCharacter();
	
	// Camera
	UPROPERTY(EditAnywhere, Category = Camera)
		UPawnCameraRig* CameraRig;
	UPROPERTY(EditAnywhere, Category = Camera)
		bool bAlignCameraToHead = true;

	// Tracked
	UPROPERTY(VisibleAnywhere)
	TArray<UObject*> Restraints;
	UPROPERTY(VisibleAnywhere)
	TArray<UObject*> WeighDowns;
	UPROPERTY(VisibleAnywhere)
	TMap<EInteractionType, TScriptInterface<IInteractable>> InteractionOverrides;
	UPROPERTY(BlueprintReadOnly)
		bool bIsAiming = false;

	// Attributes
	UPROPERTY(EditAnywhere)
		FName CharacterName;
	UPROPERTY(EditAnywhere)
		UDepletableValue* HP;
	UPROPERTY(EditAnywhere)
		EAllegiance Allegiance;
	UPROPERTY(EditAnywhere)
		bool bSneakAttackable;
	UPROPERTY(EditAnywhere, Category = Firearm)
		AFirearm* Firearm = nullptr;

	// Attached
	UPROPERTY(EditAnywhere)
		UHeadshotChecker* HeadshotChecker;
	//UPROPERTY(EditAnywhere)
	//	ThrowableObject HeldItem = nullptr;
	UPROPERTY(EditAnywhere)
		ALimpBody* DraggedBody = nullptr;

	// Assigners
	UPROPERTY(EditAnywhere)
		UChildActorComponent* LightDetectorComponent;
	UPROPERTY(EditAnywhere, Category = Firearm)
		TSubclassOf<AFirearm> DefaultFirearm;
	UPROPERTY(EditAnywhere)
		FName FirearmSocketName = "weapon_r";
	UPROPERTY(EditAnywhere)
		FName DragSocketName = "weapon_l";
	
	FTimerHandle BlindDurationHandle;

private:
	ALightDetector* LightDetector;
	FQuat DesiredRotation;
	bool bEnabled = true;
	bool bLanding;
	bool bBlinded;

protected:
	bool bSneakKeyDown;

	virtual void BeginPlay() override;

public:	
	void EnsureChildActorsInitialised();
	virtual void Tick(float DeltaTime) override;


	void SetFirearm(AFirearm* NewFirearm);
	void AimStart();
	void Aim();
	void AimEnd();
	void ShootFirearm();
	//void HoldObject(ThrowableObject* Item);
	void DragBody(ALimpBody* LimpBody);
	void Drop();
	void Throw();
	bool bIsHandsOccupied();
	bool bIsDraggingBody();
	UFUNCTION()
	virtual void Die();
	void Sleep();
	void Enable();
	void Disable();
	bool bIsEnabled();
	void CopyOther(AUmbraNighCharacter* ToCopy, bool bTakeFirearm = false);
	ALightDetector* GetLightDetector();
	UFUNCTION(BlueprintCallable)
	bool bIsImmobile();

	virtual void OnShot_Implementation(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy) override;
	virtual void DealDamage_Implementation(int ShotDamage) override;

	virtual UCameraComponent* GetCamera_Implementation() override;

	virtual void DoMove_Implementation(FVector MovementInput) override;
	virtual void DoLook_Implementation(FVector LookInput) override;
	virtual void DoJump_Implementation() override;
	virtual void DoJumpEnd_Implementation() override;
	virtual void DoSneak_Implementation() override;
	virtual void DoSneakEnd_Implementation() override;
	virtual void DoPrimaryAction_Implementation() override;
	virtual void DoPrimaryActionRelease_Implementation() override;
	virtual void DoSecondaryAction_Implementation() override;
	virtual void DoSecondaryActionRelease_Implementation() override;	

	virtual void Landed(const FHitResult& Hit) override;
	virtual void Crouch(bool bClientSimulation = false) override;
	virtual void OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;
	virtual void OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;
	virtual bool CanJumpInternal_Implementation() const override;

	virtual bool bIsBlind_Implementation() override;
	virtual void DoBlindForDuration_Implementation(int Duration) override;
	virtual void BlindSelf_Implementation() override;
	virtual void UnBlindSelf_Implementation() override;
	virtual void DoDefaultBlind_Implementation() override;

	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
};
