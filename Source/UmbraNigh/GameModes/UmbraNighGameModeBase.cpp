// Copyright Epic Games, Inc. All Rights Reserved.


#include "UmbraNighGameModeBase.h"

AUmbraNighGameModeBase::AUmbraNighGameModeBase()
{
	// Set Player Controller Class
	PlayerControllerClass = AUmbraNighPlayerController::StaticClass();

	// Assign Default Pawn Class
	//static ConstructorHelpers::FObjectFinder<UBlueprint> PlayerBlueprint(TEXT("Blueprint'/Game/Blueprints/BP_TheWitch.BP_TheWitch'"));
	UBlueprint* PlayerBlueprint = UBlueprintDefaultHelper::GetBlueprintObject<UBlueprint>(EBlueprintDefaultHelperRef::OBJ_PLAYER);
	if (PlayerBlueprint) {
		DefaultPawnClass = (UClass*)PlayerBlueprint->GeneratedClass;
	}
}
