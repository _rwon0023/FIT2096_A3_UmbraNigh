// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Player/UmbraNighPlayerController.h"

#include "GameFramework/GameModeBase.h"
#include "UmbraNighGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API AUmbraNighGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUmbraNighGameModeBase();

};
