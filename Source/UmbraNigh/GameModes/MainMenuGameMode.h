// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API AMainMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
};
