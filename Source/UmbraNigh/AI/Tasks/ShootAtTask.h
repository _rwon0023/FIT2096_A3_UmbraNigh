// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "BehaviorTree/BTTaskNode.h"
#include "ShootAtTask.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API UShootAtTask : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;
	
	UPROPERTY(Category = Blackboard, EditAnywhere)
		float Range;
	UPROPERTY(Category = Blackboard, EditAnywhere)
		bool bAutoReload;
	UPROPERTY(EditAnywhere, Category=Blackboard)
	struct FBlackboardKeySelector TargetBlackboardKey;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
