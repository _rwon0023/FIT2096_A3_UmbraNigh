// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/Enemies/EnemyCharacter.h"
#include "UmbraNigh/AI/EnemyCharAIController.h"

#include "BehaviorTree/BTTaskNode.h"
#include "AlertNearbyEnemiesTask.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API UAlertNearbyEnemiesTask : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UPROPERTY(Category = Blackboard, EditAnywhere)
		FVector Range;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
