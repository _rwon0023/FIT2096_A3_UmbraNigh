// { Ryan Wong <32491034> 2022 }


#include "WakeUpUnconsciousTask.h"

void UWakeUpUnconsciousTask::InitializeFromAsset(UBehaviorTree& Asset)
{
    ToWake.AddObjectFilter(this, FName("UnconsciousBody To Wake Up"), AUnconsciousBody::StaticClass());
}

EBTNodeResult::Type UWakeUpUnconsciousTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBehaviorTreeComponent* Component = &OwnerComp;
	if (!Component)
		return EBTNodeResult::Failed;

	UBlackboardComponent* Blackboard = Component->GetBlackboardComponent();
	if (!Blackboard)
		return EBTNodeResult::Failed;

	AUnconsciousBody* ToWakeBody = Cast<AUnconsciousBody>(Blackboard->GetValueAsObject(ToWake.SelectedKeyName));

	if (!ToWakeBody)
		return EBTNodeResult::Failed;

	ToWakeBody->UnlimpBodyCharacter();

	return EBTNodeResult::Succeeded;
}
