// { Ryan Wong <32491034> 2022 }


#include "CyclePatrolTask.h"

EBTNodeResult::Type UCyclePatrolTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBehaviorTreeComponent* Component = &OwnerComp;
	if (!Component)
		return EBTNodeResult::Failed;
	
	AEnemyCharAIController* CharController = Cast<AEnemyCharAIController>(Component->GetOwner());
	if (!CharController)
		return EBTNodeResult::Failed;

	CharController->CyclePatrol();
	return EBTNodeResult::Succeeded;
}
