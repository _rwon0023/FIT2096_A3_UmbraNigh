// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/AI/EnemyCharAIController.h"

#include "BehaviorTree/BTTaskNode.h"
#include "CyclePatrolTask.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API UCyclePatrolTask : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
