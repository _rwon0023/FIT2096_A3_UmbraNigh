// { Ryan Wong <32491034> 2022 }


#include "ShootAtTask.h"

void UShootAtTask::InitializeFromAsset(UBehaviorTree& Asset)
{
	TargetBlackboardKey.AddVectorFilter(this, FName("Target"));
}

EBTNodeResult::Type UShootAtTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{

	UBehaviorTreeComponent* Component = &OwnerComp;
	if (!Component)
		return EBTNodeResult::Failed;

	UBlackboardComponent* Blackboard = Component->GetBlackboardComponent();
	if(!Blackboard)
		return EBTNodeResult::Failed;

	FVector Target = Blackboard->GetValueAsVector(TargetBlackboardKey.SelectedKeyName);

	AAIController* Controller = Cast<AAIController>(Component->GetOwner());
	if (!Controller)
		return EBTNodeResult::Failed;

	AUmbraNighCharacter* ControlledCharacter = Cast<AUmbraNighCharacter>(Controller->GetPawn());
	if(!ControlledCharacter)
		return EBTNodeResult::Failed;

	if (ControlledCharacter->bIsBlind_Implementation())
		return EBTNodeResult::Failed;

	if (FVector::Distance(Target, ControlledCharacter->GetActorLocation()) > Range)
		return EBTNodeResult::Failed;

	if (!ControlledCharacter->Firearm)
		return EBTNodeResult::Failed;

	ControlledCharacter->AimStart();
	UCameraComponent* CharacterCamera = ControlledCharacter->GetCamera_Implementation();
	if (CharacterCamera) {
		FVector Direction = Target - CharacterCamera->GetComponentLocation();
		Direction.Normalize();
		CharacterCamera->SetWorldRotation(Direction.ToOrientationQuat());
	}
	ControlledCharacter->ShootFirearm();

	if (bAutoReload) {
		ControlledCharacter->Firearm->Reload();
	}

	return EBTNodeResult::Succeeded;
}
