// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "UmbraNigh/Pawns/UnconsciousBody.h"

#include "BehaviorTree/BTTaskNode.h"
#include "WakeUpUnconsciousTask.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API UWakeUpUnconsciousTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;

	UPROPERTY(EditAnywhere, Category = Blackboard)
		struct FBlackboardKeySelector ToWake;
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
