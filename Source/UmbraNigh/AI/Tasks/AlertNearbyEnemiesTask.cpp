// { Ryan Wong <32491034> 2022 }


#include "AlertNearbyEnemiesTask.h"

EBTNodeResult::Type UAlertNearbyEnemiesTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBehaviorTreeComponent* Component = &OwnerComp;
	if (!Component)
		return EBTNodeResult::Failed;

	UBlackboardComponent* Blackboard = Component->GetBlackboardComponent();
	if (!Blackboard)
		return EBTNodeResult::Failed;

	AController* Controller = Cast<AController>(Component->GetOwner());
	if (!Controller)
		return EBTNodeResult::Failed;

	APawn* ControlledPawn = Controller->GetPawn();
	if (!ControlledPawn)
		return EBTNodeResult::Failed;

	FVector OriginLocation = ControlledPawn->GetActorLocation();

	// Set up Trace Params
	FCollisionQueryParams TraceParams(FName(TEXT("Alert Nearby Trace")), true, ControlledPawn);
	TraceParams.bReturnPhysicalMaterial = false;
	TArray<FOverlapResult> HitOuts;
	HitOuts.Empty();
	/* Debugging */
	const FName TraceTag("AlertNearbyTaskTrace");
	ControlledPawn->GetWorld()->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;
	//*/

	// Perform Trace/Overlap
	float MaxRange = Range.GetMax();
	ControlledPawn->GetWorld()->OverlapMultiByChannel(HitOuts, OriginLocation, FQuat::Identity, ECollisionChannel::ECC_Pawn, FCollisionShape::MakeSphere(MaxRange), TraceParams);

	FSuspicion AlertSuspicion;
	AlertSuspicion.bSuspicionOnly = true;
	AlertSuspicion.DetectionAmount = 40;
	AlertSuspicion.SuspicionType = ESuspicionType::EXPLICIT;

	AEnemyCharAIController* EnemyCharContoller = Cast<AEnemyCharAIController>(Controller);
	if (EnemyCharContoller) {
		if (!EnemyCharContoller->bSensedCharacterInSight) {
			AlertSuspicion.SuspicionType = ESuspicionType::IMPLICIT;
		}
	}

	float ControlledPawnYaw = ControlledPawn->GetActorRotation().Yaw;

	for (FOverlapResult HitOut : HitOuts) {
		AActor* HitActor = HitOut.GetActor();
		if (HitActor) {
			FVector RelativeDisplacement = (HitActor->GetActorLocation() - OriginLocation).GetAbs();
			if (RelativeDisplacement.X <= Range.X && RelativeDisplacement.Y <= Range.Y && RelativeDisplacement.Z <= Range.Z) {
				AEnemyCharacter* HitEnemyCharacter = Cast<AEnemyCharacter>(HitOut.GetActor());
				if (HitEnemyCharacter) {
					AEnemyCharAIController* HitEnemyCharController = Cast<AEnemyCharAIController>(HitEnemyCharacter->Controller);
					if (HitEnemyCharController) {
						if (AlertSuspicion.SuspicionType == ESuspicionType::EXPLICIT) {
							// Match Look Direction
							FRotator EnemyCharRotation = HitEnemyCharController->GetControlRotation();
							EnemyCharRotation.Yaw = ControlledPawnYaw;
							HitEnemyCharController->SetControlRotation(EnemyCharRotation);
							HitEnemyCharacter->SetActorRotation(EnemyCharRotation);
						}

						// Add Suspicion
						HitEnemyCharController->AddSuspicion(AlertSuspicion);
					}
				}
			}
		}
	}

	return EBTNodeResult::Succeeded;
}
