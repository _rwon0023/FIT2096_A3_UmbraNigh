// { Ryan Wong <32491034> 2022 }


#include "SecurityDroneAIController.h"

void ASecurityDroneAIController::OnPossess(APawn* InPawn)
{
	PossessedDrone = Cast<ASecurityDrone>(InPawn);
	if (PossessedDrone) {
		// Get patrol locations 
		PatrolLocationsArray = &(PossessedDrone->PatrolLocations);

		// Find Closest Patrol Point and set as destination
		FVector StartingLocation = PossessedDrone->GetActorLocation();
		float GoalDistance = FVector::Distance(StartingLocation, (*PatrolLocationsArray)[0]);
		GoalIndex = 0;
		for (size_t i = 1; i < PatrolLocationsArray->Num(); i++)
		{ // Compares distance against every patrol point and store closest's index
			float Dist;
			if ((Dist = FVector::Distance(StartingLocation, (*PatrolLocationsArray)[i])) < GoalDistance) {
				GoalDistance = Dist;
				GoalIndex = i;
			}
		}

		UE_LOG(LogTemp, Display, TEXT("<%s> Possessed by Security Drone AI"), *(PossessedDrone->GetActorNameOrLabel()));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Posses <%s> as Security Drone AI"), *InPawn->GetActorNameOrLabel());
	}
}

void ASecurityDroneAIController::Tick(float DeltaSeconds)
{
	if (PossessedDrone) {
		if (PossessedDrone->TraverseTowards((*PatrolLocationsArray)[GoalIndex], DeltaSeconds)) {
			GoalIndex++;
			if (GoalIndex == PatrolLocationsArray->Num()) {
				GoalIndex = 0;
			}
		}
	}
}

void ASecurityDroneAIController::OnUnPossess()
{
	PossessedDrone = nullptr;
}
