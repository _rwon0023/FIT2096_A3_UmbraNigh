// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "UmbraNigh/Interfaces/Detectable.h"
#include "UmbraNigh/AI/EnemyCharAIController.h"
#include "Kismet/GameplayStatics.h"

#include "AIController.h"
#include "EnemyDefenceAIController.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API AEnemyDefenceAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AEnemyDefenceAIController();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float SightRadius = 600;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float SightAge = 3.5;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float LoseSightRadius = SightRadius + 50;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float FieldOfView = 45;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		UAISenseConfig_Sight* SightConfiguration;

	FTimerHandle DetectionDelayHandle;
	float DetectionDelay = 0.3F;

	UFUNCTION()
		void OnSensesUpdated(AActor* UpdatedActor, FAIStimulus Stimulus);

	virtual void DetectActor(TScriptInterface<IDetectable> DetectedActor);
};
