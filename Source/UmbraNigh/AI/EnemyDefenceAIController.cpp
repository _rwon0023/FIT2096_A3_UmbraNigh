// { Ryan Wong <32491034> 2022 }


#include "EnemyDefenceAIController.h"

AEnemyDefenceAIController::AEnemyDefenceAIController()
{
	SightConfiguration = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Configuration"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

	SightConfiguration->SightRadius = SightRadius;
	SightConfiguration->LoseSightRadius = LoseSightRadius;
	SightConfiguration->PeripheralVisionAngleDegrees = FieldOfView;
	SightConfiguration->SetMaxAge(SightAge);

	SightConfiguration->DetectionByAffiliation.bDetectEnemies = true;
	SightConfiguration->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfiguration->DetectionByAffiliation.bDetectNeutrals = true;

	GetPerceptionComponent()->SetDominantSense(*SightConfiguration->GetSenseImplementation());
	GetPerceptionComponent()->ConfigureSense(*SightConfiguration);


	GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyDefenceAIController::OnSensesUpdated);
}

void AEnemyDefenceAIController::OnSensesUpdated(AActor* UpdatedActor, FAIStimulus Stimulus)
{
	if (!GetPawn() || UpdatedActor->IsHidden()) {
		return; // Ignore Hidden Actors
	}

	UE_LOG(LogTemp, Display, TEXT("{%s} Senses Detected <%s>"), *GetName(), *UpdatedActor->GetActorNameOrLabel());
	if (UpdatedActor->GetClass()->ImplementsInterface(UDetectable::StaticClass()))
	{
		if (Stimulus.WasSuccessfullySensed())
		{
			FSuspicion Suspicion;
			IDetectable::Execute_GetInitialDetectability(UpdatedActor, Suspicion, GetPawn());

			if (Suspicion.DetectionAmount == 0 || Suspicion.bSuspicionOnly)
				return;

			FTimerDelegate DetectDelegate = FTimerDelegate::CreateUObject(this, &AEnemyDefenceAIController::DetectActor, TScriptInterface<IDetectable>(UpdatedActor));
			GetWorld()->GetTimerManager().SetTimer(DetectionDelayHandle, DetectDelegate, DetectionDelay, false);
		}
	}
}

void AEnemyDefenceAIController::DetectActor(TScriptInterface<IDetectable> DetectedActor)
{
	TArray<AActor*> CurrentlyPerceived;
	GetPerceptionComponent()->GetKnownPerceivedActors(UAISenseConfig_Sight::StaticClass(), CurrentlyPerceived);
	if (!CurrentlyPerceived.Contains(DetectedActor.GetObjectRef())) {
		return;
	}

	FSuspicion AlarmSuspicion;
	AlarmSuspicion.bSuspicionOnly = true;
	AlarmSuspicion.DetectionAmount = 100;
	AlarmSuspicion.SuspicionType = ESuspicionType::IMPLICIT;

	// Raise Alarm
	TArray<AActor*> AlarmTargets;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyCharAIController::StaticClass(), AlarmTargets);
	for (AActor* AlarmTarget : AlarmTargets) {
		AEnemyCharAIController* AlarmedEnemyCharController = Cast<AEnemyCharAIController>(AlarmTarget);
		if (IsValid(AlarmedEnemyCharController)) {
			AlarmedEnemyCharController->AddSuspicion(AlarmSuspicion);
		}
	}

	UE_LOG(LogTemp, Display, TEXT("{%s} Raised the Alarm"), *GetName());
}
