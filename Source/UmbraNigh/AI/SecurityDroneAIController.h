// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Pawns/Enemies/EnemyDefences/SecurityDrone.h"

#include "UmbraNigh/AI/EnemyDefenceAIController.h"
#include "SecurityDroneAIController.generated.h"

UCLASS()
class UMBRANIGH_API ASecurityDroneAIController : public AEnemyDefenceAIController
{
	GENERATED_BODY()
	
public:
	ASecurityDrone* PossessedDrone;
	int GoalIndex;
	TArray<FVector>* PatrolLocationsArray;

	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void OnUnPossess() override;
};
