// { Ryan Wong <32491034> 2022 }


#include "EnemyCharAIController.h"

AEnemyCharAIController::AEnemyCharAIController()
{
	PrimaryActorTick.bCanEverTick = true;


	SuspicionThresholds = TArray<float>({ 50, 200 });
	SuspicionDecayRate = TMap<ESuspicionType, float>({ {ESuspicionType::EXPLICIT, 0.2F}, {ESuspicionType::IMPLICIT, 0.05F} });


	SightConfiguration = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Configuration"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

	SightConfiguration->SightRadius = SightRadius;
	SightConfiguration->LoseSightRadius = LoseSightRadius;
	SightConfiguration->PeripheralVisionAngleDegrees = FieldOfView;
	SightConfiguration->SetMaxAge(SightAge);

	SightConfiguration->DetectionByAffiliation.bDetectEnemies = true;
	SightConfiguration->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfiguration->DetectionByAffiliation.bDetectNeutrals = true;

	GetPerceptionComponent()->SetDominantSense(*SightConfiguration->GetSenseImplementation());
	GetPerceptionComponent()->ConfigureSense(*SightConfiguration);
	

	GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyCharAIController::OnSensesUpdated);
}

void AEnemyCharAIController::BeginPlay()
{
	Super::BeginPlay();

	NavigationSystem = Cast<UNavigationSystemV1>(GetWorld()->GetNavigationSystem());
	if (!NavigationSystem)
	{
		UE_LOG(LogTemp, Warning, TEXT("{%s} could not find Navigation System"), *GetName());
	}

	if (AIBlackboard && BehaviorTree) {
		UseBlackboard(AIBlackboard, BlackboardComponent);
		RunBehaviorTree(BehaviorTree);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("{%s} AI Blackboard or Behaviour Tree unassigned"), *GetName());
	}
}

void AEnemyCharAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	ControlledEnemyChar = Cast<AEnemyCharacter>(InPawn);
	if (ControlledEnemyChar) {
		PatrolIndex = ControlledEnemyChar->StartPatrolIndex;
		PatrolLocationsArray = &ControlledEnemyChar->PatrolLocations;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("{%s} failed to possess AEnemyCharacter Pawn"), *GetName());
	}
}

void AEnemyCharAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (ControlledEnemyChar) {
		BlackboardComponent->SetValueAsBool(FName("Enabled"), ControlledEnemyChar->bIsEnabled());
		
		UCharacterMovementComponent* ControlledEnemyCharMovement = ControlledEnemyChar->GetCharacterMovement();
		if (ControlledEnemyCharMovement->MovementMode != EMovementMode::MOVE_None) {
			if (ControlledEnemyChar->bIsImmobile()) {
				ControlledEnemyCharMovement->DisableMovement();
			}
		}
		else {
			ControlledEnemyCharMovement->SetMovementMode(EMovementMode::MOVE_NavWalking);
		}

		if (ControlledEnemyChar->bIsBlind_Implementation()) {
			bSensedCharacterInSight = false;
		}
		else {

			if (!IsValid(SensedUnconscious)) {
				SensedUnconscious = nullptr;
			}
			else {
				BlackboardComponent->SetValueAsVector(FName("SensedLocation"), SensedUnconscious->GetActorLocation());
			}
			BlackboardComponent->SetValueAsObject(FName("SensedUnconscious"), SensedUnconscious);


			if (SensedCharacter && IsValid(SensedCharacter) && SensedCharacter->bIsEnabled()) {
				BlackboardComponent->SetValueAsVector(FName("SensedLocation"), SensedCharacter->GetActorLocation());
			}
			else {
				if (bSensedCharacterInSight) {
					bSensedCharacterInSight = false;
				}
				SensedCharacter = nullptr;
			}

		}

		if (!bSensedCharacterInSight) {
			for (TPair<ESuspicionType, float>& SuspicionLevel : SuspicionLevels) {

				float* DecayRate = SuspicionDecayRate.Find(SuspicionLevel.Key);
				if (DecayRate != nullptr) {
					SuspicionLevels[SuspicionLevel.Key] = FMath::Max(0, SuspicionLevel.Value - *DecayRate);
				}
			}
		}

		UpdateAlertStatus();

		BlackboardComponent->SetValueAsBool(FName("SensedCharacterInSight"), bSensedCharacterInSight);
	}	
}

void AEnemyCharAIController::OnSensesUpdated(AActor* UpdatedActor, FAIStimulus Stimulus)
{
	if (!ControlledEnemyChar || UpdatedActor->IsHidden()) {
		return; // Ignore Hidden Actors
	}

	// UE_LOG(LogTemp, Display, TEXT("{%s} Senses Detected <%s>"), *GetName(), *UpdatedActor->GetActorNameOrLabel());
	if (UpdatedActor->GetClass()->ImplementsInterface(UDetectable::StaticClass()))
	{
		if (Stimulus.WasSuccessfullySensed())
		{	
			FSuspicion Suspicion;
			IDetectable::Execute_GetInitialDetectability(UpdatedActor, Suspicion, ControlledEnemyChar);
			
			if (Suspicion.DetectionAmount == 0) 
				return;

			AUmbraNighCharacter* CurrentSensedCharacter = Cast<AUmbraNighCharacter>(UpdatedActor);
			if (CurrentSensedCharacter) {
				if (CurrentSensedCharacter->Allegiance == EAllegiance::PLAYER && (!IsValid(SensedCharacter) || Suspicion.Priority > SensedCharacterPriority)) {
					SensedCharacter = CurrentSensedCharacter;
					SensedCharacterPriority = Suspicion.Priority;
				}
			}
			else {
				AUnconsciousBody* CurrentSensedUnconsious = Cast<AUnconsciousBody>(UpdatedActor);
				if (CurrentSensedUnconsious) {
					SensedUnconscious = CurrentSensedUnconsious;
				}
			}

			bSensedCharacterInSight = true;

			AddSuspicion(Suspicion);

			
			FTimerDelegate DetectDelegate = FTimerDelegate::CreateUObject(this, &AEnemyCharAIController::DetectActor, TScriptInterface<IDetectable>(UpdatedActor));
			GetWorld()->GetTimerManager().SetTimer(DetectionDelayHandle, DetectDelegate, DetectionDelay, false);
		}
		else {
			bSensedCharacterInSight = false;
		}
	}
}

void AEnemyCharAIController::DetectActor(TScriptInterface<IDetectable> DetectedActor)
{
	if (ControlledEnemyChar && DetectedActor && IsValid(DetectedActor.GetObjectRef())) {
		FSuspicion Suspicion;
		IDetectable::Execute_GetDetectability(DetectedActor.GetObjectRef(), Suspicion, ControlledEnemyChar);
		
		TArray<AActor*> CurrentlyPerceived;
		GetPerceptionComponent()->GetKnownPerceivedActors(UAISenseConfig_Sight::StaticClass(), CurrentlyPerceived);
		if (CurrentlyPerceived.Contains(DetectedActor.GetObjectRef())) {
			AddSuspicion(Suspicion);
		}
	}
}

void AEnemyCharAIController::CyclePatrol()
{
	if (ControlledEnemyChar) {
		ControlledEnemyChar->AimEnd();
	}

	if (PatrolLocationsArray) {
		if (PatrolLocationsArray->Num() > 0) {
			FPatrolPoint PatrolPoint;
			if (PatrolIndex < PatrolLocationsArray->Num()) {
				PatrolPoint = (*PatrolLocationsArray)[PatrolIndex];
				PatrolIndex++;
			}
			else {
				PatrolIndex = 0;
				PatrolPoint = (*PatrolLocationsArray)[0];
			}
			BlackboardComponent->SetValueAsVector(FName("PatrolLocation"), PatrolPoint.PatrolLocation);
			BlackboardComponent->SetValueAsFloat(FName("PatrolWaitTime"), PatrolPoint.PatrolWaitTime);

			// UE_LOG(LogTemp, Display, TEXT("{%s} Cycled Patrol"), *GetName());
		}
	}
}

void AEnemyCharAIController::AddSuspicion(FSuspicion Suspicion)
{
	float SuspicionLevel = SuspicionLevels.Contains(Suspicion.SuspicionType) ? SuspicionLevels[Suspicion.SuspicionType] : 0;

	if (Suspicion.SuspicionType == ESuspicionType::EXPLICIT) {
		float* CurrentImplicitSuspicion = SuspicionLevels.Find(ESuspicionType::IMPLICIT);
		if (CurrentImplicitSuspicion) {
			SuspicionLevel += *CurrentImplicitSuspicion;
		}
	}

	SuspicionLevel += Suspicion.DetectionAmount;
	if (SuspicionThresholds.Num() >= 2) {
		SuspicionLevel = FMath::Min(SuspicionLevel, SuspicionThresholds[1]);
	}

	SuspicionLevels.Add(Suspicion.SuspicionType, SuspicionLevel);

	UpdateAlertStatus();
}

void AEnemyCharAIController::UpdateAlertStatus()
{
	if (SuspicionThresholds.Num() > 0) {
		float* CurrentExplicitSuspicion = SuspicionLevels.Find(ESuspicionType::EXPLICIT);
		
		/* Debugging */
		float* CurrentImplicitSuspicion = SuspicionLevels.Find(ESuspicionType::IMPLICIT);
		if (CurrentImplicitSuspicion != nullptr) GEngine->AddOnScreenDebugMessage((int32)(intptr_t)this, 2.0f, FColor::Red, FString::Printf(TEXT("{%s} Implicit Suspicion: <%F>"), *GetActorNameOrLabel(), *CurrentImplicitSuspicion));
		if (CurrentExplicitSuspicion != nullptr) GEngine->AddOnScreenDebugMessage((int32)(intptr_t)this + 1, 2.0f, FColor::Red, FString::Printf(TEXT("{%s} Explicit Suspicion: <%F>"), *GetActorNameOrLabel(), *CurrentExplicitSuspicion));
		//*/

		bIsAlert = false;
		if (CurrentExplicitSuspicion != nullptr) {
			if (*CurrentExplicitSuspicion >= SuspicionThresholds[0]) {
				bIsAlert = true;
			}
		}
	}

	BlackboardComponent->SetValueAsBool(FName("IsAlert"), bIsAlert);
}

