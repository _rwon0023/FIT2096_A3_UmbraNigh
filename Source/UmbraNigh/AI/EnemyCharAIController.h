// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "NavigationSystem.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "UmbraNigh/Pawns/Enemies/EnemyCharacter.h"
#include "UmbraNigh/Interfaces/Detectable.h"

#include "AIController.h"
#include "EnemyCharAIController.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API AEnemyCharAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AEnemyCharAIController();

	AEnemyCharacter* ControlledEnemyChar;

	TMap<ESuspicionType, float> SuspicionLevels;

	TMap<ESuspicionType, float> SuspicionDecayRate;
	TArray<float> SuspicionThresholds;

	AUmbraNighCharacter* SensedCharacter;
	float SensedCharacterPriority;

	AUnconsciousBody* SensedUnconscious;

	bool bIsAlert;
	bool bSensedCharacterInSight;

	FTimerHandle DetectionDelayHandle;
	float DetectionDelay = 0.05F;

	int PatrolIndex = 0;
	TArray<FPatrolPoint>* PatrolLocationsArray;

	UPROPERTY (EditDefaultsOnly, Category = Blackboard)
		UBlackboardData* AIBlackboard;
	UPROPERTY(EditDefaultsOnly, Category = Blackboard)
		UBehaviorTree* BehaviorTree;
	UPROPERTY(EditDefaultsOnly, Category = Blackboard)
		UBlackboardComponent* BlackboardComponent;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float SightRadius = 1000;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float SightAge = 3.5;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float LoseSightRadius = SightRadius + 1500;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float FieldOfView = 80;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		UAISenseConfig_Sight* SightConfiguration;

	UNavigationSystemV1* NavigationSystem;

	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
		void OnSensesUpdated(AActor* UpdatedActor, FAIStimulus Stimulus);
	
	void DetectActor(TScriptInterface<IDetectable> DetectedActor);
	void CyclePatrol();

	void AddSuspicion(FSuspicion Suspicion);

	void UpdateAlertStatus();

};
