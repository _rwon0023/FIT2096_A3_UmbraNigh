// { Ryan Wong <32491034> 2022 }


#include "DepletableValue.h"

UDepletableValue::UDepletableValue()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UDepletableValue::InitialiseAsFull(int Value)
{
	MaxValue = Value;
	ReplenishAll();
}

void UDepletableValue::ReplenishAll()
{
	CurrentValue = MaxValue;
}

void UDepletableValue::DepleteAll()
{
	Deplete(CurrentValue);
}

void UDepletableValue::Replenish(int Amount)
{
	CurrentValue += Amount;
	CurrentValue = FMath::Min(CurrentValue, MaxValue);
}

void UDepletableValue::Deplete(int Amount)
{
	if (this) {
		CurrentValue -= Amount;
		CurrentValue = FMath::Max(CurrentValue, 0);
		if (bIsEmpty()) {
			DepletedDelegate.Broadcast();
		}
		UE_LOG(LogTemp, Display, TEXT("Depleted {%s} for <%d>: [%d / %d]"), *GetName(), Amount, CurrentValue, MaxValue);
	}
}

bool UDepletableValue::bIsFull()
{
	return CurrentValue >= MaxValue;
}

bool UDepletableValue::bIsEmpty()
{
	return CurrentValue <= 0;
}

float UDepletableValue::GetPercentFull()
{
	return (MaxValue == 0) ? 0 : (float) CurrentValue / MaxValue;
}
