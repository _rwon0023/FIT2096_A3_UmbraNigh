// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DepletableValue.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDepleted);

UCLASS()
class UMBRANIGH_API UDepletableValue : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
		int MaxValue;
	UPROPERTY(EditAnywhere)
		int CurrentValue;

public:
	UDepletableValue();

	FOnDepleted DepletedDelegate;

	void InitialiseAsFull(int Value);
	void ReplenishAll();
	void DepleteAll();
	void Replenish(int Amount);
	void Deplete(int Amount);
	bool bIsFull();
	bool bIsEmpty();
	float GetPercentFull();
		
};
