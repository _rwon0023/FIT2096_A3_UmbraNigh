// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Containers/Map.h"

#include "BlueprintDefaultHelper.generated.h"

UENUM()
enum class EBlueprintDefaultHelperRef : uint8 {
	NONE,
	CLASS_SHADOW_HAND_PAWN,
	CLASS_RAVENRAT_PAWN,
	CLASS_ILLUSORYDOLL_PAWN,
	CLASS_SLEEPFEATHER_PAWN,
	CLASS_DARKTAG_PAWN,
	CLASS_SPELLS_UI,
	CLASS_POSSESSION_UI,
	PS_SUMMON_CLOUD,
	PS_DARK_COALESCE,
	PS_HIT_MIST,
	OBJ_PLAYER,
	OBJ_MAT_SEE_STENCIL_ONLY,
	OBJ_SPHERE,
	OBJ_CONTROLDIAMOND,
};

/*
* Statics Class for getting references to Blueprints via C++ code
* Collected here to centralise hard-coded references such that there is a single place I need to go to update any changed paths
*/
UCLASS(Blueprintable, BlueprintType)
class UMBRANIGH_API UBlueprintDefaultHelper : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

private:
	static TMap<EBlueprintDefaultHelperRef, TCHAR*> PathMap;
public:

	template <class T>
	static UClass* GetDefaultClass(EBlueprintDefaultHelperRef PathRef) {
		static_assert(TPointerIsConvertibleFromTo<T, const UObject>::Value, "'T' template parameter to GetDefaultClass must be derived from UObject");

		TCHAR** PathText = UBlueprintDefaultHelper::PathMap.Find(PathRef);
		ConstructorHelpers::FClassFinder<T> DefaultClass((PathText) ? *PathText : TEXT(""));
		if (DefaultClass.Succeeded() && DefaultClass.Class) {
			UE_LOG(LogTemp, Display, TEXT("Default <%s> Class Assigned"), *DefaultClass.Class.Get()->GetFName().ToString());
			return DefaultClass.Class;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Provided Default Class failed to Assign, Defaulting to <%s>"), *T::StaticClass()->GetFName().ToString());
			return T::StaticClass();
		}
	};

	template <class T>
	static T* GetBlueprintObject(EBlueprintDefaultHelperRef PathRef) {
		static_assert(TPointerIsConvertibleFromTo<T, const UObject>::Value, "'T' template parameter to GetBlueprintObject must be derived from UObject");

		TCHAR** PathText = UBlueprintDefaultHelper::PathMap.Find(PathRef);
		ConstructorHelpers::FObjectFinder<T> BlueprintObject((PathText) ? *PathText : TEXT(""));
		if (BlueprintObject.Succeeded() && BlueprintObject.Object) {
			UE_LOG(LogTemp, Display, TEXT("Blueprint <%s> found"), *BlueprintObject.Object->GetName());
			return BlueprintObject.Object;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Unable to find Blueprint object, attempting to generate using class"));
			UClass* ClassFind = GetDefaultClass<T>(PathRef);
			if (ClassFind) {
				return NewObject<T>(ClassFind);
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("Unable to find Blueprint nor Blueprint class, Defaulting to new <%s>"), *T::StaticClass()->GetFName().ToString());
				return NewObject<T>();
			}
		}
		return BlueprintObject.Object;
	};
};