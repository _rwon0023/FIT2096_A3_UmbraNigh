// { Ryan Wong <32491034> 2022 }


#include "BlueprintDefaultHelper.h"

TMap<EBlueprintDefaultHelperRef, TCHAR*> UBlueprintDefaultHelper::PathMap{
	{EBlueprintDefaultHelperRef::CLASS_SHADOW_HAND_PAWN, TEXT("BlueprintGeneratedClass'/Game/_UmbraNigh/Blueprints/BP_ShadowHand.BP_ShadowHand_C'")},
	{EBlueprintDefaultHelperRef::CLASS_RAVENRAT_PAWN, TEXT("BlueprintGeneratedClass'/Game/_UmbraNigh/Blueprints/BP_Ravenrat.BP_Ravenrat_C'")},
	{EBlueprintDefaultHelperRef::CLASS_ILLUSORYDOLL_PAWN, TEXT("BlueprintGeneratedClass'/Game/_UmbraNigh/Blueprints/BP_IllusoryDoll.BP_IllusoryDoll_C'")},
	{EBlueprintDefaultHelperRef::CLASS_SLEEPFEATHER_PAWN, TEXT("BlueprintGeneratedClass'/Game/_UmbraNigh/Blueprints/BP_SleepFeather.BP_SleepFeather_C'")},
	{EBlueprintDefaultHelperRef::CLASS_DARKTAG_PAWN, TEXT("BlueprintGeneratedClass'/Game/_UmbraNigh/Blueprints/BP_DarkTag.BP_DarkTag_C'")},
	{EBlueprintDefaultHelperRef::CLASS_SPELLS_UI, TEXT("WidgetBlueprintGeneratedClass'/Game/_UmbraNigh/UI/BP_GrimoireSpellsUI.BP_GrimoireSpellsUI_C'")},
	{EBlueprintDefaultHelperRef::CLASS_POSSESSION_UI, TEXT("WidgetBlueprintGeneratedClass'/Game/_UmbraNigh/UI/BP_GrimoirePossessionUI.BP_GrimoirePossessionUI_C'")},
	{EBlueprintDefaultHelperRef::PS_SUMMON_CLOUD, TEXT("NiagaraSystem'/Game/_UmbraNigh/Particles/PS_SummonCloud.PS_SummonCloud'")},
	{EBlueprintDefaultHelperRef::PS_DARK_COALESCE, TEXT("NiagaraSystem'/Game/_UmbraNigh/Particles/PS_DarkCoalesce.PS_DarkCoalesce'")},
	{EBlueprintDefaultHelperRef::PS_HIT_MIST, TEXT("NiagaraSystem'/Game/_UmbraNigh/Particles/PS_HitMist.PS_HitMist'")},
	{EBlueprintDefaultHelperRef::OBJ_PLAYER, TEXT("Blueprint'/Game/_UmbraNigh/Blueprints/BP_TheWitch.BP_TheWitch'")},
	{EBlueprintDefaultHelperRef::OBJ_MAT_SEE_STENCIL_ONLY, TEXT("Material'/Game/_UmbraNigh/Materials/PostProcess/SeeStencilOnly.SeeStencilOnly'")},
	{EBlueprintDefaultHelperRef::OBJ_SPHERE, TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'")},
	{EBlueprintDefaultHelperRef::OBJ_CONTROLDIAMOND, TEXT("StaticMesh'/ControlRig/Controls/ControlRig_Diamond_solid.ControlRig_Diamond_solid'")},
};
