// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Common/DepletableValue.h"
#include "UmbraNigh/Interfaces/Shootable.h"
#include "NiagaraFunctionLibrary.h"

#include "GameFramework/Actor.h"
#include "Firearm.generated.h"

UENUM()
enum class EAmmoType : uint8 {
	GENERIC,
	NIGHT_BULLET
};

UCLASS()
class UMBRANIGH_API AFirearm : public AActor
{
	GENERATED_BODY()
	
private:
	inline static const float AIM_HIT_DIST_TOLERANCE = 1.0F; // Distance tolerance for considering if a shot hit where it was aiming for

public:	
	AFirearm();

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere)
		UDepletableValue* Ammo;
	UPROPERTY(EditAnywhere)
		UNiagaraSystem* PS_HitEffect;
	UPROPERTY(EditAnywhere)
		EAmmoType AmmoType;
	UPROPERTY(EditAnywhere)
		float Range;
	UPROPERTY(EditAnywhere)
		int Damage;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void OnHit(TScriptInterface<IShootable> Target);
	virtual void OnHeadshot(TScriptInterface<IShootable> Target);
	virtual void PlayHitEffect(FVector Location);
	void ShootAt(FVector AimTarget, FVector AimDirection, AActor* Shooter);
	void Reload();
	bool bIsAmmoFull();
	bool bIsAmmoEmpty();
	void SetEquipped(bool bIsEquipped);
};
