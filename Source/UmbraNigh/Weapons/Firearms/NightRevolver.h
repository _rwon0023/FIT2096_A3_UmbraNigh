// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Interfaces/Blindable.h"

#include "Firearm.h"
#include "NightRevolver.generated.h"

/**
 * 
 */
UCLASS()
class UMBRANIGH_API ANightRevolver : public AFirearm
{
	GENERATED_BODY()
	
public:
	ANightRevolver();

	virtual void OnHeadshot(TScriptInterface<IShootable> Target) override;
	virtual void PlayHitEffect(FVector Location) override;
};
