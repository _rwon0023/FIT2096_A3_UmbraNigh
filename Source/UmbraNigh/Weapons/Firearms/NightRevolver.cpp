// { Ryan Wong <32491034> 2022 }


#include "NightRevolver.h"

ANightRevolver::ANightRevolver()
{
	AmmoType = EAmmoType::NIGHT_BULLET;
	Damage = 2;
	Ammo->InitialiseAsFull(8);
}

void ANightRevolver::OnHeadshot(TScriptInterface<IShootable> Target)
{
	Super::OnHeadshot(Target);

	if (Target && IsValid(Target.GetObjectRef())) {
		if (Target.GetObjectRef()->GetClass()->ImplementsInterface(UBlindable::StaticClass())) {
			IBlindable::Execute_DoDefaultBlind(Target.GetObjectRef());
		}
	}
}

void ANightRevolver::PlayHitEffect(FVector Location)
{
	Super::PlayHitEffect(Location);
}
