// { Ryan Wong <32491034> 2022 }


#include "Firearm.h"
#include "UmbraNigh/Pawns/UmbraNighCharacter.h"

AFirearm::AFirearm()
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	// Set up mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	// Set up ammo
	Ammo = CreateDefaultSubobject<UDepletableValue>(TEXT("Ammo"));
	AddOwnedComponent(Ammo);

	// Initialise Attribute Defaults
	AmmoType = EAmmoType::GENERIC;
	Range = 10000;
	Damage = 5;
	Ammo->InitialiseAsFull(10);
}

void AFirearm::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFirearm::OnHit(TScriptInterface<IShootable> Target)
{
	UE_LOG(LogTemp, Display, TEXT("{%s} triggered hit"), *GetName());
	if (Target) {
		IShootable::Execute_DealDamage(Target.GetObjectRef(), Damage);
	}
}

void AFirearm::OnHeadshot(TScriptInterface<IShootable> Target)
{
	UE_LOG(LogTemp, Display, TEXT("{%s} triggered headshot"), *GetName());
	if (Target) {
		IShootable::Execute_DealDamage(Target.GetObjectRef(), Damage*2);
	}
}

void AFirearm::PlayHitEffect(FVector Location)
{
	if (PS_HitEffect) {
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), PS_HitEffect, Location);
	}
}

void AFirearm::ShootAt(FVector AimTarget, FVector AimDirection, AActor* Shooter)
{
	UE_LOG(LogTemp, Display, TEXT("{%s} attempts to shoot at <%s>"), *GetActorNameOrLabel(), *AimTarget.ToString());

	if (bIsAmmoEmpty()) return; // Break early if trying to shoot with no ammo

	// Deplete 1 ammo use
	Ammo->Deplete(1); 

	AimDirection.Normalize();

	// Trace from Firearm to aimed-at target

	FVector TraceStart = GetActorLocation();
	FVector Direction = AimTarget - TraceStart;
	Direction.Normalize();
	const FVector TraceEnd = TraceStart + Direction * Range;

	// Set up Trace Params
	FCollisionQueryParams TraceParams(FName(TEXT("Firearm Shoot Trace")), true, this);
	TraceParams.bReturnPhysicalMaterial = false;
	FHitResult HitOut = FHitResult(ForceInit);
	// Ignore shooter if relevant
	if (Shooter) {
		TraceParams.AddIgnoredActor(Shooter);
		TArray<AActor*> ShooterAttached;
		Shooter->GetAttachedActors(ShooterAttached);
		for (AActor* ShooterAttachedActor : ShooterAttached) {
			if (ShooterAttachedActor) {
				TraceParams.AddIgnoredActor(ShooterAttachedActor);
			}
		}
		// UE_LOG(LogTemp, Display, TEXT("{%s} ignores <%s>"), *GetActorNameOrLabel(), *Shooter->GetActorNameOrLabel());
	}

	/* Debugging */
	const FName TraceTag("FirearmShootTrace");
	GetWorld()->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;
	//*/

	// Perform Trace on Shootables from this firearm to the target position
	GetWorld()->LineTraceSingleByChannel(HitOut, TraceStart, TraceEnd, IShootable::SHOOTABLE_TRACE_CHANNEL, TraceParams);

	FVector HitLocation = (HitOut.bBlockingHit) ? HitOut.ImpactPoint : TraceEnd;
	AActor* HitActor = HitOut.GetActor();

	if (HitActor) { // Check if an actor was hit
		UE_LOG(LogTemp, Display, TEXT("{%s} hits <%s> with a shot"), *GetActorNameOrLabel(), *HitActor->GetActorNameOrLabel());

		PlayHitEffect(HitLocation);

		if (HitActor->GetClass()->ImplementsInterface(UShootable::StaticClass())) { // Check if hit Actor is Shootable, and call OnShot if so

			if (FVector::Distance(HitOut.ImpactPoint, AimTarget) < AIM_HIT_DIST_TOLERANCE) {
				IShootable::Execute_OnShot(HitActor, HitOut, AimDirection, this); // If shot hit where it was aiming, pass in AimDirection to check for things like headshots
			}
			else {
				IShootable::Execute_OnShot(HitActor, HitOut, Direction, this); // If shot was obstructed before reaching AimTarget, use the current trace's direction instead
			}

		}
	}
	else {
		UE_LOG(LogTemp, Display, TEXT("{%s} shoots a bullet, hitting nothing"), *GetActorNameOrLabel())
	}
}

void AFirearm::Reload()
{
	Ammo->ReplenishAll();
}

bool AFirearm::bIsAmmoFull()
{
	return Ammo->bIsFull();
}

bool AFirearm::bIsAmmoEmpty()
{
	return Ammo->bIsEmpty();
}

void AFirearm::SetEquipped(bool bIsEquipped)
{
	if (Mesh) {
		TArray<USceneComponent*> MeshComponents;
		Mesh->GetChildrenComponents(true, MeshComponents);

		if (bIsEquipped) {
			for (USceneComponent* Component : MeshComponents) {
				UMeshComponent* FirearmMesh = Cast<UMeshComponent>(Component);
				if (FirearmMesh) {
					FirearmMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				}
			}
		}
		else {
			Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		}
	}
}

