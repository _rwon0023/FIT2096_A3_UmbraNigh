// { Ryan Wong <32491034> 2022 }


#include "PlayerTheWitch.h"


APlayerTheWitch::APlayerTheWitch()
{
	Allegiance = EAllegiance::PLAYER;
	DefaultFirearm = ANightRevolver::StaticClass();
}


void APlayerTheWitch::BeginPlay()
{
	Super::BeginPlay();
}

bool APlayerTheWitch::CanInteract_Implementation(TScriptInterface<IInteractable>& Interactable)
{
	return true;
}

void APlayerTheWitch::DoPrimaryAction_Implementation()
{
	Super::DoPrimaryAction_Implementation();
	
	if (bIsCrouched && !bIsAiming) {
		FVector TraceStart = GetActorLocation();
		FVector Direction = GetActorForwardVector();
		const FVector TraceEnd = TraceStart + Direction * SneakAttackRange;

		// Set up Trace Params
		FCollisionQueryParams TraceParams(FName(TEXT("Player Backstab Trace")), true, this);
		TraceParams.bReturnPhysicalMaterial = false;
		FHitResult HitOut = FHitResult(ForceInit);
		/* Debugging * /
		const FName TraceTag("PlayerBackstabTrace");
		GetWorld()->DebugDrawTraceTag = TraceTag;
		TraceParams.TraceTag = TraceTag;
		//*/

		// Perform Trace
		GetWorld()->LineTraceSingleByChannel(HitOut, TraceStart, TraceEnd, ECollisionChannel::ECC_Pawn, TraceParams);


		AUmbraNighCharacter* HitCharacter = Cast<AUmbraNighCharacter>(HitOut.GetActor());
		if (HitCharacter && HitCharacter->bSneakAttackable) {
			if (HitCharacter->GetActorForwardVector().Dot(Direction) > 0.4F) {
				UE_LOG(LogTemp, Display, TEXT("{%s} sneak attacks <%s>"), *GetActorNameOrLabel(), *HitCharacter->GetActorNameOrLabel());

				Restraints.Add(HitCharacter);
				HitCharacter->Restraints.Add(this);

				FTimerDelegate SneakAttackDelegate = FTimerDelegate::CreateUObject(this, &APlayerTheWitch::Backstab, HitCharacter);
				GetWorld()->GetTimerManager().SetTimer(ActionDurationHandle, SneakAttackDelegate, SneakAttackDuration, false);
			}
		}
	}
}



void APlayerTheWitch::GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::IMPLICIT;
	SuspicionOut.DetectionAmount = 20;
	SuspicionOut.bSuspicionOnly = true;

	Super::GetInitialDetectability_Implementation(SuspicionOut, Detector);
}

void APlayerTheWitch::GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector)
{
	SuspicionOut.SuspicionType = ESuspicionType::EXPLICIT;
	SuspicionOut.DetectionAmount = 45;
	SuspicionOut.bSuspicionOnly = false;

	Super::GetDetectability_Implementation(SuspicionOut, Detector);
}



void APlayerTheWitch::Backstab(AUmbraNighCharacter* Target)
{
	if (Target) {
		Restraints.Remove(Target);
		Target->HP->DepleteAll();
	}
}

