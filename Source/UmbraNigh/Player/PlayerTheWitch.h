// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Weapons/Firearms/NightRevolver.h"

#include "UmbraNigh/Pawns/UmbraNighCharacter.h"
#include "PlayerTheWitch.generated.h"


UCLASS()
class UMBRANIGH_API APlayerTheWitch : public AUmbraNighCharacter
{
	GENERATED_BODY()
	
public:
	APlayerTheWitch();

	FTimerHandle ActionDurationHandle;

	UPROPERTY(EditAnywhere)
	float SneakAttackDuration = 1.0F;
	UPROPERTY(EditAnywhere)
	float SneakAttackRange = 100.0F;

protected:
	virtual void BeginPlay() override;

public:
	virtual bool CanInteract_Implementation(TScriptInterface<IInteractable>& Interactable) override;
	virtual void DoPrimaryAction_Implementation() override;

	virtual void GetInitialDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;
	virtual void GetDetectability_Implementation(FSuspicion& SuspicionOut, AActor* Detector) override;

	void Backstab(AUmbraNighCharacter* Target);
};
