// Ryan Wong 32491034 2022

#pragma once

#include "CoreMinimal.h"

#include "UmbraNigh/Player/PlayerTheWitch.h"
#include "Blueprint/UserWidget.h"
#include "UmbraNigh/Common/BlueprintDefaultHelper.h"

#include "UmbraNigh/Interfaces/Playable.h"
#include "UmbraNigh/Interfaces/Interactable.h"
#include "UmbraNigh/Magic/Grimoire.h"
#include "UmbraNigh/Magic/GrimoireFactory.h"
#include "UmbraNigh/UI/GrimoireSpellsUI.h"
#include "UmbraNigh/UI/GrimoirePossessionUI.h"

#include "GameFramework/PlayerController.h"
#include "UmbraNighPlayerController.generated.h"

// Forward Declaration
class IPlayable;

UCLASS()
class UMBRANIGH_API AUmbraNighPlayerController : public APlayerController
{
	GENERATED_BODY()

private:
	inline static const float INTERACTION_DETECTION_RANGE = 3000.0F;

public:
	DECLARE_DELEGATE_OneParam(FInteractionDelegate, EInteractionType);

	TScriptInterface<IPlayable> CurrentPawn;
	TScriptInterface<IPlayable> MainPlayerPawn;
	UCameraComponent* CurrentCamera;

	UPROPERTY(EditAnywhere)
	float InteractionRange = 200.0F;

	UPROPERTY(EditAnywhere)
	UGrimoire* PlayerGrimoire;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> SpellsUIClass;
	
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> PossessionUIClass;

private:
	FVector MovementInput;
	FVector LookInput;

	EInteractionType CurrentInteraction;
	bool bIsInteracting = false;

	bool bUIOpen = false;
	UUserWidget* SpellsUI;
	UUserWidget* PossessionUI;

public:
	AUmbraNighPlayerController();

	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;
	virtual void Tick(float DeltaSeconds) override;
	
private:
	UFUNCTION()
	virtual void OnMainPlayerDies();

	bool TryToggleUI(UClass* UIClass, UUserWidget*& UIRef);
	UFUNCTION()
		void ToggleSpellsUI();
	UFUNCTION()
		void TogglePossessionUI();
	UFUNCTION()
		void OnGrimoirePossessionChanged();
	
	TScriptInterface<IInteractable> CheckInteraction();

	void MoveForwardBack(float AxisValue);
	void MoveRightLeft(float AxisValue);
	void LookPitch(float AxisValue);
	void LookYaw(float AxisValue);
	void Jump();
	void JumpEnd();
	void Sneak();
	void SneakEnd();
	void SneakToggle();
	void PrimaryAction();
	void PrimaryActionRelease();
	void SecondaryAction();
	void SecondaryActionRelease();
	void DoInteraction(EInteractionType InteractionType);
};
