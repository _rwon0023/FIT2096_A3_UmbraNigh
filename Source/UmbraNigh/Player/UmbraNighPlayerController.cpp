// Ryan Wong 32491034 2022


#include "UmbraNighPlayerController.h"

AUmbraNighPlayerController::AUmbraNighPlayerController()
{
	SpellsUIClass = UBlueprintDefaultHelper::GetDefaultClass<UUserWidget>(EBlueprintDefaultHelperRef::CLASS_SPELLS_UI);
	PossessionUIClass = UBlueprintDefaultHelper::GetDefaultClass<UUserWidget>(EBlueprintDefaultHelperRef::CLASS_POSSESSION_UI);
}


void AUmbraNighPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("PrimaryAction", IE_Pressed, this, &AUmbraNighPlayerController::PrimaryAction);
	InputComponent->BindAction("PrimaryAction", IE_Released, this, &AUmbraNighPlayerController::PrimaryActionRelease);
	InputComponent->BindAction("SecondaryAction", IE_Pressed, this, &AUmbraNighPlayerController::SecondaryAction);
	InputComponent->BindAction("SecondaryAction", IE_Released, this, &AUmbraNighPlayerController::SecondaryActionRelease);

	InputComponent->BindAction("Jump", IE_Pressed, this, &AUmbraNighPlayerController::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &AUmbraNighPlayerController::JumpEnd);

	InputComponent->BindAction("Sneak", IE_Pressed, this, &AUmbraNighPlayerController::Sneak);
	InputComponent->BindAction("Sneak", IE_Released, this, &AUmbraNighPlayerController::SneakEnd);

	InputComponent->BindAction("Sneak Toggle", IE_Pressed, this, &AUmbraNighPlayerController::SneakToggle);

	InputComponent->BindAxis("Move Vertical", this, &AUmbraNighPlayerController::MoveForwardBack);
	InputComponent->BindAxis("Move Horizontal", this, &AUmbraNighPlayerController::MoveRightLeft);

	InputComponent->BindAxis("Mouse Look Vertical", this, &AUmbraNighPlayerController::LookPitch);
	InputComponent->BindAxis("Mouse Look Horizontal", this, &AUmbraNighPlayerController::LookYaw);

	InputComponent->BindAction("Toggle Spellbook", IE_Pressed, this, &AUmbraNighPlayerController::ToggleSpellsUI).bExecuteWhenPaused = true;
	InputComponent->BindAction("Toggle Possession Menu", IE_Pressed, this, &AUmbraNighPlayerController::TogglePossessionUI).bExecuteWhenPaused = true;

	InputComponent->BindAction<FInteractionDelegate>("Interact", IE_Pressed, this, &AUmbraNighPlayerController::DoInteraction, EInteractionType::INTERACT);
	InputComponent->BindAction<FInteractionDelegate>("Handle", IE_Pressed, this, &AUmbraNighPlayerController::DoInteraction, EInteractionType::HANDLE);
}

void AUmbraNighPlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeGameOnly());
}

void AUmbraNighPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (InPawn) {
		if (InPawn->GetClass()->ImplementsInterface(UPlayable::StaticClass())) {
			// Assign Current Possessed Playable
			CurrentPawn = InPawn;
			UE_LOG(LogTemp, Display, TEXT("<%s> Possessed by Player"), *(Cast<AActor>(CurrentPawn.GetObject())->GetActorNameOrLabel()));
			
			// Assign Main Player Pawn if not already set
			if (!MainPlayerPawn && Cast<APlayerTheWitch>(CurrentPawn.GetObjectRef())) {
				MainPlayerPawn = CurrentPawn;
				AUmbraNighCharacter* MainPlayerCharacter = Cast<AUmbraNighCharacter>(MainPlayerPawn.GetObjectRef());
				MainPlayerCharacter->HP->DepletedDelegate.AddDynamic(this, &AUmbraNighPlayerController::OnMainPlayerDies);

				FString MainPlayerName = (Cast<AActor>(MainPlayerPawn.GetObject())->GetActorNameOrLabel());
				UE_LOG(LogTemp, Display, TEXT("<%s> Assigned as Main Player Pawn"), *MainPlayerName);

				// Bind player to Grimoire as its Spellcaster
				UGrimoireFactory::GenerateDefaultGrimoire(PlayerGrimoire);
				if (IsValid(PlayerGrimoire)) {
					if (MainPlayerCharacter) {
						PlayerGrimoire->AddPossessable(MainPlayerPawn);
						PlayerGrimoire->Spellcaster = MainPlayerCharacter;
						PlayerGrimoire->CastInitialSpells();
						PlayerGrimoire->PossessionChangedDelegate.AddDynamic(this, &AUmbraNighPlayerController::OnGrimoirePossessionChanged);
						UE_LOG(LogTemp, Display, TEXT("<%s> successfully bound to Grimoire"), *MainPlayerName);
					}
					else {
						UE_LOG(LogTemp, Warning, TEXT("<%s> failed to bind to Grimoire: Could not be cast to AUmbraNighCharacter"), *MainPlayerName);
					}
				}
				else {
					UE_LOG(LogTemp, Warning, TEXT("<%s> failed to bind to Grimoire: No Grimoire Exists"), *MainPlayerName);
				}
			}

			// Assign Current Camera
			CurrentCamera = IPlayable::Execute_GetCamera(InPawn);
			if (CurrentCamera) {
				UE_LOG(LogTemp, Display, TEXT("<%s> Assigned as Current Player Camera"), *CurrentCamera->GetFName().ToString());
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Player Controller tried to possess non-Playable pawn"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Player Controller tried to possess null pawn"));
	}
}

void AUmbraNighPlayerController::OnUnPossess()
{
	UE_LOG(LogTemp, Display, TEXT("Player Controller unpossessing"));

	Super::OnUnPossess();
}

void AUmbraNighPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (MainPlayerPawn) {
		// Check Possessed Playable, refresh possession if need be
		if (!CurrentPawn || !IsValid(CurrentPawn.GetObjectRef())) {
			OnGrimoirePossessionChanged();
		}

		// Perform Movement
		if (CurrentPawn && CurrentCamera && !MovementInput.IsZero()) {
			// UE_LOG(LogTemp, Display, TEXT("Received Raw Player Movement Input <%s>"), *MovementInput.ToString());

			MovementInput.Normalize();
			MovementInput = MovementInput.RotateAngleAxis(CurrentCamera->GetComponentRotation().Yaw, FVector::UpVector);
			IPlayable::Execute_DoMove(CurrentPawn.GetObject(), MovementInput);
		}
	}
}

void AUmbraNighPlayerController::OnMainPlayerDies()
{
	CurrentPawn = nullptr;
	MainPlayerPawn = nullptr;
	bShowMouseCursor = true;
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 0.4);
}

bool AUmbraNighPlayerController::TryToggleUI(UClass* UIClass, UUserWidget*& UIRef)
{
	UE_LOG(LogTemp, Display, TEXT("UI Toggle Requested"))
	if (UIClass) {
		if (!UIRef) {
			bUIOpen = true;
			SetPause(true);

			UIRef = CreateWidget<UUserWidget>(this, UIClass);
			UIRef->AddToViewport();
			SetInputMode(FInputModeGameAndUI());
			bShowMouseCursor = true;
			UE_LOG(LogTemp, Display, TEXT("UI Created"));
			return true;
		}
		else {
			bUIOpen = false;
			SetPause(false);

			UIRef->RemoveFromParent();
			UIRef = nullptr;
			bShowMouseCursor = false;
			SetInputMode(FInputModeGameOnly());
			UE_LOG(LogTemp, Display, TEXT("UI Removed"))
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UI Class Attempted to be Toggled is not Assigned"))
	}
	return false;
}

void AUmbraNighPlayerController::ToggleSpellsUI()
{
	UE_LOG(LogTemp, Display, TEXT("Spells UI Toggle Requested"));
	if (IsValid(PlayerGrimoire)) {
		if (PossessionUI)TogglePossessionUI();

		if (TryToggleUI(SpellsUIClass, SpellsUI)) {
			UGrimoireSpellsUI* _SpellsUI = Cast<UGrimoireSpellsUI>(SpellsUI);
			if (IsValid(_SpellsUI)) { // If just created, initialise
				_SpellsUI->AssignSpellbook(PlayerGrimoire); // Assign Grimoire to Spell UI
				_SpellsUI->RequestCloseDelegate.AddDynamic(this, &AUmbraNighPlayerController::ToggleSpellsUI); // Bind Delegate for if it requests deletion internally
			}
		}
	}
}

void AUmbraNighPlayerController::TogglePossessionUI()
{
	UE_LOG(LogTemp, Display, TEXT("Possession UI Toggle Requested"));
	if (IsValid(PlayerGrimoire)) {
		if (SpellsUI)ToggleSpellsUI();
		
		if (TryToggleUI(PossessionUIClass, PossessionUI)) {
			UGrimoirePossessionUI* _PossessionUI = Cast<UGrimoirePossessionUI>(PossessionUI);
			if (IsValid(_PossessionUI)) { // If just created, initialise
				_PossessionUI->InitialiseButtons(PlayerGrimoire);
				_PossessionUI->RequestCloseDelegate.AddDynamic(this, &AUmbraNighPlayerController::TogglePossessionUI); // Bind Delegate for if it requests deletion internally (#todo: add an interface or abstract class for ui toggling)
			}
		}
	}
}

void AUmbraNighPlayerController::OnGrimoirePossessionChanged()
{
	if (IsValid(PlayerGrimoire)) {
		TScriptInterface<IPlayable> NewPossessed = PlayerGrimoire->GetCurrentPossessed();
		if (NewPossessed && !(CurrentPawn && NewPossessed.GetObjectRef() == CurrentPawn.GetObjectRef())) { // Check if new playable is valid and that it's not the same as the current one
			APawn* NewPossessedPawn = Cast<APawn>(NewPossessed.GetObjectRef());
			if (NewPossessedPawn) { // Possess new Playable if is valid pawn
				Possess(NewPossessedPawn);
			}
		}
	}
}

TScriptInterface<IInteractable> AUmbraNighPlayerController::CheckInteraction()
{
	AActor* CurrentPawnActor = (CurrentPawn) ? Cast<AActor>(CurrentPawn.GetObjectRef()) : nullptr;
	if (CurrentPawnActor) {
		const FVector TraceStart = CurrentCamera->GetComponentLocation();
		const FVector TraceEnd = TraceStart + CurrentCamera->GetForwardVector() * INTERACTION_DETECTION_RANGE;

		// Set up Trace Params
		FCollisionQueryParams TraceParams(FName(TEXT("Interaction Trace")), true, CurrentPawnActor);
		TraceParams.bReturnPhysicalMaterial = false;
		FHitResult HitOut = FHitResult(ForceInit);
		/* Debugging */
		const FName TraceTag("InteractionTrace");
		GetWorld()->DebugDrawTraceTag = TraceTag;
		TraceParams.TraceTag = TraceTag;
		//*/

		// Perform Trace 
		GetWorld()->LineTraceSingleByChannel(HitOut, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, TraceParams);

		AActor* HitActor = HitOut.GetActor();

		if (HitActor) {
			UE_LOG(LogTemp, Display, TEXT("Interaction Trace hits <%s>"), *HitActor->GetActorNameOrLabel());
			if (HitActor->GetClass()->ImplementsInterface(UInteractable::StaticClass())) {

				float InteractableDistance = FVector::Distance(HitActor->GetActorLocation(), CurrentPawnActor->GetActorLocation());
				UE_LOG(LogTemp, Display, TEXT("{%s} is <%f>/<%f> units from Interactable <%s>"), *GetActorNameOrLabel(), InteractableDistance, InteractionRange, *HitActor->GetActorNameOrLabel());
				
				if (InteractableDistance <= InteractionRange) {
					UE_LOG(LogTemp, Display, TEXT("Found Interaction"));
					return HitActor;
				}
			}
		}
	}

	return nullptr;
}


void AUmbraNighPlayerController::MoveForwardBack(float AxisValue)
{
	MovementInput.X = AxisValue;
}

void AUmbraNighPlayerController::MoveRightLeft(float AxisValue)
{
	MovementInput.Y = AxisValue;
}

void AUmbraNighPlayerController::LookPitch(float AxisValue)
{
	if (bUIOpen) return; // Disable if UI is open

	LookInput.Y = AxisValue;
	if (CurrentPawn && IsValid(CurrentPawn.GetObject())) {
		IPlayable::Execute_DoLook(CurrentPawn.GetObject(), LookInput); // If called in Tick, AddControllerPitchInput does not apply to Camera
	}
}

void AUmbraNighPlayerController::LookYaw(float AxisValue)
{
	if (bUIOpen) return; // Disable if UI is open

	LookInput.Z = AxisValue;
	if (CurrentPawn && IsValid(CurrentPawn.GetObject())) {
		IPlayable::Execute_DoLook(CurrentPawn.GetObject(), LookInput); // If called in Tick, AddControllerYawInput does not apply to Camera
	}
}

void AUmbraNighPlayerController::Jump()
{
	if (CurrentPawn) {
		IPlayable::Execute_DoJump(CurrentPawn.GetObject());
	}
}

void AUmbraNighPlayerController::JumpEnd()
{
	if (CurrentPawn) {
		IPlayable::Execute_DoJumpEnd(CurrentPawn.GetObject());
	}
}

void AUmbraNighPlayerController::Sneak()
{
	if (CurrentPawn) {
		IPlayable::Execute_DoSneak(CurrentPawn.GetObject());
	}
}

void AUmbraNighPlayerController::SneakEnd()
{
	if (CurrentPawn) {
		IPlayable::Execute_DoSneakEnd(CurrentPawn.GetObject());
	}
}

void AUmbraNighPlayerController::SneakToggle()
{
}

void AUmbraNighPlayerController::PrimaryAction()
{
	if (bUIOpen) return; // Disable if UI is open

	if (CurrentPawn) {
		UE_LOG(LogTemp, Display, TEXT("Player Controller Triggered Primary Action on Current Pawn"));
		IPlayable::Execute_DoPrimaryAction(CurrentPawn.GetObject());
	}
	
}

void AUmbraNighPlayerController::PrimaryActionRelease()
{
	if (CurrentPawn) {
		IPlayable::Execute_DoPrimaryActionRelease(CurrentPawn.GetObject());
	}
}

void AUmbraNighPlayerController::SecondaryAction()
{
	if (bUIOpen) return; // Disable if UI is open

	if (CurrentPawn) {
		UE_LOG(LogTemp, Display, TEXT("Player Controller Triggered Secondary Action on Current Pawn"));
		IPlayable::Execute_DoSecondaryAction(CurrentPawn.GetObject());
	}
}

void AUmbraNighPlayerController::SecondaryActionRelease()
{
	if (CurrentPawn) {
		IPlayable::Execute_DoSecondaryActionRelease(CurrentPawn.GetObject());
	}
}

void AUmbraNighPlayerController::DoInteraction(EInteractionType InteractionType)
{
	if (CurrentPawn && IsValid(CurrentPawn.GetObjectRef())) {

		AUmbraNighCharacter* CurrentCharacter = Cast<AUmbraNighCharacter>(CurrentPawn.GetObjectRef());

		// Use and clear overridden Interaction if exists
		TScriptInterface<IInteractable> InteracionOverride;
		InteracionOverride.SetObject(nullptr);
		CurrentCharacter->InteractionOverrides.RemoveAndCopyValue(InteractionType, InteracionOverride);
		if (InteracionOverride && IsValid(InteracionOverride.GetObjectRef())) {
			IInteractable::Execute_Interact(InteracionOverride.GetObjectRef(), CurrentCharacter);
			return;
		}

		// If no overridder interaction, check for interaction normally
		TScriptInterface<IInteractable> Interactable = CheckInteraction();
		if (Interactable && IPlayable::Execute_CanInteract(CurrentPawn.GetObjectRef(), Interactable)) {

			UObject* InteractableRef = Interactable.GetObjectRef();
			if (IsValid(InteractableRef)) {

				if (IInteractable::Execute_GetInteractionType(InteractableRef) == InteractionType) {
					// If appropriate input, execute interaction
					IInteractable::Execute_Interact(InteractableRef, CurrentCharacter);
				}
			}
		}
	}
}
