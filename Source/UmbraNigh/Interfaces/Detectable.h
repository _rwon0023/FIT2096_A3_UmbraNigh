// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Detectable.generated.h"

UENUM()
enum class ESuspicionType : uint8 {
	IMPLICIT		UMETA(DisplayName = "IMPLICIT SUSPICION"),
	EXPLICIT		UMETA(DisplayName = "EXPLICIT SUSPICION"),
};

USTRUCT(BlueprintType)
struct FSuspicion {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ESuspicionType SuspicionType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DetectionAmount = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bSuspicionOnly = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Priority = 0;
};

UINTERFACE(MinimalAPI)
class UDetectable : public UInterface
{
	GENERATED_BODY()
};

class UMBRANIGH_API IDetectable
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Detectable")
	void GetInitialDetectability(FSuspicion& SuspicionOut, AActor* Detector);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Detectable")
	void GetDetectability(FSuspicion& SuspicionOut, AActor* Detector);
};
