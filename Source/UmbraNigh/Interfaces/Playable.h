// Ryan Wong 32491034 2022

#pragma once

#include "CoreMinimal.h"

#include "UObject/Interface.h"
#include "Playable.generated.h"

class IInteractable;

UINTERFACE(BlueprintType)
class UPlayable : public UInterface
{
	GENERATED_BODY()
};

class UMBRANIGH_API IPlayable
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoMove(FVector MovementInput);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoLook(FVector LookInput);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoJump();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoJumpEnd();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoSneak();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoSneakEnd();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoPrimaryAction();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoPrimaryActionRelease();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoSecondaryAction();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable Control")
	void DoSecondaryActionRelease();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable")
	UCameraComponent* GetCamera();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Playable")
	bool CanInteract(TScriptInterface<IInteractable>& Interactable);

};
