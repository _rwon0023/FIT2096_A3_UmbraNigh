// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Blindable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBlindable : public UInterface
{
	GENERATED_BODY()
};

class UMBRANIGH_API IBlindable
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Blindable")
	bool bIsBlind();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Blindable")
	void DoBlindForDuration(int Duration);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Blindable")
	void BlindSelf();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Blindable")
	void UnBlindSelf();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Blindable")
	void DoDefaultBlind();
};
