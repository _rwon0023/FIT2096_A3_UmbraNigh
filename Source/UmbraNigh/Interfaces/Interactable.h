// Ryan Wong 32491034 2022

#pragma once

#include "CoreMinimal.h"

#include "UObject/Interface.h"
#include "Interactable.generated.h"

class AUmbraNighCharacter;

UINTERFACE(MinimalAPI)
class UInteractable : public UInterface
{
	GENERATED_BODY()
};

UENUM()
enum class EInteractionType : uint8
{
	INTERACT	UMETA(DisplayName = "INTERACT"), // General Interactions
	HANDLE		UMETA(DisplayName = "HANDLE"), // Handling things, picking up and dropping
};

class UMBRANIGH_API IInteractable
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable")
	void Interact(AUmbraNighCharacter* Interactor);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable")
	EInteractionType GetInteractionType();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable")
	bool bIsMainPlayerExclusive();
};
