// { Ryan Wong <32491034> 2022 }


#include "Shootable.h"
#include "UmbraNigh/Weapons/Firearms/Firearm.h"

// Add default functionality here for any IShootable functions that are not pure virtual.

void IShootable::AssignColliderAsShootable(UPrimitiveComponent* Collider)
{
	// Assign collider to block Shootable Trace channel (ECC_GameTraceChannel1)
	Collider->SetCollisionResponseToChannel(SHOOTABLE_TRACE_CHANNEL, ECollisionResponse::ECR_Block);
}
