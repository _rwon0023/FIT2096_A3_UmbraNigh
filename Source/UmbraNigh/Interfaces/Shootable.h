// { Ryan Wong <32491034> 2022 }

#pragma once

#include "CoreMinimal.h"

#include "Components/PrimitiveComponent.h"

#include "UObject/Interface.h"
#include "Shootable.generated.h"

// Forward Declarations
class AFirearm;

UINTERFACE(MinimalAPI)
class UShootable : public UInterface
{
	GENERATED_BODY()
};

class UMBRANIGH_API IShootable
{
	GENERATED_BODY()

public:
	inline static const ECollisionChannel SHOOTABLE_TRACE_CHANNEL = ECollisionChannel::ECC_GameTraceChannel1;

	void AssignColliderAsShootable(UPrimitiveComponent* Collider);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Shootable")
		void OnShot(FHitResult& Hit, FVector ShotDirection, AFirearm* FirearmShotBy);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Shootable")
		void DealDamage(int ShotDamage);
};
