Notes: 

There was not enough time to implement some minor features that were previously planned, and such these have been omitted. All the core functionality as required by the specifications is here however. Technical Design Document has been updated to reflect final gameplay by removing and modifiying some features.

> *(Only inconsistency is that I couldn't get the Security Defences to completion as Unreal's Perception Component System wasn't detecting anything for them, but Regular Enemy AI works fine)*

Controls specified in Technical Design Document. To move to next level, interact with the objective (Octohedron in Level 0, Laptop in Level 1, Glowing Question Mark in Level 2) and return to the blue box near the start. 

> *(note: I forgot to update controls for Ravenrat; [Shift + Right Click] to break transformation, [Left Click] to stop flight)*

When the game ends you just return to the main menu.

If the Enemy Suspicion debug prompts get excessive, you can remove them in `Source/UmbraNigh/AI/EnemyCharAIController.cpp` in the `UpdateAlertStatus()` function.
